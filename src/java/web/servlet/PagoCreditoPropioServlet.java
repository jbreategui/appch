package web.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import service.CuentaPropiaService;
import service.PagoCreditoPropioService;
import service.ResumenCuentaService;
import util.JsonObjectWaka;
import util.MetodosGlobales;
import util.Result;

@WebServlet(name = "PagoCreditoPropioServlet", urlPatterns = {"/PagoCreditoPropioServlet"})
public class PagoCreditoPropioServlet extends HttpServlet {

    MetodosGlobales metodosGlobales = new MetodosGlobales();

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        metodosGlobales.validar_dispositivo(request, response);
        
        String accion = request.getParameter("accion");

        String ID = (String) request.getSession().getAttribute("ID");
        if ((ID == null) || (!ID.equals(request.getSession().getId()))) {
            try (PrintWriter out = response.getWriter()) {
                String result = "0" + "%%%" + "index";
                out.print(result);
            }

        } else {

            request.getSession().setAttribute("rutaServlet", "PagoCreditoPropioServlet");

            switch (accion) {
                case "menu_pago_credito":
                    menu_pago_credito(request, response);
                    break;
                case "listar_cuenta_credito":
                    listar_cuenta_credito(request, response);
                    break;
                case "registrar_pago_credito":
                    registrar_pago_credito(request, response);
                    break;
                case "listar_cuotas":
                    listar_cuotas(request, response);
                    break;
                case "confirmar_pago":
                    confirmar(request, response);
                    break;

                case "elegir_cuenta_cargo":
                    elegir_cuenta_cargo(request, response);
                    break;

                case "pagos":
                    pagos(request, response);
                    break;
                default:
                    break;
            }
        }
    }

    protected void menu_pago_credito(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String ID = (String) request.getSession().getAttribute("ID");
        String result= "";
        if ((ID == null) || (!ID.equals(request.getSession().getId()))) {

            result = 0 + "%%%" + "index";

        } else {
                       // web/login/resumen_cuentas/pagos/elegir_tipopago.jsp
            result = 0+ "%%%" +"elegirTipopago";
    
        }
         try (PrintWriter out = response.getWriter()) {
            out.print(result);
         }
                            
    }
    
    protected void elegir_cuenta_cargo(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        request.getSession().setAttribute("accion", "listar_cuotas");

        String codigo_cliente = (String) request.getSession().getAttribute("r_codigoCliente");
        String token = (String) request.getSession().getAttribute("r_tokenAcceso");

        List<Object[]> array_lista_cuenta_ori = new LinkedList<>();
        array_lista_cuenta_ori = (List<Object[]>) request.getSession().getAttribute("pago_propio_credito");

        String indicador_cuenta = request.getParameter("indicador_cuenta");
        Integer ind = Integer.parseInt(indicador_cuenta);
        Object[] cuenta_total = array_lista_cuenta_ori.get(ind);
        String codigo_cuenta_credito = (String) cuenta_total[0];

        CuentaPropiaService cps = new CuentaPropiaService();
        Result resultadoWS = cps.listar_cuenta_propia_detalle(codigo_cliente, "P", token);
        String result;
        if (resultadoWS.getTipoMensaje() == 0) {

            List<Object[]> lista = new LinkedList<>();
            lista = metodosGlobales.read3(resultadoWS.getResultado());

            request.getSession().setAttribute("pago_propio_cta_credito_destino", codigo_cuenta_credito);
            request.getSession().setAttribute("elegir_cuenta_creditos_propios", lista);
            
             // web/login/funciones/elegir_cuenta.jsp
            result = resultadoWS.getTipoMensaje() + "%%%" + "elegirCuenta";

        } else {

            result = resultadoWS.getTipoMensaje() + "%%%" + resultadoWS.getDescripcion();

        }

        try (PrintWriter out = response.getWriter()) {
            out.print(result);
        }

    }

    protected void listar_cuenta_credito(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.getSession().setAttribute("accion", "elegir_cuenta_cargo");

        request.getSession().setAttribute("FLAG_OP_SERVLET", false);
        request.getSession().setAttribute("inputs_AP02", null);
        String token = (String) request.getSession().getAttribute("r_tokenAcceso");
        String numero_tarjeta = (String) request.getSession().getAttribute("r_numeroTarjeta");

        ResumenCuentaService service = new ResumenCuentaService();
        Result resultadoWS = service.listar_cuenta_credito(numero_tarjeta, token);
        String result = null;
        if (resultadoWS.getTipoMensaje() == 0) {

            List<Object[]> lista_cuenta = new LinkedList<>();
            lista_cuenta = metodosGlobales.read3(resultadoWS.getResultado());

            request.getSession().setAttribute("pago_propio_credito", lista_cuenta);

            // web/login/funciones/elegir_cuenta_credito.jsp
            result = resultadoWS.getTipoMensaje() + "%%%" + "elegirCuenta_credito";

        } else {
            result = resultadoWS.getTipoMensaje() + "%%%" + resultadoWS.getDescripcion();

        }

        try (PrintWriter out = response.getWriter()) {
            out.print(result);
        }

    }

    protected void listar_cuotas(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
       
        String type=metodosGlobales.validar_dispositivo_teclado(request, response);
        
        String numerotarjeta = (String) request.getSession().getAttribute("r_numeroTarjeta");
        Object obj_Flag = request.getSession().getAttribute("FLAG_OP_SERVLET");
        Boolean flag_servlet = (Boolean) obj_Flag;
        String cuenta_credito_destino = "";
        String cuenta_credit_cargo = "";
        if (flag_servlet == false) {

            List<Object[]> array_lista_cuenta_ori = new LinkedList<>();
            array_lista_cuenta_ori = (List<Object[]>) request.getSession().getAttribute("elegir_cuenta_creditos_propios");

            String indicador_cuenta_cargo = request.getParameter("indicador_cuenta_cargo");
            Integer ind = Integer.parseInt(indicador_cuenta_cargo);
            Object[] cuenta_total = array_lista_cuenta_ori.get(ind);
            cuenta_credit_cargo = (String) cuenta_total[0];

            cuenta_credito_destino = (String) request.getSession().getAttribute("pago_propio_cta_credito_destino");
        } else {
            List sesion = (List) request.getSession().getAttribute("inputs_AP05");
            request.getSession().setAttribute("numero_cuenta_pro", sesion.get(3).toString());
            cuenta_credit_cargo  = sesion.get(3).toString();
            cuenta_credito_destino  = sesion.get(6).toString();
        }
        request.getSession().setAttribute("pago_propio_cta_credito_destino", cuenta_credito_destino);

        String token = (String) request.getSession().getAttribute("r_tokenAcceso");
        PagoCreditoPropioService tarjetaService = new PagoCreditoPropioService();
        Result resultadoWS = tarjetaService.listarCuotas(numerotarjeta, cuenta_credito_destino, token);
        String result;
        if (resultadoWS.getTipoMensaje() == 0) {

            Object[] lista = (Object[]) metodosGlobales.read2(resultadoWS.getResultado());

            if (lista.length > 0) {

                request.getSession().setAttribute("moneda", ((List) lista[0]).get(2));
                request.getSession().setAttribute("elegir_cuota_monto_creditos_propios", lista);

                //Monto de la cuota
                request.getSession().setAttribute("monto_maximo", ((List) lista[0]).get(4));

                request.getSession().setAttribute("cuentaCreditCargo", cuenta_credit_cargo);
                request.getSession().setAttribute("primera_cuota_propio", ((List) lista[0]).get(1));
                
                request.getSession().setAttribute("type", type);
                
                // web/login/funciones/elegir_cuota_monto.jsp
                result = resultadoWS.getTipoMensaje() + "%%%" + "elegirCuotaMonto";

            } else {
                result = "1" + "%%%" + "No se encontraron cuotas ";
            }

        } else {

            result = resultadoWS.getTipoMensaje() + "%%%" + resultadoWS.getDescripcion();

        }

        try (PrintWriter out = response.getWriter()) {
            out.print(result);
        }
    }

    protected void confirmar(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String result;
        String primera_cuota = (String) request.getSession().getAttribute("primera_cuota_propio");
        Object[] cuotas = (Object[]) request.getSession().getAttribute("elegir_cuota_monto_creditos_propios");
        String id_array_cuota = primera_cuota;
        String listar_cuotas_monto = request.getParameter("listar_cuotas_monto");

        String cuenaCargo = (String) request.getSession().getAttribute("cuentaCreditCargo");
        List lista = new LinkedList();
        for (Object object : cuotas) {
            lista = (List) object;
            if (lista.get(1).toString().compareTo(id_array_cuota) == 0) {
                break;

            }
        }

        //validar aqui 
        Double montoDigitado = Double.parseDouble(listar_cuotas_monto);
        Double montoPagar = Double.parseDouble(lista.get(4).toString());

        String cuenta_origen = cuenaCargo;
        String cuenta_destino = (String) request.getSession().getAttribute("pago_propio_cta_credito_destino");
        String cuota = lista.get(1).toString();
        String moneda = lista.get(2).toString();
        String monto = lista.get(4).toString();
        String vence = lista.get(0).toString();
        String mon = montoDigitado.toString();

        String tipoAfiliacion = (String) request.getSession().getAttribute("r_tipoAfilicacion");
        ArrayList<String> coordenada_clave = metodosGlobales.tipo_afiliacion(tipoAfiliacion);

        Object[] confirmacion_creditos_propios = {
            cuenta_origen,
            cuenta_destino,
            cuota,
            moneda,
            monto,
            vence,
            mon,
            coordenada_clave.get(2)
        };

        request.getSession().setAttribute("cuota_seleccionada", lista);
        request.getSession().setAttribute("confirmacion_creditos_propios", confirmacion_creditos_propios);

        
        // web/login/funciones/confirmacion.jsp
        result = "0" + "%%%" + "confirmacion";

        try (PrintWriter out = response.getWriter()) {
            out.print(result);
        }

    }

    protected void registrar_pago_credito(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        Object[] objDatosCredito = (Object[]) request.getSession().getAttribute("confirmacion_creditos_propios");

        String operacion_recurente = request.getParameter("operacion_recurente");
        String alias_operacion = request.getParameter("alias_operacion");
        String ip = MetodosGlobales.getClientIpAddr(request);

        String numerotarjeta = (String) request.getSession().getAttribute("r_numeroTarjeta");
        String codigo_cliente = (String) request.getSession().getAttribute("r_codigoCliente");
        String cuenta_credito_destino = (String) request.getSession().getAttribute("pago_propio_cta_credito_destino");
        String cuenta_cargo = (String) request.getSession().getAttribute("cuentaCreditCargo");
        String monto_pago_credito = objDatosCredito[6].toString();
        String token = (String) request.getSession().getAttribute("r_tokenAcceso");

        String estado_operacion_frecuente;
        if (operacion_recurente.compareTo("1") == 0) {
            estado_operacion_frecuente = "true";
        } else {
            estado_operacion_frecuente = "false";
        }

        String ipPublicaMovil = ip;
        String codigoSubMenu = "AP05";
        String header = (String) request.getSession().getAttribute("r_tokenAcceso");//cambiar

        PagoCreditoPropioService service = new PagoCreditoPropioService();

        Result res = service.registrar_pago_credito_propio(
                numerotarjeta,
                codigo_cliente,
                cuenta_cargo,
                cuenta_credito_destino,
                monto_pago_credito,
                estado_operacion_frecuente,
                alias_operacion,
                ipPublicaMovil,
                codigoSubMenu,
                header
        );
        String result;
        if (res.getTipoMensaje() == 0) {
            List lista = (List) metodosGlobales.read2(res.getResultado());

            String codigo_operacion = lista.get(0).toString();
            String fecha = lista.get(1).toString();
            String hora = lista.get(2).toString();
            String cuenta_cargo_cons = lista.get(3).toString();
            String cuenta_credito = lista.get(4).toString();

            Object[] dataObj =  (Object[]) lista.get(8);
            List dataAct =  (List) dataObj[0];
            
            String cuota = (dataAct.get(0) == null) ? "" : dataAct.get(0).toString();
            String moneda = dataAct.get(1).toString();
            String monto = metodosGlobales.formato_monto(dataAct.get(2).toString());

            Object[] dataObjProx =  (Object[]) lista.get(9);
            List dataProx =  (List) dataObjProx[0];
           
            String proxima_cuota_moneda = "";
            String proxima_cuota_monto = "";
            String proxima_cuota_vence = "";

            request.getSession().setAttribute("siguiente_cuota", "0");

            if (!dataProx.isEmpty()) {

                proxima_cuota_moneda = dataProx.get(1).toString();
                proxima_cuota_monto = metodosGlobales.formato_monto(dataProx.get(2).toString());
                proxima_cuota_vence = dataProx.get(3).toString();

                request.getSession().setAttribute("siguiente_cuota", "1");

            }

            Object[] obj = {
                codigo_operacion,
                fecha,
                hora,
                cuenta_cargo_cons,
                cuenta_credito,
                cuota,
                moneda,
                monto,
                moneda,
                monto,
                proxima_cuota_moneda,
                proxima_cuota_monto,
                proxima_cuota_vence
            };

            request.getSession().setAttribute("constancia_pago_credito", obj);
            request.getSession().setAttribute("inputs_AP05", null);

            // web/login/funciones/constancia.jsp
            result = "0" + "%%%" + "constancia";

            JsonObjectWaka arrayCabecera = new JsonObjectWaka();

            arrayCabecera.addObject("cod", codigo_operacion)
                    .addObject("fecha", fecha)
                    .addObject("hora", hora)
                    .addObject("cuenta", cuenta_cargo_cons)
                    .addObject("cuentaCredito", cuenta_credito)
                    .addObject("cuota", cuota + " : " + moneda + " " + monto)
                    .addObject("monedamonto", moneda + " " + monto)
                    .addObject("prxima", proxima_cuota_moneda + " " + proxima_cuota_monto)
                    .addObject("vence", proxima_cuota_vence);

            request.getSession().setAttribute("compartir_credio", arrayCabecera.getString());

        } else {
            result = res.getTipoMensaje() + "%%%" + res.getDescripcion();
        }

        try (PrintWriter out = response.getWriter()) {
            out.print(result);
        }

    }

    protected void pagos(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String result = "";
 // web/login/resumen_cuentas/pagos.jsp
        result = "0" + "%%%" + "pagos";

        try (PrintWriter out = response.getWriter()) {
            out.print(result);
        }

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
