package web.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import service.PreguntasFrecuentesService;
import util.MetodosGlobales;
import util.Result;

@WebServlet(name = "PreguntasFrecuentesServlet", urlPatterns = {"/PreguntasFrecuentesServlet"})
public class PreguntasFrecuentesServlet extends HttpServlet {

    MetodosGlobales metodosGlobales = new MetodosGlobales();

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        metodosGlobales.validar_dispositivo(request, response);
        
        String accion = request.getParameter("accion");

        String ID = (String) request.getSession().getAttribute("ID");
        if ((ID == null) || (!ID.equals(request.getSession().getId()))) {
           response.sendRedirect("./");

        } else {
            request.getSession().setAttribute("rutaServlet", "PreguntasFrecuentesServlet");

            switch (accion) {
                case "listar_preguntas":
                    listar_preguntas(request, response);
                    break;
                case "obtener_respuesta":
                    obtener_respuesta(request, response);
                    break;
                case "listar_redes_sociales":
                    listar_redes_sociales(request, response);
                    break;
                default:
                    break;
            }
        }
    }

    protected void listar_preguntas(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        PreguntasFrecuentesService cps = new PreguntasFrecuentesService();
        Result resultadoWS = cps.listar_oper_frecuentes();

        Object[] lista = (Object[]) metodosGlobales.read2(resultadoWS.getResultado());
        request.getSession().setAttribute("lista_prguntas", lista);
        RequestDispatcher dispatcher = request.getRequestDispatcher("web/login/resumen_cuentas/otras_opciones/preguntas_frecuentes.jsp");
        dispatcher.forward(request, response);

    }

    protected void listar_redes_sociales(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        PreguntasFrecuentesService cps = new PreguntasFrecuentesService();
        Result resultadoWS = cps.listar_redes_sociales();

        Object[] lista = (Object[]) metodosGlobales.read2(resultadoWS.getResultado());

        request.getSession().setAttribute("redes_facebook", lista[0]);
        request.getSession().setAttribute("redes_twitter", lista[1]);
        request.getSession().setAttribute("redes_youtube", lista[2]);
        request.getSession().setAttribute("redes_blogger", lista[3]);
        RequestDispatcher dispatcher = request.getRequestDispatcher("web/login/resumen_cuentas/otras_opciones/novedades.jsp");
        dispatcher.forward(request, response);

    }

    protected void obtener_respuesta(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String codigoPregunta = request.getParameter("codigoPregunta");
        Object[] lista = (Object[]) request.getSession().getAttribute("lista_prguntas");
        String respuesta = "";
        for (Object object : lista) {

            List lis = (List) object;

            if (lis.get(0).toString().compareTo(codigoPregunta) == 0) {
                respuesta = lis.get(2).toString();
                break;
            }

        }

        request.setAttribute("respuesta_preguntas_frec", respuesta);
        RequestDispatcher dispatcher = request.getRequestDispatcher("web/login/resumen_cuentas/otras_opciones/pregunta_especifica.jsp");
        dispatcher.forward(request, response);

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
