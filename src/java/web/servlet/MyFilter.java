package web.servlet;

import static com.sun.corba.se.spi.presentation.rmi.StubAdapter.request;
import java.io.IOException;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebFilter(servletNames
        = {
            "IndexServlet",
            "BloqueoTarjetaServlet",
            "BloqueoTarjetaServlet",
            "ConfiguraHuellaDigitalServlet",
            "ConfiguraOperFrecuentesServlet",
            "ConfiguraTarjetaServlet",
            "CuentaOtroBancoServlet",
            "CuentaPropiaServlet",
            "CuentaTerceroServlet",
            "DetalleCuentaServlet",
            "EnvioGirosServlet",
            "LoginServlet",
            "MiPerfilServlet",
            "OlvidoClaveServlet",
            "OperFrecuentesServlet",
            "PagoCreditoPropioServlet",
            "PagoCreditoTerceroServlet",
            "PagoInstitucionesServlet",
            "PagoServicioServlet",
            "PagosRecargasServlet",
            "PreguntasFrecuentesServlet",
            "RegistroPagoCreditoServlet",
            "ResumenCuentaServlet",
            "UbicanosServlet"
        }
)
public class MyFilter implements Filter {

    public void init(FilterConfig arg0) throws ServletException {
    }

    public void doFilter(ServletRequest req, ServletResponse resp,
            FilterChain chain) throws IOException, ServletException {

        HttpServletRequest request = (HttpServletRequest) req;
        HttpServletResponse response = (HttpServletResponse) resp; // PrintWriter out = resp.getWriter();
        if (true) {
            chain.doFilter(req, resp);//sends request to next resource

        } else {
            response.sendRedirect("./Index?accion=index");
        }

    }

    public void destroy() {
    }

}
