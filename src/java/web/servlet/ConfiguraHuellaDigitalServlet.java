package web.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.LinkedList;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import service.HuellaDigitalService;
import util.JsonObjectWaka;
import util.MetodosGlobales;
import util.Result;

@WebServlet(name = "ConfiguraHuellaDigitalServlet", urlPatterns = {"/ConfiguraHuellaDigitalServlet"})
public class ConfiguraHuellaDigitalServlet extends HttpServlet {

    MetodosGlobales metodosGlobales = new MetodosGlobales();

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");

        metodosGlobales.validar_dispositivo(request, response);

        String accion = request.getParameter("accion");

        switch (accion) {

            case "registrar_huella":
                registrar_huella(request, response);
                break;
            case "retornar_huella":
                retornar_huella(request, response);
                break;
            case "configura_huella":
                configura_huella(request, response);
                break;
            case "habilita_huella":
                habilita_huella(request, response);
                break;

            default:
                break;
        }

    }

    protected void configura_huella(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        response.setContentType("text/html;charset=UTF-8");

        String result = "";
        String huella_estado = request.getParameter("huella_estado");
        String cambio_estado_huella = "";

        HuellaDigitalService service = new HuellaDigitalService();
        Result resultadoWS = service.retornar_terminos();
        List terminos;

        if (resultadoWS.getTipoMensaje() == 0) {

            terminos = (LinkedList) metodosGlobales.read2(resultadoWS.getResultado());

            if (huella_estado.equals("true")) {
                cambio_estado_huella = "false";
            } else {
                cambio_estado_huella = "true";
            }

            if (terminos.get(0) == null) {
                String terminos2 = "";
                request.getSession().setAttribute("terminos", terminos2);
            } else {
                request.getSession().setAttribute("terminos", terminos);
            }

            request.getSession().setAttribute("cambio_estado_huella", cambio_estado_huella);
            // web/login/resumen_cuentas/otras_opciones/terminos_huella.jsp
            result = resultadoWS.getTipoMensaje() + "%%%" + "terminosHuella";
        } else {
            result = resultadoWS.getTipoMensaje() + "%%%" + resultadoWS.getDescripcion();
        }

        try (PrintWriter out = response.getWriter()) {
            out.print(result);
        }

    }

    protected void habilita_huella(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // web/login/resumen_cuentas/otras_opciones/huella_digital.jsp

        String result = "0" + "%%%" + "huellaDigital";

        try (PrintWriter out = response.getWriter()) {
            out.print(result);
        }

    }

    protected void registrar_huella(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");

        request.getSession().setAttribute("rutaServlet", "HuellaDigital-registrar_huella");

        String numero_tarjeta = (String) request.getSession().getAttribute("r_numeroTarjeta");
        String token = (String) request.getSession().getAttribute("r_tokenAcceso");
        String estado_huella_digital = request.getParameter("estado_huella_digital");
        String huella_cifrada = request.getParameter("huella_cifrada");
        String ippublicamovil = MetodosGlobales.getClientIpAddr(request);
        // String fe = request.getParameter("fe");
        String codigo_sub_menu = "AP17";

        String result = "";
        HuellaDigitalService service = new HuellaDigitalService();
        Result resultadoWS = service.registrar_estado_huella_digital(numero_tarjeta, estado_huella_digital, huella_cifrada, ippublicamovil, codigo_sub_menu, token);

        List componentes_registro_huella;
        if (resultadoWS.getTipoMensaje() == 0) {

            componentes_registro_huella = (LinkedList) metodosGlobales.read2(resultadoWS.getResultado());

            request.getSession().setAttribute("componentes_registro_huella", componentes_registro_huella);
            // request.getSession().setAttribute("fe", fe);

            result = resultadoWS.getTipoMensaje() + "%%%" + "constancia";

            JsonObjectWaka arrayCabecera = new JsonObjectWaka();

            arrayCabecera.addObject("fecha", componentes_registro_huella.get(0).toString())
                    .addObject("hora", componentes_registro_huella.get(1).toString())
                    .addObject("mensaje", componentes_registro_huella.get(2).toString());

            request.getSession().setAttribute("compartir_registro_huella", arrayCabecera.getString());
        } else {
            result = resultadoWS.getTipoMensaje() + "%%%" + resultadoWS.getDescripcion();
        }

        try (PrintWriter out = response.getWriter()) {
            out.print(result);
        }

    }

    protected void retornar_huella(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");

        // request.getSession().setAttribute("rutaServlet", "HuellaDigital-retornar_huella");
        String token = (String) request.getSession().getAttribute("r_tokenAcceso");

        String numero_tarjeta = (String) request.getSession().getAttribute("r_numeroTarjeta");

        String result = "";

        HuellaDigitalService service = new HuellaDigitalService();
        Result resultadoWS = service.retornar_estado_huella(numero_tarjeta, token);

        List estado_huella = new LinkedList<>();

        if (resultadoWS.getTipoMensaje() == 0) {

            estado_huella = (LinkedList) metodosGlobales.read2(resultadoWS.getResultado());

            String estado = (String) estado_huella.get(0);
            request.getSession().setAttribute("estado_huella", estado_huella);

            String mensaje_estado = "";
            if (estado.equals("true")) {
                mensaje_estado = "Deshabilitar huella digital";
            } else {
                mensaje_estado = "Habilitar huella digital";
            }
            request.getSession().setAttribute("mensaje_estado", mensaje_estado);
            // web/login/resumen_cuentas/otras_opciones/huella_digital.jsp
            result = resultadoWS.getTipoMensaje() + "%%%" + "huellaDigital";

        } else {
            result = resultadoWS.getTipoMensaje() + "%%%" + resultadoWS.getDescripcion();
        }

        try (PrintWriter out = response.getWriter()) {
            out.print(result);
        }

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
