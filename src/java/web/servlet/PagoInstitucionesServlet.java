package web.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import service.CuentaPropiaService;
import service.PagoCreditoTerceroService;
import service.PagoInstitucionService;
import service.PagoServicioService;
import util.JsonObjectWaka;
import util.MetodosGlobales;
import util.Result;

@WebServlet(name = "PagoInstitucionesServlet", urlPatterns = {"/PagoInstitucionesServlet"})
public class PagoInstitucionesServlet extends HttpServlet {

    MetodosGlobales metodosGlobales = new MetodosGlobales();
    Boolean estado_frecuente = null;

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        response.setContentType("text/html;charset=UTF-8");
        
        metodosGlobales.validar_dispositivo(request, response);
        ArrayList<String> afiliacion_validador = new ArrayList<>();
        afiliacion_validador = metodosGlobales.tipo_afiliacion((String) request.getSession().getAttribute("r_tipoAfilicacion"));

        if ((afiliacion_validador.get(0)).equals("false")) {

            String result;
            result = afiliacion_validador.get(2) + "%%%" + afiliacion_validador.get(1);
            try (PrintWriter out = response.getWriter()) {
                out.print(result);
            }

        } else {
            String accion = request.getParameter("accion");

            String ID = (String) request.getSession().getAttribute("ID");
            if ((ID == null) || (!ID.equals(request.getSession().getId()))) {
                try (PrintWriter out = response.getWriter()) {
                    String result = "0" + "%%%" + "index";
                    out.print(result);
                }
            } else {
                request.getSession().setAttribute("rutaServlet", "PagoInstitucionesServlet");
                estado_frecuente = (Boolean) Boolean.parseBoolean(request.getSession().getAttribute("r_estado_frecuente").toString());

                switch (accion) {
                    case "pago_instituciones":
                        pago_instituciones(request, response);
                        break;
                    case "buscarEmpresa":
                        buscarEmpresa(request, response);
                        break;
                    case "validarClienteServicio":
                        validarClienteServicio(request, response);
                        break;
                    case "confirmacionClienteServicio":
                        confirmacionClienteServicio(request, response);
                        break;
                    case "confirmarRegistro":
                        confirmarRegistro(request, response);
                        break;
                    case "elegir_cuenta_cargo":
                        elegir_cuenta_cargo(request, response);
                        break;
                    default:
                        break;
                }
            }
        }
    }

    protected void pago_instituciones(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        // web/login/resumen_cuentas/pagos/pago_instituciones.jsp
        String result = "0" + "%%%" + "pagoInstituciones";

        request.getSession().setAttribute("FLAG_OP_SERVLET", false);
        request.getSession().setAttribute("inputs_AP08", null);
        try (PrintWriter out = response.getWriter()) {
            out.print(result);
        }

    }

    protected void validarClienteServicio(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");

        String token = (String) request.getSession().getAttribute("r_tokenAcceso");//cambiar

        Object obj_Flag = request.getSession().getAttribute("FLAG_OP_SERVLET");
        Boolean flag_servlet = (Boolean) obj_Flag;
        String codigo_empresa = "";
        String dni_cliente_servicio = "";
        if (flag_servlet == false) {
            codigo_empresa = request.getParameter("codigo_empresa");
            dni_cliente_servicio = request.getParameter("codigo_cliente");
        } else {
            List sesion = (List) request.getSession().getAttribute("inputs_AP08");
            codigo_empresa = sesion.get(6).toString();
            dni_cliente_servicio = sesion.get(7).toString();
        }

        PagoInstitucionService tarjetaService = new PagoInstitucionService();
        Result resultadoWS = tarjetaService.validar_cliente_Inst(codigo_empresa, dni_cliente_servicio, token);
        String result;

        if (resultadoWS.getTipoMensaje() == 0) {
            List lista = (LinkedList) metodosGlobales.read2(resultadoWS.getResultado());
            Object[] detalles = (Object[]) lista.get(6);
            List<Object[]> salida = new LinkedList<>();
            int id = 0;
            for (Object detalle : detalles) {
                Object[] obj = null;

                List datos = ((List) detalle);
                obj = new Object[datos.size() + 1];
                for (int i = 0; i < datos.size() + 1; i++) {

                    if (i == datos.size()) {
                        id++;
                        obj[i] = id;
                    } else {
                        obj[i] = (datos.get(i) == null) ? "" : datos.get(i).toString();
                    }
                }
                salida.add(obj);
            }

            request.getSession().setAttribute("elegir_cuota_monto_instituciones", lista);
            request.getSession().setAttribute("elegir_cuota_monto_instituciones_detalle", salida);
            // web/login/funciones/elegir_cuota_monto.jsp
            result = resultadoWS.getTipoMensaje() + "%%%" + "elegirCuotaMonto";
        } else {

            result = resultadoWS.getTipoMensaje() + "%%%" + resultadoWS.getDescripcion();

        }

        try (PrintWriter out = response.getWriter()) {
            out.print(result);
        }
    }

    protected void confirmacionClienteServicio(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String result;

        List<Object[]> dataSession = (List<Object[]>) request.getSession().getAttribute("array_list_datos");
        List dataCabecera = (LinkedList) request.getSession().getAttribute("elegir_cuota_monto_instituciones");

        String codigo_cuenta_origen = (String) request.getSession().getAttribute("cuentaOrigen");
        String codigo_empresa = dataCabecera.get(1).toString();
        String razon_social_empresa = dataCabecera.get(2).toString();
        String monto_total = (String) request.getSession().getAttribute("monto_total").toString();
        String cuenta_servicio = dataCabecera.get(5).toString();
        String codigo_cliente_servicio = dataCabecera.get(3).toString();
        String nombre_cliente_servicio = dataCabecera.get(4).toString();
        String estado_operacion_frecuente = request.getParameter("operacion_recurente");
        String alias_operacion = request.getParameter("alias_operacion");
        String tipo_afiliacion = (String) request.getSession().getAttribute("r_tipoAfilicacion");
        String numero_tarjeta = (String) request.getSession().getAttribute("r_numeroTarjeta");
        String codigo_cliente = (String) request.getSession().getAttribute("r_codigoCliente");
        String codigo_solicitud = "";

        String clave_dinamica = (request.getParameter("coordenada") == null) ? "" : request.getParameter("coordenada");
        String clave_cordenada = (request.getParameter("coordenada") == null) ? "" : request.getParameter("coordenada");
        String numero_columna = "";
        String numero_fila = "";
        String ip_publica_movil = MetodosGlobales.getClientIpAddr(request);
        String codigo_sub_menu = "AP08";

        String token = (String) request.getSession().getAttribute("r_tokenAcceso");

        if (estado_operacion_frecuente.compareTo("1") == 0) {
            estado_operacion_frecuente = "true";
        } else {
            estado_operacion_frecuente = "false";
        }
        Object obj_Flag = request.getSession().getAttribute("FLAG_OP_SERVLET");

        Boolean flag_servlet = (Boolean) obj_Flag;
        Boolean estado_frecuente_confirmar = (Boolean) Boolean.parseBoolean(request.getSession().getAttribute("estado_frecuente_confirmar").toString());
        if (flag_servlet == false || estado_frecuente_confirmar == false) {
            if (tipo_afiliacion.compareTo("D") == 0) {
                clave_cordenada = "";
                codigo_solicitud = (String) request.getSession().getAttribute("Inst_codigoSolicitud");
            } else if (tipo_afiliacion.compareTo("A") == 0) {
                clave_dinamica = "";
                numero_columna = (String) request.getSession().getAttribute("Inst_columna");
                numero_fila = (String) request.getSession().getAttribute("Inst_fila");
            }
        }
        PagoInstitucionService pss = new PagoInstitucionService();
        Result resultadoWS = pss.registrar_pago_servicio(
                codigo_cuenta_origen,
                codigo_empresa,
                razon_social_empresa,
                monto_total,
                cuenta_servicio,
                codigo_cliente_servicio,
                nombre_cliente_servicio,
                estado_operacion_frecuente,
                alias_operacion,
                tipo_afiliacion,
                numero_tarjeta,
                codigo_cliente,
                clave_dinamica,
                codigo_solicitud,
                clave_cordenada,
                numero_columna,
                numero_fila,
                ip_publica_movil,
                codigo_sub_menu,
                dataSession,
                token,
                estado_frecuente_confirmar.toString()
        );

        if (resultadoWS.getTipoMensaje() == 0) {
            List lista = (LinkedList) metodosGlobales.read2(resultadoWS.getResultado());

            String codigo_operaacion = lista.get(0).toString();
            String fecha = lista.get(1).toString();
            String hora = lista.get(2).toString();
            String cuentacargo = lista.get(3).toString();
            String empresa = lista.get(4).toString();
            String datos = lista.get(5).toString();
            String moneda = lista.get(6).toString();
            String mon = metodosGlobales.formato_monto(lista.get(7).toString());

            Object[] constancia = {codigo_operaacion, fecha, hora, cuentacargo, empresa, datos, moneda, mon};
            request.getSession().setAttribute("constancia_pago_instituciones_datos", constancia);

            // web/login/funciones/constancia.jsp
            result = resultadoWS.getTipoMensaje() + "%%%" + "constancia";
            JsonObjectWaka arrayCabecera = new JsonObjectWaka();
            arrayCabecera.addObject("cod", codigo_operaacion)
                    .addObject("fecha", fecha)
                    .addObject("hora", hora)
                    .addObject("cuenta", cuentacargo)
                    .addObject("empresa", empresa)
                    .addObject("datos", datos)
                    .addObject("monedamonto", moneda + " " + mon);

            request.getSession().setAttribute("pago_inst_compartir", arrayCabecera.getString());

        } else {

            result = resultadoWS.getTipoMensaje() + "%%%" + resultadoWS.getDescripcion();

        }
        try (PrintWriter out = response.getWriter()) {
            out.print(result);
        }
    }

    protected void confirmarRegistro(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        List<Object[]> data_session = (List<Object[]>) request.getSession().getAttribute("array_list_datos");

        String monto = (String) request.getSession().getAttribute("monto_pago_instituciones");
        List data_cabecera = (LinkedList) request.getSession().getAttribute("elegir_cuota_monto_instituciones");

        List<Object[]> array_lista_cuenta_ori = new LinkedList<>();
        array_lista_cuenta_ori = (List<Object[]>) request.getSession().getAttribute("elegir_cuenta_instituciones");

        String indicador_cuenta = request.getParameter("indicador_cuenta");
        Integer ind = Integer.parseInt(indicador_cuenta);
        Object[] cuenta_total = array_lista_cuenta_ori.get(ind);
        String cuenta_origen = (String) cuenta_total[0];

        String empresa = data_cabecera.get(2).toString();
        String datos = data_cabecera.get(4).toString();

        String moneda_monto = data_session.get(0)[20] + " " + metodosGlobales.formato_monto(monto.toString());

        String token = (String) request.getSession().getAttribute("r_tokenAcceso");
        String numero_tarjeta = (String) request.getSession().getAttribute("r_numeroTarjeta");
        String tipo_afiliacion = (String) request.getSession().getAttribute("r_tipoAfilicacion");
        String codigo_sub_menu = "AP08";
        ArrayList<String> coordenada_clave = metodosGlobales.tipo_afiliacion(tipo_afiliacion);
        String result = "";
        PagoCreditoTerceroService egs = new PagoCreditoTerceroService();

        boolean flag = true;
        Object obj_Flag = request.getSession().getAttribute("FLAG_OP_SERVLET");
        Boolean flag_servlet = (Boolean) obj_Flag;
        Boolean estado_frecuente_confirmar = (Boolean) Boolean.parseBoolean(request.getSession().getAttribute("r_estado_frecuente").toString());
        if (flag_servlet == false || estado_frecuente == false) {
            estado_frecuente_confirmar = false;
            if (tipo_afiliacion.compareTo("D") == 0) {
                Result r = egs.retornar_codigo_operacion(numero_tarjeta, codigo_sub_menu, monto.toString(), token);
                if (r.getTipoMensaje() == 0) {
                    List codigoSolicitud = (List) metodosGlobales.read2(r.getResultado());
                    request.getSession().setAttribute("Inst_codigoSolicitud", codigoSolicitud.get(0));
                    System.out.println(codigoSolicitud.get(0));

                } else {
                    result = r.getTipoMensaje() + "%%%" + r.getDescripcion();
                    flag = false;
                }
            } else if (tipo_afiliacion.compareTo("A") == 0) {

                request.getSession().setAttribute("Inst_columna", coordenada_clave.get(0));
                request.getSession().setAttribute("Inst_fila", coordenada_clave.get(1));

            }
            request.getSession().setAttribute("flag_clave", tipo_afiliacion);
        }
        request.getSession().setAttribute("estado_frecuente_confirmar", estado_frecuente_confirmar);
        if (flag) {

            Object[] obj = {cuenta_origen, empresa, datos, moneda_monto, coordenada_clave.get(2)};
            request.getSession().setAttribute("confirmacion_instituciones", obj);
            request.getSession().setAttribute("monto_total", monto);
            request.getSession().setAttribute("cuentaOrigen", cuenta_origen);
            // web/login/funciones/confirmacion.jsp
            result = "0" + "%%%" + "confirmacion";
        }
        try (PrintWriter out = response.getWriter()) {
            out.print(result);
        }

    }

    protected void elegir_cuenta_cargo(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String codigo_cliente = (String) request.getSession().getAttribute("r_codigoCliente");
        String token = (String) request.getSession().getAttribute("r_tokenAcceso");//cambiar
        List<Object[]> lista_session = (List<Object[]>) request.getSession().getAttribute("elegir_cuota_monto_instituciones_detalle");
        String[] array = request.getParameterValues("array[]");
        List<Object[]> lista_nueva = new LinkedList<>();
        Double monto = 0d;
        for (Object[] obj : lista_session) {

            for (String value : array) {
                if (value.equals(obj[25].toString())) {
                    String monto_pagado = obj[8].toString();
                    String estado_pago_cuota = "T";
                    String estado_registro = "1";
                    obj[11] = monto_pagado;
                    obj[14] = estado_pago_cuota;
                    obj[19] = estado_registro;
                    monto += Double.parseDouble(monto_pagado);

                }

            }
            lista_nueva.add(obj);

        }

        CuentaPropiaService cps = new CuentaPropiaService();
        Result resultadoWS = cps.listar_cuenta_propia_detalle(codigo_cliente, "P", token);
        String result;
        if (resultadoWS.getTipoMensaje() == 0) {

            List<Object[]> lista = new LinkedList<>();
            lista = metodosGlobales.read3(resultadoWS.getResultado());
            request.getSession().setAttribute("elegir_cuenta_instituciones", lista);
            request.getSession().setAttribute("array_list_datos", lista_nueva);
            request.getSession().setAttribute("monto_pago_instituciones", monto.toString());
            // web/login/funciones/elegir_cuenta.jsp
            result = resultadoWS.getTipoMensaje() + "%%%" + "elegirCuenta";

        } else {

            result = resultadoWS.getTipoMensaje() + "%%%" + resultadoWS.getDescripcion();

        }

        try (PrintWriter out = response.getWriter()) {
            out.print(result);
        }

    }

    protected void buscarEmpresa(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String razon_social_empresa = request.getParameter("empresa");
//        String razonSocialEmpresa = "c";
        String codigo_sub_menu = "AP08";
        PagoServicioService tarjetaService = new PagoServicioService();
        Result resultadoWS = tarjetaService.buscarEmpresa(razon_social_empresa, codigo_sub_menu);
        String content = "text/xml;charset=UTF-8";
        request.setCharacterEncoding("UTF-8");
        response.setContentType(content);
        String result = "";
        if (resultadoWS.getTipoMensaje() == 0) {
            Object[] lista = (Object[]) metodosGlobales.read2(resultadoWS.getResultado());

            List<Object[]> array = new LinkedList<>();
            Object[] obj = null;
            for (Object object : lista) {
                List data = (LinkedList) object;
                obj = new Object[data.size()];

                obj[0] = data.get(0);
                obj[1] = data.get(1);
                obj[2] = data.get(2);
                array.add(obj);
            }

            StringBuilder sb = new StringBuilder();
            for (Object[] f : array) {
                sb.append("<option value='").append(f[0]).append("%%%").append(f[2]).append("'>");
                sb.append(f[1]);
                sb.append("</option>");
            }
            result = sb.toString();

            try (PrintWriter out = response.getWriter()) {
                out.print(result);
            }
        }

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
