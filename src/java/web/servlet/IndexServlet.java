package web.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import service.RetornoDatosGobales;
import util.MetodosGlobales;
import util.Result;

@WebServlet(name = "IndexServlet", urlPatterns = {"/Index"})
public class IndexServlet extends HttpServlet {

    MetodosGlobales metodosGlobales = new MetodosGlobales();

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
         response.setContentType("text/html;charset=UTF-8");
        
        metodosGlobales.validar_dispositivo(request, response);
        
        String accion = request.getParameter("accion");

        switch (accion) {
            case "iniciar_variables_globales":
                iniciar_variables_globales(request, response);
                break;
            case "lista_dinamica":
                lista_dinamica(request, response);
                break;
            case "index":
                index(request, response);
                break;
            default:
                break;
        }

    }

    protected void iniciar_variables_globales(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        
        

        RetornoDatosGobales datosGlobales = new RetornoDatosGobales();
        Result result = datosGlobales.listar_datos_globales();
        String resultado = "";
        String codigo_instalacion = req.getParameter("codigoInstalacion").trim();
        String validador = req.getParameter("validador");
        String ip = req.getParameter("ip").trim();
        Result r2 = datosGlobales.listar_tipo_tarjeta();
        if (result.getTipoMensaje() == 0) {
            if (r2.getTipoMensaje() == 0) {
                Object[] lista_datos_tarjeta = (Object[]) metodosGlobales.read2(r2.getResultado());
                String obj[] = new String[2];
                String codigo = "";
                String valor_default = "";

                for (Object dato : lista_datos_tarjeta) {

                    LinkedList<String> linkedlist = (LinkedList<String>) dato;

                    codigo = linkedlist.get(0);
                    valor_default = linkedlist.get(2);
                    break;

                }

                Result r = datosGlobales.retornar_numeroTarjeta(codigo, codigo_instalacion);
                if (r.getTipoMensaje() == 0) {
                    List datos = (List) metodosGlobales.read2(r.getResultado());

                    String estadoRecordar = (String) datos.get(1);
                    String numeroTarjeta = "";
                    if (estadoRecordar.equals("true")) {
                        numeroTarjeta = (String) datos.get(0);

                        obj[1] = "true";
                        obj[0] = numeroTarjeta;

                    } else {
                        obj[1] = "false";
                        obj[0] = valor_default;
                    }
                } else {
                    obj[1] = "false";
                    obj[0] = valor_default;
                }

                String datos_val[] = new String[2];
                datos_val = obj;

                Object lista = metodosGlobales.read2(result.getResultado());

                SimpleDateFormat dt = new SimpleDateFormat("yyyy.MM.dd");
                String fecha = "";
                try {
                    Date date = dt.parse(((List<Object>) (((Object[]) lista)[1])).get(1).toString());
                    fecha = new SimpleDateFormat("EEEE dd 'de' MMMM', 'YYYY", new Locale("es", "ES")).format(date);

                } catch (ParseException ex) {

                }
                req.getSession().setAttribute("correo_sugerencias", ((List<Object>) (((Object[]) lista)[0])).get(1).toString());
                req.getSession().setAttribute("tipo_cambio_compra", ((List<Object>) (((Object[]) lista)[3])).get(1).toString());
                req.getSession().setAttribute("tipo_cambio_venta", ((List<Object>) (((Object[]) lista)[4])).get(1).toString());
                req.getSession().setAttribute("numero_fijo", ((List<Object>) (((Object[]) lista)[2])).get(1).toString());
                req.getSession().setAttribute("fecha_sistema", fecha);
                req.getSession().setAttribute("r_codigoCliente", null);
                req.getSession().setAttribute("datos_tarjeta_recordar", datos_val);
                req.getSession().setAttribute("IP_iOS", ip);
                req.getSession().setAttribute("codigoInstalacion_iOS", codigo_instalacion);
                req.getSession().setAttribute("validador", validador);
                req.getSession().setAttribute("lista_datos_tarjeta", (Object[]) lista_datos_tarjeta);

                req.getSession().setAttribute("fecha_numero", metodosGlobales.fecha_numero());
                   // "web/login/login.jsp"
                resultado = result.getTipoMensaje() + "%%%" + "login";

            } else {
                resultado = r2.getTipoMensaje() + "%%%" + result.getDescripcion();

            }

        } else {
            resultado = result.getTipoMensaje() + "%%%" + result.getDescripcion();

        }
        try (PrintWriter out = resp.getWriter()) {
            out.print(resultado);
        }

    }

    protected void lista_dinamica(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        RetornoDatosGobales datosGlobales = new RetornoDatosGobales();
        String resultado = "";

        String codigoTarjeta = req.getParameter("codigoTarjeta");
        String codigoInstalacion = req.getParameter("codigoInstalacion");
        Result resultadoWS = datosGlobales.retornar_numeroTarjeta(codigoTarjeta, codigoInstalacion);
        LinkedList<Object[]> linkedlis = new LinkedList<>();
        String result = "";

        String content = "text/xml;charset=UTF-8";
        req.setCharacterEncoding("UTF-8");
        resp.setContentType(content);
        String[] obj = new String[2];

        if (resultadoWS.getTipoMensaje() == 0) {
            List datos = (List) metodosGlobales.read2(resultadoWS.getResultado());
            String estadoRecordar = (String) datos.get(1);
            String numero_tarjeta = "";
            if (estadoRecordar.equals("true")) {
                numero_tarjeta = (String) datos.get(0);

                obj[0] = "true";
                obj[1] = numero_tarjeta;

            } else {
                obj[0] = "false";
                obj[1] = "default";
            }
            resultado = obj[0] + "%%%" + obj[1];
            try (PrintWriter out = resp.getWriter()) {
                out.print(resultado);
            }

        } else {
            Result r2 = datosGlobales.listar_tipo_tarjeta();
            Object lista_datos_tarjeta = metodosGlobales.read2(r2.getResultado());
            req.getSession().setAttribute("lista_datos_tarjeta", lista_datos_tarjeta);
        }

    }

    protected void index(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        req.getSession().invalidate();

        
        
        if( req.getHeader("User-Agent").contains("Mobile") || req.getHeader("User-Agent").contains("iPad") || req.getHeader("User-Agent").contains("iPhone") ) {
            req.getSession(true);
            resp.sendRedirect("./");
        } else {
            resp.sendRedirect("./");
        }

        

    }

// <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
