package web.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import service.CuentaOtroBancoService;
import service.PagoCreditoTerceroService;
import service.RegistraPagoCreditoService;
import util.JsonObjectWaka;
import util.MetodosGlobales;
import util.Result;

@WebServlet(name = "RegistroPagoCreditoServlet", urlPatterns = {"/RegistroPagoCreditoServlet"})
public class RegistroPagoCreditoServlet extends HttpServlet {

    MetodosGlobales metodosGlobales = new MetodosGlobales();
    Boolean estado_frecuente = null;

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        response.setContentType("text/html;charset=UTF-8");
        
        metodosGlobales.validar_dispositivo(request, response);
        ArrayList<String> afiliacion_validador = new ArrayList<>();
        afiliacion_validador = metodosGlobales.tipo_afiliacion((String) request.getSession().getAttribute("r_tipoAfilicacion"));

        if ((afiliacion_validador.get(0)).equals("false")) {

            String result;
            result = afiliacion_validador.get(2) + "%%%" + afiliacion_validador.get(1);
            try (PrintWriter out = response.getWriter()) {
                out.print(result);
            }

        } else {

            String accion = request.getParameter("accion");
            response.setContentType("text/html;charset=UTF-8");
            String ID = (String) request.getSession().getAttribute("ID");
            if ((ID == null) || (!ID.equals(request.getSession().getId()))) {
                try (PrintWriter out = response.getWriter()) {
                    String result = "0" + "%%%" + "index";
                    out.print(result);
                }

            } else {
                request.getSession().setAttribute("rutaServlet", "RegistroPagoCreditoServlet");
                estado_frecuente = (Boolean) Boolean.parseBoolean(request.getSession().getAttribute("r_estado_frecuente").toString());
 
                
                switch (accion) {
                    case "listar_banco_origen":
                        listar_banco_origen(request, response);
                        break;
                    case "confirmar_registro_pago_credito":
                        confirmar_registro_pago_credito(request, response);
                        break;
                    case "registar_abono":
                        registar_abono(request, response);
                        break;
                    case "registrar":
                        registrar(request, response);
                        break;
                    case "elegir_cuenta_cargo":
                        elegir_cuenta_cargo(request, response);
                        break;
                    default:
                        break;
                }
            }
        }
    }

    protected void elegir_cuenta_cargo(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        request.getSession().setAttribute("FLAG_OP_SERVLET", false);
        request.getSession().setAttribute("inputs_AP04", null);

        String codigo_cliente = (String) request.getSession().getAttribute("r_codigoCliente");
        String token = (String) request.getSession().getAttribute("r_tokenAcceso");//cambiar
        CuentaOtroBancoService cps = new CuentaOtroBancoService();
        Result resultadoWS = cps.listar_cuenta_propia_otro_banco_detalle(codigo_cliente, "P", token);
        String result;
        if (resultadoWS.getTipoMensaje() == 0) {

            List<Object[]> lista = new LinkedList<>();
            lista = metodosGlobales.read3(resultadoWS.getResultado());
            request.getSession().setAttribute("elegir_cuenta_tarjeta_credito", lista);

            // web/login/funciones/elegir_cuenta.jsp
            result = resultadoWS.getTipoMensaje() + "%%%" + "elegirCuenta";

        } else {

            result = resultadoWS.getTipoMensaje() + "%%%" + resultadoWS.getDescripcion();

        }

        try (PrintWriter out = response.getWriter()) {
            out.print(result);
        }

    }

    protected void listar_banco_origen(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        String type=metodosGlobales.validar_dispositivo_teclado(req, resp);
        
        List<Object[]> array_lista_cuenta_ori = new LinkedList<>();
        Object obj_Flag = req.getSession().getAttribute("FLAG_OP_SERVLET");
        String numeroctacargo = "";
        Boolean flag_servlet = (Boolean) obj_Flag;

        String tipo_mon_ori = "";
        String codigo_moneda = "";
        String oficina_origen = "";
        String codigo_destino = "";

        if (flag_servlet == false) {
            array_lista_cuenta_ori = (List<Object[]>) req.getSession().getAttribute("elegir_cuenta_tarjeta_credito");
            String indicador_cuenta = req.getParameter("indicador_cuenta");
            Integer ind = Integer.parseInt(indicador_cuenta);
            Object[] cuenta_total = array_lista_cuenta_ori.get(ind);
            numeroctacargo = (String) cuenta_total[0];
            codigo_moneda = (String) cuenta_total[3];
            oficina_origen = (String) cuenta_total[4];
            tipo_mon_ori = (String) cuenta_total[3];
        } else {
            List sesion = (List) req.getSession().getAttribute("inputs_AP04");
            req.getSession().setAttribute("numero_cuenta_pro", sesion.get(3).toString());
            numeroctacargo = sesion.get(3).toString();
            tipo_mon_ori = sesion.get(4).toString();
            codigo_destino = sesion.get(6).toString();
        }

        RegistraPagoCreditoService service = new RegistraPagoCreditoService();
        Result resultadoWS = service.listar_banco_origen();
        String result = "";

        String moneda = "";

        if (resultadoWS.getTipoMensaje() == 0) {

            Object[] lista = (Object[]) metodosGlobales.read2(resultadoWS.getResultado());

            if (flag_servlet == true) {
                List sesion = (List) req.getSession().getAttribute("inputs_AP04");
                int c = 0;
                for (Object obj : lista) {
                    c++;
                    List list_bank = (List) obj;

                    if ((sesion.get(7).toString().trim()).equals(list_bank.get(0).toString())) {

                        String temp1 = ((List) lista[0]).get(0).toString();
                        String temp2 = ((List) lista[0]).get(1).toString();
                        String temp3 = ((List) lista[0]).get(2).toString();
                        ((List) lista[0]).set(0, list_bank.get(0).toString());
                        ((List) lista[0]).set(1, list_bank.get(1).toString());
                        ((List) lista[0]).set(2, list_bank.get(2).toString());
                        ((List) lista[c - 1]).set(0, temp1);
                        ((List) lista[c - 1]).set(1, temp2);
                        ((List) lista[c - 1]).set(2, temp3);
                    }
                }
            }

            req.getSession().setAttribute("listabancos", lista);
            req.getSession().setAttribute("moneda_ptc", moneda);
            req.getSession().setAttribute("numeroctacargo_pago_credito", numeroctacargo);
            req.getSession().setAttribute("tipo_mon_ori", tipo_mon_ori);

            req.getSession().setAttribute("codigo_destino", codigo_destino);
            req.getSession().setAttribute("oficina_origen", oficina_origen);
            req.getSession().setAttribute("codigo_moneda", codigo_moneda);
            req.getSession().setAttribute("type", type);
            
            // web/login/resumen_cuentas/pagos/datos_pago.jsp
            result = resultadoWS.getTipoMensaje() + "%%%" + "datosPago";

        } else {

            result = resultadoWS.getTipoMensaje() + "%%%" + resultadoWS.getDescripcion();

        }

        try (PrintWriter out = resp.getWriter()) {
            out.print(result);
        }

    }

    protected void registar_abono(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        request.getSession().setAttribute("rutaServlet", "RegistroPagoCreditoServlet");

        RegistraPagoCreditoService creditoService = new RegistraPagoCreditoService();

        String cuentacargo = (String) request.getSession().getAttribute("numeroctacargo_pago_credito");
        String token = (String) request.getSession().getAttribute("r_tokenAcceso");
        String result = "";
        String banco = request.getParameter("banco");
        String agencia = request.getParameter("agencia");
        String numero_tarjeta = request.getParameter("numero_tarjeta");
        String moneda = request.getParameter("moneda");
        Double monto = Double.parseDouble(request.getParameter("monto"));
        String nombremoneda;
        if (moneda.compareTo("1") == 0) {
            nombremoneda = "S/";
        } else {
            nombremoneda = "USD";
        }

        PagoCreditoTerceroService egs = new PagoCreditoTerceroService();
        String codigo_sub_menu = "AP04";
        String numeroTarjeta = (String) request.getSession().getAttribute("r_numeroTarjeta");

        Result r = creditoService.retornar_comision(banco, agencia, moneda, monto + "", numero_tarjeta, cuentacargo, token);
        if (r.getTipoMensaje() == 0) {
            List arrayComision = (List) metodosGlobales.read2(r.getResultado());

            Object[] objetosDatos = {banco, numero_tarjeta, moneda, monto, cuentacargo, agencia};
            request.getSession().setAttribute("objDatosRegPagos_rpc", objetosDatos);

            String cuentaCargo = cuentacargo;
            String bancodestino = arrayComision.get(0).toString();
            String monto_transferencia = arrayComision.get(1).toString();
            String comision_interbancaria = arrayComision.get(2).toString();
            String comision_cliente = arrayComision.get(3).toString();
            String numerotarjeta = arrayComision.get(4).toString();
            String comision_abono = arrayComision.get(5).toString();
            String simbolo_moneda = arrayComision.get(6).toString();
            String itf = arrayComision.get(7).toString();
            String validador_ruta = arrayComision.get(8).toString();

            String monedaMonto = nombremoneda + " " + metodosGlobales.formato_monto(monto.toString());

            request.getSession().setAttribute("cuentaCargo", cuentaCargo);
            request.getSession().setAttribute("bancodestino", bancodestino);
            request.getSession().setAttribute("numerotarjeta", numerotarjeta);
            request.getSession().setAttribute("monedaMonto", monedaMonto);
            request.getSession().setAttribute("monto_transferencia", monto_transferencia);
            request.getSession().setAttribute("comision_interbancaria", comision_interbancaria);
            request.getSession().setAttribute("nombremoneda", nombremoneda);
            request.getSession().setAttribute("comision_cliente", comision_cliente);
            request.getSession().setAttribute("comision_abono", comision_abono);
            request.getSession().setAttribute("simbolo_moneda", simbolo_moneda);
            request.getSession().setAttribute("itf", itf);
            request.getSession().setAttribute("validador_ruta", validador_ruta);

            if (validador_ruta.equals("true")) {
                //  web/login/funciones/abono.jsp
                result = r.getTipoMensaje() + "%%%" + "abono";
            } else {
                request.getSession().setAttribute("opcion_elegida", "");
                request.getSession().setAttribute("celular_correo", "");
                request.getSession().setAttribute("medio_ingresado", "");

                confirmar_registro_pago_credito(request, response);
            }

        } else {
            result = r.getTipoMensaje() + "%%%" + r.getDescripcion();
        }

        try (PrintWriter out = response.getWriter()) {
            out.print(result);
        }

    }

    protected void confirmar_registro_pago_credito(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String validador_ruta = (String) request.getSession().getAttribute("validador_ruta");
        String opcion_elegida = "";
        String celular_correo = "";
        String medio_ingresado = "";

        if (validador_ruta.equals("true")) {
            opcion_elegida = request.getParameter("opcion_elegida");
            celular_correo = request.getParameter("celular_correo");
            medio_ingresado = request.getParameter("medio_ingresado");
        } else {
            opcion_elegida = (String) request.getSession().getAttribute("opcion_elegida");
            celular_correo = (String) request.getSession().getAttribute("celular_correo");
            medio_ingresado = (String) request.getSession().getAttribute("medio_ingresado");
        }

        String cuenta_cargo = (String) request.getSession().getAttribute("cuentaCargo");
        String bancodestino = (String) request.getSession().getAttribute("bancodestino");
        String numerotarjeta = (String) request.getSession().getAttribute("numerotarjeta");
        String moneda_monto = (String) request.getSession().getAttribute("monedaMonto");
        String nombremoneda = (String) request.getSession().getAttribute("nombremoneda");
        String monto_transferencia = (String) request.getSession().getAttribute("monto_transferencia");
        String comision_interbancaria = (String) request.getSession().getAttribute("comision_interbancaria");
        String comision_cliente = (String) request.getSession().getAttribute("comision_cliente");
        String comision_abono = (String) request.getSession().getAttribute("comision_abono");
        String simbolo_moneda = (String) request.getSession().getAttribute("simbolo_moneda");
        String itf = (String) request.getSession().getAttribute("itf");

        String token = (String) request.getSession().getAttribute("r_tokenAcceso");

        String result = "";

        if (opcion_elegida.equals("false")) {
            comision_abono = "0.00";
        }

        Double monto = Double.parseDouble(monto_transferencia);
        Double comision_interbancaria_int = Double.parseDouble(comision_interbancaria);
        Double comision_cliente_int = Double.parseDouble(comision_cliente);
        Double comision_abono_int = Double.parseDouble(comision_abono);
        Double itf_int = Double.parseDouble(itf);
        Double comsiones = comision_cliente_int + comision_interbancaria_int + comision_abono_int + itf_int;
        Double total = monto + comsiones;

        PagoCreditoTerceroService egs = new PagoCreditoTerceroService();
        String codigo_sub_menu = "AP04";
        String numero_tarjeta = (String) request.getSession().getAttribute("r_numeroTarjeta");
        String tipo_afiliacion = (String) request.getSession().getAttribute("r_tipoAfilicacion");
        boolean flag = true;
        ArrayList<String> coordenada_clave = metodosGlobales.tipo_afiliacion(tipo_afiliacion);

        Object obj_Flag = request.getSession().getAttribute("FLAG_OP_SERVLET");

        Boolean flag_servlet = (Boolean) obj_Flag;

        if (flag_servlet == false || estado_frecuente == false) { //cuando necesita dinamica

            if (tipo_afiliacion.compareTo("D") == 0) {
                Result r = egs.retornar_codigo_operacion(numero_tarjeta, codigo_sub_menu, monto.toString(), token);
                if (r.getTipoMensaje() == 0) {
                    List codigoSolicitud = (List) metodosGlobales.read2(r.getResultado());
                    request.getSession().setAttribute("rpc_codigoSolicitud", codigoSolicitud.get(0));
                    
                    // web/login/funciones/confirmacion.jsp
                    result = r.getTipoMensaje() + "%%%" + "confirmacion";
                } else {
                    result = r.getTipoMensaje() + "%%%" + r.getDescripcion();
                    flag = false;
                }
            } else if (tipo_afiliacion.compareTo("A") == 0) {

                request.getSession().setAttribute("rpc_columna", coordenada_clave.get(0));
                request.getSession().setAttribute("rpc_fila", coordenada_clave.get(1));

                result = 0 + "%%%" + "confirmacion";

            }
            request.getSession().setAttribute("flag_clave", tipo_afiliacion);
        } else {
            result = 0 + "%%%" + "confirmacion";
        }

        Object[] obj = {
            cuenta_cargo,
            bancodestino,
            numerotarjeta,
            moneda_monto,
            nombremoneda + " " + metodosGlobales.formato_monto(comision_interbancaria_int.toString()),
            nombremoneda + " " + metodosGlobales.formato_monto(comision_cliente_int.toString()),
            nombremoneda + " " + metodosGlobales.formato_monto(comision_abono_int.toString()),
            nombremoneda + " " + metodosGlobales.formato_monto(itf_int.toString()),
            nombremoneda + " " + metodosGlobales.formato_monto(total.toString()),
            coordenada_clave.get(2)
        };

        request.getSession().setAttribute("confirmacion_tarjeta_credito", obj);
        request.getSession().setAttribute("opcion_elegida", opcion_elegida);
        request.getSession().setAttribute("celular_correo", celular_correo);
        request.getSession().setAttribute("medio_ingresado", medio_ingresado);

        try (PrintWriter out = response.getWriter()) {
            out.print(result);
        }

    }

    protected void registrar(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        RegistraPagoCreditoService creditoService = new RegistraPagoCreditoService();
        String result;
        Object[] obj_datos_reg_pagos_rpc = (Object[]) request.getSession().getAttribute("objDatosRegPagos_rpc");

        String codigo_cuenta_origen = obj_datos_reg_pagos_rpc[4].toString();
        String codigo_banco = obj_datos_reg_pagos_rpc[0].toString();
        String codigo_agencia = obj_datos_reg_pagos_rpc[5].toString();
        String numero_tarjeta_banco = obj_datos_reg_pagos_rpc[1].toString();
        String codigo_moneda = obj_datos_reg_pagos_rpc[2].toString();
        String monto_pago = obj_datos_reg_pagos_rpc[3].toString();
        String token = (String) request.getSession().getAttribute("r_tokenAcceso");//cambiar

        String estadoOperacionFrecuente = request.getParameter("operacion_recurente");

        if (estadoOperacionFrecuente.compareTo("1") == 0) {
            estadoOperacionFrecuente = "true";
        } else {
            estadoOperacionFrecuente = "false";
        }
        String alias_operacion = request.getParameter("alias_operacion");
        String tipo_afiliacion = (String) request.getSession().getAttribute("r_tipoAfilicacion");
        String numero_tarjeta = (String) request.getSession().getAttribute("r_numeroTarjeta");
        String afectaComision = (String) request.getSession().getAttribute("opcion_elegida");
        String comisionAbono = (String) request.getSession().getAttribute("comision_abono");
        String tipo_medio = (String) request.getSession().getAttribute("celular_correo");
        String valor_medio = (String) request.getSession().getAttribute("medio_ingresado");
        String itf = (String) request.getSession().getAttribute("itf");

        String codigo_cliente = (String) request.getSession().getAttribute("r_codigoCliente");

        String clave_dinamica = (request.getParameter("coordenada") == null) ? "" : request.getParameter("coordenada");
        String clave_cordenada = (request.getParameter("coordenada") == null) ? "" : request.getParameter("coordenada");
        String numero_columna = "";
        String numero_fila = "";
        String codigoSolicitud = "";

        Object obj_Flag = request.getSession().getAttribute("FLAG_OP_SERVLET");
        Boolean flag_servlet = (Boolean) obj_Flag;
        if (flag_servlet == false || estado_frecuente == false) {

            if (tipo_afiliacion.compareTo("D") == 0) {
                clave_cordenada = "";
                codigoSolicitud = (String) request.getSession().getAttribute("rpc_codigoSolicitud");

            } else if (tipo_afiliacion.compareTo("A") == 0) {
                clave_dinamica = "";
                numero_columna = (String) request.getSession().getAttribute("rpc_columna");
                numero_fila = (String) request.getSession().getAttribute("rpc_fila");
            }
        }

        String ip_publica_movil = MetodosGlobales.getClientIpAddr(request);
        String codigo_sub_menu = "AP04";

        Result r = creditoService.registrarPagoCredito(
                codigo_cuenta_origen,
                codigo_banco,
                codigo_agencia,
                numero_tarjeta_banco,
                codigo_moneda,
                monto_pago,
                estadoOperacionFrecuente,
                alias_operacion,
                tipo_afiliacion,
                numero_tarjeta,
                codigo_cliente,
                clave_dinamica,
                clave_cordenada,
                numero_columna,
                numero_fila,
                ip_publica_movil,
                codigo_sub_menu,
                token,
                codigoSolicitud,
                estado_frecuente.toString(),
                afectaComision,
                comisionAbono,
                tipo_medio,
                valor_medio,
                itf
        );

        if (r.getTipoMensaje() == 0) {
            List arrayComision = (List) metodosGlobales.read2(r.getResultado());

            String codigooperacion = arrayComision.get(0).toString();
            String fecha = arrayComision.get(1).toString();
            String hora = arrayComision.get(2).toString();
            String cuentacargo = arrayComision.get(3).toString();
            String bancodestino = arrayComision.get(4).toString();
            String numerotarjeta = arrayComision.get(5).toString();
            String moneda = arrayComision.get(6).toString();
            String monto = arrayComision.get(7).toString();
            Double comision_inter = Double.parseDouble(arrayComision.get(8).toString());
            Double comision_cliete = Double.parseDouble(arrayComision.get(9).toString());
            String total = metodosGlobales.formato_monto(arrayComision.get(10).toString());
            String comision_abono = metodosGlobales.formato_monto(arrayComision.get(11).toString());
            String codigo_solicitud = arrayComision.get(12).toString();
            String itf_constancia = metodosGlobales.formato_monto(arrayComision.get(13).toString());
            Double comsiones = comision_inter + comision_cliete;

            Object[] datos = {
                codigooperacion,
                fecha,
                hora,
                cuentacargo,
                bancodestino,
                numerotarjeta,
                moneda,
                metodosGlobales.formato_monto(monto),
                metodosGlobales.formato_monto(comision_inter.toString()),
                metodosGlobales.formato_monto(comision_cliete.toString()),
                total, comision_abono,
                codigo_solicitud,
                itf_constancia};

            request.getSession().setAttribute("constancia_pago_tarjeta_credito_datos", datos);
            
            // web/login/funciones/constancia.jsp
            result = r.getTipoMensaje() + "%%%" + "constancia";

            request.getSession().setAttribute("inputs_AP04", null);
            JsonObjectWaka arrayCabecera = new JsonObjectWaka();
            arrayCabecera.addObject("cod", codigooperacion)
                    .addObject("fecha", fecha)
                    .addObject("hora", hora)
                    .addObject("cuenta", cuentacargo)
                    .addObject("banco", bancodestino)
                    .addObject("numero", numerotarjeta)
                    .addObject("moneda", moneda + " " + metodosGlobales.formato_monto(monto))
                    .addObject("comisioninterbancaria", moneda + " " + metodosGlobales.formato_monto(comision_inter.toString()))
                    .addObject("comisioncliente", moneda + " " + metodosGlobales.formato_monto(comision_cliete.toString()))
                    .addObject("comisionabono", moneda + " " + metodosGlobales.formato_monto(comision_abono))
                    .addObject("itf", moneda + " " + metodosGlobales.formato_monto(itf_constancia))
                    .addObject("total", moneda + " " + total)
                    .addObject("codigosolicitud", codigo_solicitud);

            request.getSession().setAttribute("cons_compratir", arrayCabecera.getString());
        } else {
            result = r.getTipoMensaje() + "%%%" + r.getDescripcion();
        }
        try (PrintWriter out = response.getWriter()) {
            out.print(result);
        }

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
