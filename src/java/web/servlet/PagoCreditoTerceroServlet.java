package web.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import service.CuentaPropiaService;
import service.PagoCreditoPropioService;
import service.PagoCreditoTerceroService;
import util.JsonObjectWaka;
import util.MetodosGlobales;
import util.Result;

@WebServlet(name = "PagoCreditoTerceroServlet", urlPatterns = {"/PagoCreditoTerceroServlet"})
public class PagoCreditoTerceroServlet extends HttpServlet {

    MetodosGlobales metodosGlobales = new MetodosGlobales();
    Boolean estado_frecuente = null;

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        response.setContentType("text/html;charset=UTF-8");
        
        metodosGlobales.validar_dispositivo(request, response);
        ArrayList<String> afiliacion_validador = new ArrayList<>();
        afiliacion_validador = metodosGlobales.tipo_afiliacion((String) request.getSession().getAttribute("r_tipoAfilicacion"));

        if ((afiliacion_validador.get(0)).equals("false")) {

            String result;
            result = afiliacion_validador.get(2) + "%%%" + afiliacion_validador.get(1);
            try (PrintWriter out = response.getWriter()) {
                out.print(result);
            }

        } else {
            String accion = request.getParameter("accion");

            String ID = (String) request.getSession().getAttribute("ID");
            if ((ID == null) || (!ID.equals(request.getSession().getId()))) {
                try (PrintWriter out = response.getWriter()) {
                    String result = "0" + "%%%" + "index";
                    out.print(result);
                }

            } else {
                request.getSession().setAttribute("rutaServlet", "PagoCreditoTerceroServlet");
                estado_frecuente = (Boolean) Boolean.parseBoolean(request.getSession().getAttribute("r_estado_frecuente").toString());

                switch (accion) {
                    case "pago_terceros":
                        pago_terceros(request, response);
                        break;
                    case "validar_cuenta_credito":
                        validar_cuenta_credito(request, response);
                        break;
                    case "elegir_cuenta_cargo":
                        elegir_cuenta_cargo(request, response);
                        break;
                    case "registrar_pago_credito_tercero":
                        registrar_pago_credito_tercero(request, response);
                        break;

                    case "confirmar_pago":
                        confirmar(request, response);
                        break;

                    default:
                        break;
                }
            }
        }
    }

    protected void pago_terceros(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        request.getSession().setAttribute("FLAG_OP_SERVLET", false);
        request.getSession().setAttribute("inputs_AP02", null);
        // web/login/resumen_cuentas/pagos/ingresar_credito_pagar.jsp
        String result = "0" + "%%%" + "creditoPagar";

        try (PrintWriter out = response.getWriter()) {
            out.print(result);
        }

    }

    protected void validar_cuenta_credito(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String type=metodosGlobales.validar_dispositivo_teclado(request, response);
        
        Object obj_Flag = request.getSession().getAttribute("FLAG_OP_SERVLET");
        Boolean flag_servlet = (Boolean) obj_Flag;
        String result;
        String numerotarjeta = (String) request.getSession().getAttribute("r_numeroTarjeta");
        String token = (String) request.getSession().getAttribute("r_tokenAcceso");

        String cuenta_credito_destino = "";
        if (flag_servlet == false) {
            cuenta_credito_destino = request.getParameter("cuenta_credito_destino").trim();
        } else {
            List sesion = (List) request.getSession().getAttribute("inputs_AP06");
            cuenta_credito_destino = sesion.get(6).toString();
        }

        PagoCreditoTerceroService service = new PagoCreditoTerceroService();
        Result resultadoWS = service.validar_cuenta_credito(numerotarjeta, cuenta_credito_destino, token);

        if (resultadoWS.getTipoMensaje() == 0) {

            Object[] lista_cuenta = (Object[]) metodosGlobales.read2(resultadoWS.getResultado());

            if (lista_cuenta.length != 0) {

                List lista = (LinkedList) lista_cuenta[0];

                Object[] objx = (Object[]) lista.get(2);
                request.getSession().setAttribute("elegir_cuota_monto_creditos_terceros", lista.get(2));
                request.getSession().setAttribute("lista_cuenta_terceros_obj", lista);

                List lista_sub = (LinkedList) objx[0];
                String valor_sub = (String) lista_sub.get(4);

                //Monto de la cuota
                request.getSession().setAttribute("monto_maximo", valor_sub);

                request.getSession().setAttribute("lista_primera_cuota", objx[0]);
                request.getSession().setAttribute("type", type);
                // web/login/funciones/elegir_cuota_monto.jsp
                result = resultadoWS.getTipoMensaje() + "%%%" + "elegirCuotaMonto";

            } else {
                result = "1" + "%%%" + "No se encontraron resultados";

            }

        } else {
            result = resultadoWS.getTipoMensaje() + "%%%" + resultadoWS.getDescripcion();

        }

        try (PrintWriter out = response.getWriter()) {
            out.print(result);
        }
    }

    protected void elegir_cuenta_cargo(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String codigo_cliente = (String) request.getSession().getAttribute("r_codigoCliente");
        String token = (String) request.getSession().getAttribute("r_tokenAcceso");
        String result = "";
        String monto_pagar = request.getParameter("listar_cuotas_monto");

        Object[] session = (Object[]) request.getSession().getAttribute("elegir_cuota_monto_creditos_terceros");

        List list = (List) session[0];

        CuentaPropiaService cps = new CuentaPropiaService();
        Result resultadoWS = cps.listar_cuenta_propia_detalle(codigo_cliente, "P", token);

        if (resultadoWS.getTipoMensaje() == 0) {

            List<Object[]> lista = new LinkedList<>();
            lista = metodosGlobales.read3(resultadoWS.getResultado());

            request.getSession().setAttribute("pago_tercros_montoPagar", monto_pagar);
            request.getSession().setAttribute("primera_cuota_lista", list);
            request.getSession().setAttribute("elegir_cuenta_creditos_terceros", lista);
            
            // web/login/funciones/elegir_cuenta.jsp
            result = resultadoWS.getTipoMensaje() + "%%%" + "elegirCuenta";

        } else {

            result = resultadoWS.getTipoMensaje() + "%%%" + resultadoWS.getDescripcion();

        }

        try (PrintWriter out = response.getWriter()) {
            out.print(result);
        }

    }

    protected void confirmar(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String result = "";
        String token = (String) request.getSession().getAttribute("r_tokenAcceso");
        PagoCreditoTerceroService service = new PagoCreditoTerceroService();
        String numero_tarjeta = (String) request.getSession().getAttribute("r_numeroTarjeta");
        String codigo_sub_menu = "AP06";
        String listar_cuotas_monto = (String) request.getSession().getAttribute("pago_tercros_montoPagar");

        String tipo_afiliacion = (String) request.getSession().getAttribute("r_tipoAfilicacion");
        ArrayList<String> coordenada_clave = metodosGlobales.tipo_afiliacion(tipo_afiliacion);

        boolean flag = true;

        Object obj_Flag = request.getSession().getAttribute("FLAG_OP_SERVLET");

        Boolean flag_servlet = (Boolean) obj_Flag;

        Boolean estado_frecuente_confirmar = (Boolean) Boolean.parseBoolean(request.getSession().getAttribute("r_estado_frecuente").toString());
        if (flag_servlet == false || estado_frecuente == false) {
            estado_frecuente_confirmar = false;
            request.getSession().setAttribute("estado_frecuente_confirmar", estado_frecuente_confirmar);
            if (tipo_afiliacion.compareTo("D") == 0) {
                Result r = service.retornar_codigo_operacion(numero_tarjeta, codigo_sub_menu, listar_cuotas_monto, token);
                if (r.getTipoMensaje() == 0) {
                    List codigoSolicitud = (List) metodosGlobales.read2(r.getResultado());
                    request.getSession().setAttribute("terceros_solicitud", codigoSolicitud.get(0));
                    System.out.println(codigoSolicitud.get(0));
                } else {
                    result = r.getTipoMensaje() + "%%%" + r.getDescripcion();
                    flag = false;
                }
            } else if (tipo_afiliacion.compareTo("A") == 0) {

                request.getSession().setAttribute("terceros_columna", coordenada_clave.get(0));
                request.getSession().setAttribute("terceros_fila", coordenada_clave.get(1));
            }
            request.getSession().setAttribute("flag_clave", tipo_afiliacion);
        }
        request.getSession().setAttribute("estado_frecuente_confirmar", estado_frecuente_confirmar);
        if (flag) {

            List primeraCuota = (List) request.getSession().getAttribute("lista_primera_cuota");
            List sessionObj = (LinkedList) request.getSession().getAttribute("lista_cuenta_terceros_obj");
            String id_array_cuota = primeraCuota.get(1).toString();

            List<Object[]> array_lista_cuenta_ori = new LinkedList<>();
            array_lista_cuenta_ori = (List<Object[]>) request.getSession().getAttribute("elegir_cuenta_creditos_terceros");

            String indicador_cuenta_cargo = request.getParameter("indicador_cuenta_cargo");
            Integer ind = Integer.parseInt(indicador_cuenta_cargo);
            Object[] cuenta_total = array_lista_cuenta_ori.get(ind);
            String cuentacargo = (String) cuenta_total[0];

            //String cuentacargo = request.getParameter("codigoTrajeta");
            List list = (List) request.getSession().getAttribute("primera_cuota_lista");
            Double monto_digitado = Double.parseDouble(listar_cuotas_monto);
            Double monto_pagar = Double.parseDouble(list.get(4).toString());
            String cuenta_cargo = cuentacargo;
            String cuenta_credito = sessionObj.get(0).toString();
            String titular = sessionObj.get(1).toString();
            String cuota = id_array_cuota;
            String moneda = list.get(2).toString();
//            String monto = listar_cuotas_monto;
            String vence = list.get(0).toString();
            Object[] datos = {cuenta_cargo, cuenta_credito, titular, cuota, moneda, monto_pagar, vence, monto_digitado, coordenada_clave.get(2)};

            request.getSession().setAttribute("confirmacion_creditos_terceros", datos);
            
            // web/login/funciones/confirmacion.jsp
            result = "0" + "%%%" + "confirmacion";
        }
        try (PrintWriter out = response.getWriter()) {
            out.print(result);
        }

    }

    protected void registrar_pago_credito_tercero(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String result;

        Object[] obj = (Object[]) request.getSession().getAttribute("confirmacion_creditos_terceros");
        String token = (String) request.getSession().getAttribute("r_tokenAcceso");
        String numero_tarjeta = (String) request.getSession().getAttribute("r_numeroTarjeta");
        String codigo_cliente = (String) request.getSession().getAttribute("r_codigoCliente");
        String cuenta_origen_ahorro = obj[0].toString();
        String cuenta_credito_destino = obj[1].toString();
        String monto_pago_credito = (String) request.getSession().getAttribute("pago_tercros_montoPagar");
        String estado_operacion_frecuente = request.getParameter("operacion_recurente");
        String alias_operacion = request.getParameter("alias_operacion");
        String ip_publica_movil = MetodosGlobales.getClientIpAddr(request);
        String codigo_sub_menu = "AP06";

        String tipo_afilacion = (String) request.getSession().getAttribute("r_tipoAfilicacion");
        String codigo_solicitud = "";
        String numero_columna = "";
        String numero_fila = "";

        String clave_dinamica = (request.getParameter("coordenada") == null) ? "" : request.getParameter("coordenada");
        String clave_cordenada = (request.getParameter("coordenada") == null) ? "" : request.getParameter("coordenada");

        Object obj_Flag = request.getSession().getAttribute("FLAG_OP_SERVLET");
        Boolean flag_servlet = (Boolean) obj_Flag;

        Boolean estado_frecuente_confirmar = (Boolean) Boolean.parseBoolean(request.getSession().getAttribute("estado_frecuente_confirmar").toString());

        if (flag_servlet == false || estado_frecuente_confirmar == false) {

            if (tipo_afilacion.compareTo("D") == 0) {
                clave_cordenada = "";
                codigo_solicitud = (String) request.getSession().getAttribute("terceros_solicitud");
            } else if (tipo_afilacion.compareTo("A") == 0) {
                clave_dinamica = "";
                numero_columna = (String) request.getSession().getAttribute("terceros_columna");
                numero_fila = (String) request.getSession().getAttribute("terceros_fila");
            }
        }
        if (estado_operacion_frecuente.compareTo("1") == 0) {
            estado_operacion_frecuente = "true";
        } else {
            estado_operacion_frecuente = "false";
        }
        PagoCreditoTerceroService service = new PagoCreditoTerceroService();

        Result resultadoWS = service.registrar_pago_credito_tercero(
                numero_tarjeta,
                codigo_cliente,
                cuenta_origen_ahorro,
                cuenta_credito_destino,
                monto_pago_credito,
                estado_operacion_frecuente,
                alias_operacion,
                tipo_afilacion,
                numero_tarjeta,
                codigo_cliente,
                clave_dinamica,
                codigo_solicitud,
                clave_cordenada,
                numero_columna,
                numero_fila,
                ip_publica_movil,
                codigo_sub_menu,
                token,
                estado_frecuente_confirmar.toString()
        );

        if (resultadoWS.getTipoMensaje() == 0) {

            List lista = (List) metodosGlobales.read2(resultadoWS.getResultado());

            // web/login/funciones/constancia.jsp
            result = resultadoWS.getTipoMensaje() + "%%%" + "constancia";

            String codigo_operacion = lista.get(0).toString();
            String fecha = lista.get(1).toString();
            String hora = lista.get(2).toString();

            String cuenta_cargo = lista.get(3).toString();
            String cuenta_credito = lista.get(4).toString();
            String titular = obj[2].toString();
            String cuota = obj[3].toString();
            String moneda = obj[4].toString();

            Object[] dataObjProx = (Object[]) lista.get(9);
            List dataProx = (List) dataObjProx[0];

            String proxima_cuota_moneda = "";
            String proxima_cuota_monto = "";
            String proxima_cuota_vence = "";

            request.getSession().setAttribute("siguiente_cuota", "0");

            if (!dataProx.isEmpty()) {

                proxima_cuota_moneda = dataProx.get(1).toString();
                proxima_cuota_monto = metodosGlobales.formato_monto(dataProx.get(2).toString());
                proxima_cuota_vence = dataProx.get(3).toString();

                request.getSession().setAttribute("siguiente_cuota", "1");

            }

            Object[] contancia = {
                codigo_operacion,
                fecha,
                hora,
                cuenta_cargo,
                cuenta_credito,
                titular,
                cuota,
                moneda,
                metodosGlobales.formato_monto(monto_pago_credito),
                proxima_cuota_moneda,
                proxima_cuota_monto,
                proxima_cuota_vence
            };

            request.getSession().setAttribute("contancia_terceros", contancia);
            //kill session
            request.getSession().setAttribute("elegir_cuota_monto_creditos_terceros", null);
            request.getSession().setAttribute("lista_cuenta_terceros_obj", null);
            request.getSession().setAttribute("pago_tercros_montoPagar", null);
            request.getSession().setAttribute("pago_tercros_cuota", null);
            request.getSession().setAttribute("elegir_cuenta_creditos_terceros", null);
            request.getSession().setAttribute("elegir_cuenta_creditos_terceros", null);
            request.getSession().setAttribute("confirmacion_creditos_terceros", null);

            JsonObjectWaka arrayCabecera = new JsonObjectWaka();

            arrayCabecera.addObject("cod", codigo_operacion)
                    .addObject("fecha", fecha)
                    .addObject("hora", hora)
                    .addObject("cuenta", cuenta_cargo)
                    .addObject("cuentaCredito", cuenta_credito)
                    .addObject("titular", titular)
                    .addObject("cuota", cuota)
                    .addObject("monedamonto", moneda + " " + metodosGlobales.formato_monto(monto_pago_credito))
                    .addObject("titular", titular)
                    .addObject("proxima_cuota_moneda", proxima_cuota_moneda + " " + proxima_cuota_monto)
                    .addObject("vence", proxima_cuota_vence);
            request.getSession().setAttribute("coompartir_terceros", arrayCabecera.getString());
        } else {
            result = resultadoWS.getTipoMensaje() + "%%%" + resultadoWS.getDescripcion();
        }

        try (PrintWriter out = response.getWriter()) {
            out.print(result);
        }

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
