package web.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import service.CuentaTerceroService;
import util.JsonObjectWaka;
import util.MetodosGlobales;
import util.Result;

@WebServlet(name = "CuentaTerceroServlet", urlPatterns = {"/CuentaTerceroServlet"})
public class CuentaTerceroServlet extends HttpServlet {

    MetodosGlobales metodosGlobales = new MetodosGlobales();

    private Object[] dat_mon;

    Boolean estado_frecuente = null;

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");

        metodosGlobales.validar_dispositivo(request, response);
        ArrayList<String> afiliacion_validador = new ArrayList<>();
        afiliacion_validador = metodosGlobales.tipo_afiliacion((String) request.getSession().getAttribute("r_tipoAfilicacion"));

        if ((afiliacion_validador.get(0)).equals("false")) {

            String result;
            result = afiliacion_validador.get(2) + "%%%" + afiliacion_validador.get(1);
            try (PrintWriter out = response.getWriter()) {
                out.print(result);
            }

        } else {
            String accion = request.getParameter("accion");
            String ID = (String) request.getSession().getAttribute("ID");
            if ((ID == null) || (!ID.equals(request.getSession().getId()))) {
                try (PrintWriter out = response.getWriter()) {
                    String result = "0" + "%%%" + "index";
                    out.print(result);
                }

            } else {

                estado_frecuente = (Boolean) Boolean.parseBoolean(request.getSession().getAttribute("r_estado_frecuente").toString());
                switch (accion) {
                    case "listar_cuenta_propia_tercero":
                        listar_cuenta_propia_tercero(request, response);
                        break;
                    case "ingresa_monto_propia_tercero":
                        ingresa_monto_propia_tercero(request, response);
                        break;
                    case "confirmar_transferencia_cuenta_tercero":
                        confirmar_transferencia_cuenta_tercero(request, response);
                        break;
                    case "constancia_transferencia_cuenta_tercero":
                        constancia_transferencia_cuenta_tercero(request, response);
                        break;

                    default:
                        break;
                }
            }
        }

    }

    protected void listar_cuenta_propia_tercero(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");

        request.getSession().setAttribute("FLAG_OP_SERVLET", false);
        request.getSession().setAttribute("inputs_AP02", null);

        request.getSession().setAttribute("rutaServlet", "CuentaTerceroServlet-listar_cuenta_propia_tercero");

        String tipo_operacion_string = "P";
        String token = (String) request.getSession().getAttribute("r_tokenAcceso");
        String codigo_cliente = (String) request.getSession().getAttribute("r_codigoCliente");
        String result = "";
        CuentaTerceroService service = new CuentaTerceroService();
        Result resultadoWS = service.listar_cuenta_propia_tercer_detalle(codigo_cliente, tipo_operacion_string, token);

        if (resultadoWS.getTipoMensaje() == 0) {

            List<Object[]> lista_cuenta_tercero = new LinkedList<>();
            lista_cuenta_tercero = metodosGlobales.read3(resultadoWS.getResultado());

            request.setAttribute("elegir_cuenta_cuentas_terceros", lista_cuenta_tercero);

            request.getSession().setAttribute("elegir_cuenta_cuentas_terceros", lista_cuenta_tercero);
            // web/login/funciones/elegir_cuenta.jsp
            result = resultadoWS.getTipoMensaje() + "%%%" + "elegirCuenta";

        } else {
            result = resultadoWS.getTipoMensaje() + "%%%" + resultadoWS.getDescripcion();
        }

        try (PrintWriter out = response.getWriter()) {
            out.print(result);
        }

    }

    protected void ingresa_monto_propia_tercero(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");

        String type = metodosGlobales.validar_dispositivo_teclado(request, response);

        List<Object[]> array_lista_cuenta_ori = new LinkedList<>();
        Object obj_Flag = request.getSession().getAttribute("FLAG_OP_SERVLET");
        String numero_cuenta_origen = "";
        Boolean flag_servlet = (Boolean) obj_Flag;
        String tipo_mon_ori = "";
        String[] objsol2 = new String[3];
        String[] objdol = new String[3];
        if (flag_servlet == false) {
            array_lista_cuenta_ori = (List<Object[]>) request.getSession().getAttribute("elegir_cuenta_cuentas_terceros");
            String indicador_cuenta = request.getParameter("indicador_cuenta");
            Integer ind = Integer.parseInt(indicador_cuenta);
            Object[] cuenta_total = array_lista_cuenta_ori.get(ind);
            numero_cuenta_origen = (String) cuenta_total[0];
            tipo_mon_ori = (String) cuenta_total[3];
        } else {
            List sesion = (List) request.getSession().getAttribute("inputs_AP02");
            request.getSession().setAttribute("numero_cuenta_pro", sesion.get(3).toString());
            numero_cuenta_origen = sesion.get(3).toString();
            tipo_mon_ori = sesion.get(4).toString();
        }

        String tipo_cambio_compra = (String) request.getSession().getAttribute("tipo_cambio_compra");
        String tipo_cambio_venta = (String) request.getSession().getAttribute("tipo_cambio_venta");

        String[] tipo_cambio = new String[2];;
        tipo_cambio[0] = tipo_cambio_compra;
        tipo_cambio[1] = tipo_cambio_venta;

        String result = "";

        CuentaTerceroService service = new CuentaTerceroService();
        Result resultadoWS = service.listar_moneda_tercero();

        if (resultadoWS.getTipoMensaje() == 0) {

            dat_mon = (Object[]) metodosGlobales.read2(resultadoWS.getResultado());
            for (Object dato : dat_mon) {

                LinkedList<String> linkedlist = (LinkedList<String>) dato;

                String[] objsol = null;
                objsol = new String[linkedlist.size()];
                objdol = new String[linkedlist.size()];

                objsol[0] = linkedlist.get(0);
                objsol[1] = linkedlist.get(1);
                objsol[2] = linkedlist.get(2);

                String valor1 = objsol[0];
                String valor2 = objsol[1];
                String valor3 = objsol[2];

                if (valor1.equals("1")) {

                    objsol2[0] = valor1;
                    objsol2[1] = valor2;
                    objsol2[2] = valor3;
                } else if (valor1.equals("2")) {

                    objdol[0] = valor1;
                    objdol[1] = valor2;
                    objdol[2] = valor3;
                }

            }

            String valor_moneda = "";
            if (tipo_mon_ori.equals(objsol2[0])) {
                valor_moneda = objsol2[2];
            } else {
                valor_moneda = objdol[2];
            }

            request.getSession().setAttribute("dat_mon_sol", objsol2);
            request.getSession().setAttribute("dat_mon_dol", objdol);
            request.getSession().setAttribute("tipo_cambio", tipo_cambio);
            request.getSession().setAttribute("valor_moneda", valor_moneda);
            request.getSession().setAttribute("tipo_mon_ori", tipo_mon_ori);
            request.getSession().setAttribute("numero_cuenta_origen", numero_cuenta_origen);
            request.getSession().setAttribute("type", type);

            // web/login/resumen_cuentas/ingresar_monto_cuenta_destino.jsp
            result = resultadoWS.getTipoMensaje() + "%%%" + "ingresar_monto_cuenta_destino";

        } else {
            result = resultadoWS.getTipoMensaje() + "%%%" + resultadoWS.getDescripcion();
        }

        try (PrintWriter out = response.getWriter()) {
            out.print(result);
        }

    }

    protected void confirmar_transferencia_cuenta_tercero(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");

        request.getSession().setAttribute("rutaServlet", "CuentaTerceroServlet-confirmar_transferencia_cuenta_tercero");

        String monto = request.getParameter("monto");
        String cuenta_destino = request.getParameter("cuenta_destino");
        String codigo_moneda = request.getParameter("codigoMoneda");
        String result = "";

        String token = (String) request.getSession().getAttribute("r_tokenAcceso");
        String tipo_afiliacion = (String) request.getSession().getAttribute("r_tipoAfilicacion");
        String numero_cuenta_origen = (String) request.getSession().getAttribute("numero_cuenta_origen");
        List lista_val_destino;

        String numero_tarjeta = (String) request.getSession().getAttribute("r_numeroTarjeta");
        String codigo_sub_menu = "AP02";

        CuentaTerceroService service = new CuentaTerceroService();
        Result resultadoWS = service.val_cuenta_destino(cuenta_destino, token);

        if (resultadoWS.getTipoMensaje() == 0) {

            lista_val_destino = (LinkedList) metodosGlobales.read2(resultadoWS.getResultado());

            String nombre_destino_val = (String) lista_val_destino.get(0);
            String cuenta_destino_val = (String) lista_val_destino.get(1);
            String columna = "";
            String fila = "";

            String[] confirmar = new String[6];

            confirmar[0] = numero_cuenta_origen;
            confirmar[1] = cuenta_destino_val;
            confirmar[2] = nombre_destino_val;
            confirmar[3] = codigo_moneda;
            confirmar[4] = monto;
            confirmar[5] = "";

            String numero_columna = "";

            String numero_fila = "";

            ArrayList<String> afiliacion_tipo = new ArrayList<>();
            afiliacion_tipo = metodosGlobales.tipo_afiliacion(tipo_afiliacion);
            confirmar[5] = afiliacion_tipo.get(2);

            request.getSession().setAttribute("confirmacion_cuentas_terceros", confirmar);

            String[] coordenadas = new String[2];
            String codigo_solicitud = "";

            Object obj_Flag = request.getSession().getAttribute("FLAG_OP_SERVLET");

            Boolean flag_servlet = (Boolean) obj_Flag;

            Boolean estado_frecuente_confirmar = (Boolean) Boolean.parseBoolean(request.getSession().getAttribute("r_estado_frecuente").toString());

            if (flag_servlet == false || estado_frecuente == false) {

                estado_frecuente_confirmar = false;
                if (tipo_afiliacion.equals("A")) {

                    coordenadas[0] = afiliacion_tipo.get(0);
                    coordenadas[1] = afiliacion_tipo.get(1);

                    columna = afiliacion_tipo.get(0);
                    fila = afiliacion_tipo.get(1);

                    // web/login/funciones/confirmacion.jsp
                    result = resultadoWS.getTipoMensaje() + "%%%" + "confirmacion";

                } else {

                    Result resultadoWS2 = service.clave_dinamica(numero_tarjeta, codigo_sub_menu, monto, token);
                    if (resultadoWS2.getTipoMensaje() == 0) {
                        List dinamica = (LinkedList) metodosGlobales.read2(resultadoWS2.getResultado());

                        codigo_solicitud = (String) dinamica.get(0);

                        coordenadas[0] = "";
                        coordenadas[1] = "";

                        columna = "";
                        fila = "";
                        // web/login/funciones/confirmacion.jsp
                        result = resultadoWS2.getTipoMensaje() + "%%%" + "confirmacion";
                    } else {
                        result = resultadoWS2.getTipoMensaje() + "%%%" + resultadoWS2.getDescripcion();
                    }
                }
                request.getSession().setAttribute("flag_clave", tipo_afiliacion);

            } else {
                //                 web/login/funciones/confirmacion.jsp 

                result = resultadoWS.getTipoMensaje() + "%%%" + "confirmacion";
            }
            request.getSession().setAttribute("estado_frecuente_confirmar", estado_frecuente_confirmar);
            request.getSession().setAttribute("coordenadas", coordenadas);

            request.getSession().setAttribute("columna", columna);
            request.getSession().setAttribute("fila", fila);

            request.getSession().setAttribute("codigo_solicitud", codigo_solicitud);
            request.getSession().setAttribute("monto", monto);
            request.getSession().setAttribute("cuenta_destino_val", cuenta_destino_val);
            request.getSession().setAttribute("nombre_destino_val", nombre_destino_val);

        } else {
            result = resultadoWS.getTipoMensaje() + "%%%" + resultadoWS.getDescripcion();
        }
        try (PrintWriter out = response.getWriter()) {
            out.print(result);
        }

    }

    protected void constancia_transferencia_cuenta_tercero(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");

        request.getSession().setAttribute("rutaServlet", "CuentaTerceroServlet-constancia_transferencia_cuenta_tercero");

        String numero_cuenta_origen = (String) request.getSession().getAttribute("numero_cuenta_origen");
        String cuenta_destino_val = (String) request.getSession().getAttribute("cuenta_destino_val");
        String tipoAfiliacion = (String) request.getSession().getAttribute("r_tipoAfilicacion");
        String monto = (String) request.getSession().getAttribute("monto");
        List lista_contancia_tercero;

        String ope_frecuente = request.getParameter("ope_frecuente");//luz de casa
        String alias_ope = "transPropias";
        String estado_ope = request.getParameter("estado_ope");//true or false
        String ip_publica_movil = MetodosGlobales.getClientIpAddr(request);
        String columna;
        String fila;
        Object obj_Flag = request.getSession().getAttribute("FLAG_OP_SERVLET");
        Boolean flag_servlet = (Boolean) obj_Flag;

        if (flag_servlet == false) {
            columna = (String) request.getSession().getAttribute("columna");
            fila = (String) request.getSession().getAttribute("fila");
        } else {
            columna = "";
            fila = "";
        }
        String codigo_solicitud = (String) request.getSession().getAttribute("codigo_solicitud");

        String numero_tarjeta = (String) request.getSession().getAttribute("r_numeroTarjeta");
        String codigo_cliente = (String) request.getSession().getAttribute("r_codigoCliente");
        String token = (String) request.getSession().getAttribute("r_tokenAcceso");

        String codigo_sub_menu = "AP02";
        String tipo_afiliacion = tipoAfiliacion;

        String clave_dinamica = "";

        String clave_coordenada = "";

        String numero_columna = columna;

        String numero_fila = fila;

        String result = "";

        Boolean estado_frecuente_confirmar = (Boolean) Boolean.parseBoolean(request.getSession().getAttribute("estado_frecuente_confirmar").toString());

        if (flag_servlet == false || estado_frecuente_confirmar == false) {
            if (tipo_afiliacion.equals("D")) {

                String clave_dinamica2 = request.getParameter("clave");
                clave_dinamica = clave_dinamica2;

            } else if (tipo_afiliacion.equals("A")) {

                String clave_coordenada2 = request.getParameter("clave");
                clave_coordenada = clave_coordenada2;
                numero_columna = (String) request.getSession().getAttribute("columna");
                numero_fila = (String) request.getSession().getAttribute("fila");

            }
        }

        CuentaTerceroService service = new CuentaTerceroService();
        Result resultadoWS = service.constancia_cuenta_tercero_destino_detalle(numero_cuenta_origen,
                cuenta_destino_val,
                monto,
                estado_ope,
                ope_frecuente,
                tipo_afiliacion,
                numero_tarjeta,
                codigo_cliente,
                clave_dinamica,
                codigo_solicitud,
                clave_coordenada,
                numero_columna,
                numero_fila,
                ip_publica_movil,
                codigo_sub_menu,
                token,
                estado_frecuente_confirmar.toString());

        if (resultadoWS.getTipoMensaje() == 0) {

            lista_contancia_tercero = (LinkedList) metodosGlobales.read2(resultadoWS.getResultado());

            request.getSession().setAttribute("lista_contancia", lista_contancia_tercero);

            // web/login/funciones/constancia.jsp
            result = resultadoWS.getTipoMensaje() + "%%%" + "constancia";
            request.getSession().setAttribute("inputs_AP02", null);

            JsonObjectWaka arrayCabecera = new JsonObjectWaka();

            arrayCabecera.addObject("cod", lista_contancia_tercero.get(0).toString())
                    .addObject("fecha", lista_contancia_tercero.get(1).toString())
                    .addObject("hora", lista_contancia_tercero.get(2).toString())
                    .addObject("cuenta", lista_contancia_tercero.get(3).toString())
                    .addObject("destino", lista_contancia_tercero.get(4).toString())
                    .addObject("datos", lista_contancia_tercero.get(5).toString())
                    .addObject("monedamonto", lista_contancia_tercero.get(6).toString() + " " + metodosGlobales.formato_monto(lista_contancia_tercero.get(7).toString()));

            request.getSession().setAttribute("compartir_cuenta_tceros", arrayCabecera.getString());

        } else {
            result = resultadoWS.getTipoMensaje() + "%%%" + resultadoWS.getDescripcion();
        }

        try (PrintWriter out = response.getWriter()) {
            out.print(result);
        }

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
