package web.servlet;

import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import service.UbicacionService;
import util.MetodosGlobales;
import util.Result;

@WebServlet(name = "UbicanosServlet", urlPatterns = {"/UbicanosServlet"})
public class UbicanosServlet extends HttpServlet {

    MetodosGlobales metodosGlobales = new MetodosGlobales();

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        metodosGlobales.validar_dispositivo(request, response);
        
        String accion = request.getParameter("accion");

        
            if (accion.equals("listar_datos_mapa")) {
                listar_ubicacion(request, response);

            }

    }

    protected void listar_ubicacion(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        UbicacionService ubicacionService = new UbicacionService();
        Result resultadoWSTodos = ubicacionService.listar_todos();

        if (resultadoWSTodos.getTipoMensaje() == 0) {

            req.setAttribute("array_oficina_mapa_todos", resultadoWSTodos.getResultado());

        }
        else{
            req.setAttribute("array_oficina_mapa_todos", "[]");
        }

        Result resultadoWSOficinas = ubicacionService.listar_oficinas();

        if (resultadoWSOficinas.getTipoMensaje() == 0) {
            req.setAttribute("array_oficina_mapa_oficinas", resultadoWSOficinas.getResultado());

        }
        else{
            req.setAttribute("array_oficina_mapa_oficinas", "[]");
        }

        Result resultadoWSAgentes = ubicacionService.listar_agentes();

        if (resultadoWSAgentes.getTipoMensaje() == 0) {
            req.setAttribute("array_oficina_mapa_agentes", resultadoWSAgentes.getResultado());

        }
        else{
            req.setAttribute("array_oficina_mapa_agentes", "[]");
        }
        
        Result resultadoWSCajeros = ubicacionService.listar_cajeros();

        if (resultadoWSCajeros.getTipoMensaje() == 0) {
            req.setAttribute("array_oficina_mapa_cajeros", resultadoWSCajeros.getResultado());

        }
        else{
            req.setAttribute("array_oficina_mapa_cajeros", "[]");
        }
        

        RequestDispatcher dispatcher = req.getRequestDispatcher("web/menu_publico/ubicanos.jsp");
        dispatcher.forward(req, resp);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
