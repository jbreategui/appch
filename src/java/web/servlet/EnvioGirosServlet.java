package web.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import service.CuentaPropiaService;
import service.EnvioGirosService;
import service.PagoCreditoTerceroService;
import util.JsonObjectWaka;
import util.MetodosGlobales;
import util.Result;

@WebServlet(name = "EnvioGirosServlet", urlPatterns = {"/EnvioGirosServlet"})
public class EnvioGirosServlet extends HttpServlet {

    MetodosGlobales metodosGlobales = new MetodosGlobales();
    Boolean estado_frecuente = null;

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        response.setContentType("text/html;charset=UTF-8");
        
        metodosGlobales.validar_dispositivo(request, response);
        ArrayList<String> afiliacion_validador = new ArrayList<>();
        afiliacion_validador = metodosGlobales.tipo_afiliacion((String) request.getSession().getAttribute("r_tipoAfilicacion"));

        if ((afiliacion_validador.get(0)).equals("false")) {

            String result;
            result = afiliacion_validador.get(2) + "%%%" + afiliacion_validador.get(1);
            try (PrintWriter out = response.getWriter()) {
                out.print(result);
            }

        } else {
            String accion = request.getParameter("accion");

            String ID = (String) request.getSession().getAttribute("ID");
            if ((ID == null) || (!ID.equals(request.getSession().getId()))) {
                try (PrintWriter out = response.getWriter()) {
                    String result = "0" + "%%%" + "index";
                    out.print(result);
                }

            } else {
                request.getSession().setAttribute("rutaServlet", "EnvioGirosServlet");
                estado_frecuente = (Boolean) Boolean.parseBoolean(request.getSession().getAttribute("r_estado_frecuente").toString());

                switch (accion) {
                    case "listar_agencias":
                        listar_agencias(request, response);
                        break;
                    case "ver_confirmacion":
                        ver_confirmacion(request, response);
                        break;
                    case "registrar_giro":
                        registrar_giro(request, response);
                        break;
                    case "elegir_cuenta_cargo":
                        elegir_cuenta_cargo(request, response);
                        break;
                    default:
                        break;
                }
            }
        }
    }

    protected void elegir_cuenta_cargo(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String codigo_cliente = (String) request.getSession().getAttribute("r_codigoCliente");
        String token = (String) request.getSession().getAttribute("r_tokenAcceso");//cambiar
        CuentaPropiaService cps = new CuentaPropiaService();
        Result resultadoWS = cps.listar_cuenta_propia_detalle(codigo_cliente, "P", token);
        String result;
        request.getSession().setAttribute("FLAG_OP_SERVLET", false);
        request.getSession().setAttribute("inputs_AP09", null);
        if (resultadoWS.getTipoMensaje() == 0) {

            List<Object[]> lista = new LinkedList<>();
            lista = metodosGlobales.read3(resultadoWS.getResultado());

            request.getSession().setAttribute("elegir_cuenta_giros", lista);
            // web/login/funciones/elegir_cuenta.jsp
            result = resultadoWS.getTipoMensaje() + "%%%" + "elegirCuenta";

        } else {

            result = resultadoWS.getTipoMensaje() + "%%%" + resultadoWS.getDescripcion();

        }

        try (PrintWriter out = response.getWriter()) {
            out.print(result);
        }

    }

    protected void listar_agencias(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        EnvioGirosService service = new EnvioGirosService();
        Result resultadoWS = service.listar_agencias();
        String result;
       
        String type=metodosGlobales.validar_dispositivo_teclado(req, resp);

        Object obj_Flag = req.getSession().getAttribute("FLAG_OP_SERVLET");
        Boolean flag_servlet = (Boolean) obj_Flag;
        String numeroctacargo = "";
        String codigo_moneda = "";
        if (flag_servlet == false) {
            List<Object[]> array_lista_cuenta_ori = new LinkedList<>();
            array_lista_cuenta_ori = (List<Object[]>) req.getSession().getAttribute("elegir_cuenta_giros");

            String indicador_cuenta = req.getParameter("indicador_cuenta");
            Integer ind = Integer.parseInt(indicador_cuenta);
            Object[] cuenta_total = array_lista_cuenta_ori.get(ind);
            numeroctacargo = (String) cuenta_total[0];
            codigo_moneda = (String) cuenta_total[3];
        } else {
            List sesion = (List) req.getSession().getAttribute("inputs_AP09");

            numeroctacargo = sesion.get(3).toString();
            codigo_moneda = sesion.get(4).toString();
        }

        String moneda = "";
        if (codigo_moneda.compareTo("1") == 0) {
            moneda = "S/";
        } else {
            moneda = "USD";
        }

        if (resultadoWS.getTipoMensaje() == 0) {

            Object[] lista = (Object[]) metodosGlobales.read2(resultadoWS.getResultado());
            req.getSession().setAttribute("moneda_giro", moneda);
            req.getSession().setAttribute("numeroctacargo_giro", numeroctacargo);
            req.getSession().setAttribute("lista_agencia", lista);
            req.getSession().setAttribute("type", type);

            //  web/login/resumen_cuentas/pagos/datos_giro.jsp
            result = resultadoWS.getTipoMensaje() + "%%%" + "datos_giro";

        } else {

            result = resultadoWS.getTipoMensaje() + "%%%" + resultadoWS.getDescripcion();

        }

        try (PrintWriter out = resp.getWriter()) {
            out.print(result);
        }

    }

    protected void ver_confirmacion(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        PagoCreditoTerceroService egs = new PagoCreditoTerceroService();
        String codigo_sub_menu = "AP09";
        String numero_tarjeta = (String) request.getSession().getAttribute("r_numeroTarjeta");
        String tipo_afiliacion = (String) request.getSession().getAttribute("r_tipoAfilicacion");
        String token = (String) request.getSession().getAttribute("r_tokenAcceso");

        ArrayList<String> coordenada_clave = metodosGlobales.tipo_afiliacion(tipo_afiliacion);

        String numerotarjetacargo = (String) request.getSession().getAttribute("numeroctacargo_giro");
        String apellido_paterno = request.getParameter("apellido_paterno");
        String apellido_materno = request.getParameter("apellido_materno");
        String nombres = request.getParameter("nombres");
        String dni = request.getParameter("dni");
        String telefono = request.getParameter("telefono");
        String agencia = request.getParameter("agencia");
        String nombre_agencia = request.getParameter("nombre_agencia");
        String monto = request.getParameter("monto");

        Object[] objetosDatos = {apellido_paterno, apellido_materno, nombres, dni, telefono, agencia, monto, numerotarjetacargo};

        String cuentaCargo = numerotarjetacargo;
        String beneficiario = apellido_paterno + " " + apellido_materno + ", " + nombres;
        String monedaMonto = request.getSession().getAttribute("moneda_giro") + " " + metodosGlobales.formato_monto(monto);

        String result = "";

        boolean flag = true;
        Object obj_Flag = request.getSession().getAttribute("FLAG_OP_SERVLET");

        Boolean flag_servlet = (Boolean) obj_Flag;

        Boolean estado_frecuente_confirmar = (Boolean) Boolean.parseBoolean(request.getSession().getAttribute("r_estado_frecuente").toString());
        if (flag_servlet == false || estado_frecuente == false) {

            estado_frecuente_confirmar = false;
            if (tipo_afiliacion.compareTo("D") == 0) {
                Result r = egs.retornar_codigo_operacion(numero_tarjeta, codigo_sub_menu, monto, token);
                if (r.getTipoMensaje() == 0) {
                    List codigoSolicitud = (List) metodosGlobales.read2(r.getResultado());
                    request.getSession().setAttribute("giro_codigoSolicitud", codigoSolicitud.get(0));
                    System.out.println(codigoSolicitud.get(0));
                } else {
                    result = r.getTipoMensaje() + "%%%" + r.getDescripcion();
                    flag = false;
                }
            } else if (tipo_afiliacion.compareTo("A") == 0) {

                request.getSession().setAttribute("giro_columna", coordenada_clave.get(0));
                request.getSession().setAttribute("giro_fila", coordenada_clave.get(1));

            }
            request.getSession().setAttribute("flag_clave", tipo_afiliacion);
        }
        request.getSession().setAttribute("estado_frecuente_confirmar", estado_frecuente_confirmar);
        if (flag) {
            request.getSession().setAttribute("objetosDatos", objetosDatos);
            Object[] obj = {cuentaCargo, beneficiario, dni, nombre_agencia, monedaMonto, coordenada_clave.get(2)};
            request.getSession().setAttribute("confirmacion_giros", obj);
            // web/login/funciones/confirmacion.jsp
            result = "0" + "%%%" + "confirmacion";
        }
        try (PrintWriter out = response.getWriter()) {
            out.print(result);
        }

    }

    protected void registrar_giro(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String token = (String) request.getSession().getAttribute("r_tokenAcceso");
        String clave_dinamica = (request.getParameter("coordenada") == null) ? "" : request.getParameter("coordenada");

        String result;
        Object[] objetosDatos = (Object[]) request.getSession().getAttribute("objetosDatos");

        String codigo_cuenta_origen = objetosDatos[7].toString();
        String monto_envio = objetosDatos[6].toString();
        String codigo_oficina_destino = objetosDatos[5].toString();

        String estado_operacion_frecuente = "false";

        String alias_operacion = "";

        String apellido_paterno = objetosDatos[0].toString();
        String apellido_materno = objetosDatos[1].toString();
        String nombres_beneficiario = objetosDatos[2].toString();
        String dni_beneficiario = objetosDatos[3].toString();
        String celular_beneficiario = objetosDatos[4].toString();

        String codigo_solicitud = "";

        String tipo_afiliacion = (String) request.getSession().getAttribute("r_tipoAfilicacion");
        String numero_tarjeta = (String) request.getSession().getAttribute("r_numeroTarjeta");
        String codigo_cliente = (String) request.getSession().getAttribute("r_codigoCliente");
        String claveDinamica = clave_dinamica;
        String claveCordenada = clave_dinamica;
        String numero_columna = "";
        String numero_fila = "";

        String ip_publica_movil = MetodosGlobales.getClientIpAddr(request);
        String codigo_sub_menu = "AP09";

        Object obj_Flag = request.getSession().getAttribute("FLAG_OP_SERVLET");
        Boolean flag_servlet = (Boolean) obj_Flag;
        Boolean estado_frecuente_confirmar = (Boolean) Boolean.parseBoolean(request.getSession().getAttribute("estado_frecuente_confirmar").toString());
        if (flag_servlet == false || estado_frecuente_confirmar == false) {
            if (tipo_afiliacion.compareTo("D") == 0) {
                claveCordenada = "";
                codigo_solicitud = (String) request.getSession().getAttribute("giro_codigoSolicitud");
            } else if (tipo_afiliacion.compareTo("A") == 0) {
                claveDinamica = "";
                numero_columna = (String) request.getSession().getAttribute("giro_columna");
                numero_fila = (String) request.getSession().getAttribute("giro_fila");
            }
        }
        EnvioGirosService service = new EnvioGirosService();
        Result resultadoWS = service.registrar_giro(
                codigo_cuenta_origen,
                monto_envio,
                codigo_oficina_destino,
                estado_operacion_frecuente,
                alias_operacion,
                apellido_paterno,
                apellido_materno,
                nombres_beneficiario,
                dni_beneficiario,
                celular_beneficiario,
                tipo_afiliacion,
                numero_tarjeta,
                codigo_cliente,
                claveDinamica,
                claveCordenada,
                numero_columna,
                numero_fila,
                ip_publica_movil,
                codigo_sub_menu,
                token,
                codigo_solicitud,
                estado_frecuente_confirmar.toString()
        );

        if (resultadoWS.getTipoMensaje() == 0) {

            List lista = (List) metodosGlobales.read2(resultadoWS.getResultado());

            String codigoOperacion = lista.get(0).toString();
            String fecha = lista.get(1).toString();
            String hora = lista.get(2).toString();
            String cuentaCargo = lista.get(3).toString();
            String DNI = lista.get(4).toString();
            String datos = lista.get(5).toString();
            String destino = lista.get(6).toString();
            String moneda = lista.get(7).toString();
            String monto = lista.get(8).toString();
            String comision = lista.get(9).toString();

            Object[] constancia = {codigoOperacion, fecha, hora, cuentaCargo, DNI, datos, moneda, metodosGlobales.formato_monto(monto), destino, comision};

            request.getSession().setAttribute("constancia", constancia);
            request.getSession().setAttribute("inputs_AP09", null);
            
            // web/login/funciones/constancia.jsp
            result = resultadoWS.getTipoMensaje() + "%%%" + "constancia";

            JsonObjectWaka arrayCabecera = new JsonObjectWaka();
            arrayCabecera.addObject("cod", codigoOperacion)
                    .addObject("fecha", fecha)
                    .addObject("hora", hora)
                    .addObject("cuenta", cuentaCargo)
                    .addObject("dni", DNI)
                    .addObject("datos", datos)
                    .addObject("monedamonto", moneda + " " + metodosGlobales.formato_monto(monto))
                    .addObject("destino", destino)
                    .addObject("comision", moneda + " " + metodosGlobales.formato_monto(comision));

            request.getSession().setAttribute("compartir_giro", arrayCabecera.getString());

        } else {
            result = resultadoWS.getTipoMensaje() + "%%%" + resultadoWS.getDescripcion();
        }
        try (PrintWriter out = response.getWriter()) {
            out.print(result);
        }

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
