package web.servlet;

import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import util.MetodosGlobales;

@WebServlet(name = "ControladorServlet", urlPatterns = {"/cs"})
public class ControladorServlet extends HttpServlet {

    
    
    MetodosGlobales metodosGlobales = new MetodosGlobales();

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        metodosGlobales.validar_dispositivo(request, response);
        controlador(request, response);
    }

    private void controlador(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String accion = request.getParameter("accion");
        switch (accion) {
            case "login": {
                login(request, response);
                break;
            }

            case "index": {
                index(request, response);
                break;
            }
            case "constancia": {
                constancia(request, response);
                break;
            }
            case "bloqueoTarjeta": {
                bloqueoTarjeta(request, response);
                break;
            }
            case "huellaDigital": {
                huellaDigital(request, response);
                break;
            }
            case "terminosHuella": {
                terminosHuella(request, response);
                break;
            }
            case "configuraTarjeta": {
                configuraTarjeta(request, response);
                break;
            }
            case "otrasOpciones": {
                otrasOpciones(request, response);
                break;
            }
            case "elegirCuenta": {
                elegirCuenta(request, response);
                break;
            }
            case "ingresar_monto_cuenta_interbancaria": {
                ingresar_monto_cuenta_interbancaria(request, response);
                break;
            }
            case "abono": {
                abono(request, response);
                break;
            }
            case "confirmacion": {
                confirmacion(request, response);
                break;
            }
       
            case "ejecutaTransferencia": {
                ejecutaTransferencia(request, response);
                break;
            }
            case "transferencia": {
                transferencia(request, response);
                break;
            }
            case "ingresar_monto_cuenta_destino": {
                ingresar_monto_cuenta_destino(request, response);
                break;
            }
            case "detalle_ahorro": {
                detalle_ahorro(request, response);
                break;
            }
            case "detalle_credito": {
                detalle_credito(request, response);
                break;
            }
            case "datos_giro": {
                datos_giro(request, response);
                break;
            }
            case "perfil": {
                perfil(request, response);
                break;
            }
            case "informacion": {
                informacion(request, response);
                break;
            }
            case "servicioCliente": {
                servicioCliente(request, response);
                break;
            }
            case "configuraciones": {
                configuraciones(request, response);
                break;
            }
            case "elegirTipopago": {
                elegirTipopago(request, response);
                break;
            }
            case "elegirCuenta_credito": {
                elegirCuenta_credito(request, response);
                break;
            }
            case "elegirCuotaMonto": {
                elegirCuotaMonto(request, response);
                break;
            }
            case "pagos": {
                pagos(request, response);
                break;
            }
            case "creditoPagar": {
                creditoPagar(request, response);
                break;
            }
            case "pagoInstituciones": {
                pagoInstituciones(request, response);
                break;
            }
            case "recargaCelular": {
                recargaCelular(request, response);
                break;
            }
            case "datosPago": {
                datosPago(request, response);
                break;
            }
            case "resumenCuentas": {
                resumenCuentas(request, response);
                break;
            }
            case "operacionesFrecuentes": {
                operacionesFrecuentes(request, response);
                break;
            }
      
            case "operaciones_recuentes": {
                operaciones_recuentes(request, response);
                break;
            }
      

            default: {
                break;
            }

        }
    }

    protected void login(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");

        RequestDispatcher rd = request
                .getRequestDispatcher("/WEB-INF/web/login/login.jsp");
        rd.forward(request, response);

    }

    protected void index(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");

        RequestDispatcher rd = request
                .getRequestDispatcher("/WEB-INF/index.jsp");
        rd.forward(request, response);

    }

    protected void constancia(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");

        RequestDispatcher rd = request
                .getRequestDispatcher("/WEB-INF/web/login/funciones/constancia.jsp");
        rd.forward(request, response);

    }

    protected void bloqueoTarjeta(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");

        RequestDispatcher rd = request
                .getRequestDispatcher("/WEB-INF/web/menu_publico/bloquear_tarjeta.jsp");
        rd.forward(request, response);

    }

    protected void huellaDigital(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");

        RequestDispatcher rd = request
                .getRequestDispatcher("/WEB-INF/web/login/resumen_cuentas/otras_opciones/huella_digital.jsp");
        rd.forward(request, response);

    }

    protected void terminosHuella(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");

        RequestDispatcher rd = request
                .getRequestDispatcher("/WEB-INF/web/login/resumen_cuentas/otras_opciones/terminos_huella.jsp");
        rd.forward(request, response);

    }

    protected void configuraTarjeta(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");

        RequestDispatcher rd = request
                .getRequestDispatcher("/WEB-INF/web/login/resumen_cuentas/otras_opciones/configura_tarjeta.jsp");
        rd.forward(request, response);

    }

    protected void otrasOpciones(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");

        RequestDispatcher rd = request
                .getRequestDispatcher("/WEB-INF/web/login/resumen_cuentas/otras_opciones.jsp");
        rd.forward(request, response);

    }

    protected void elegirCuenta(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");

        RequestDispatcher rd = request
                .getRequestDispatcher("/WEB-INF/web/login/funciones/elegir_cuenta.jsp");
        rd.forward(request, response);

    }

    protected void ingresar_monto_cuenta_interbancaria(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");

        RequestDispatcher rd = request
                .getRequestDispatcher("/WEB-INF/web/login/resumen_cuentas/ingresar_monto_cuenta_interbancaria.jsp");
        rd.forward(request, response);

    }

    protected void abono(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");

        RequestDispatcher rd = request
                .getRequestDispatcher("/WEB-INF/web/login/funciones/abono.jsp");
        rd.forward(request, response);

    }

    protected void confirmacion(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");

        RequestDispatcher rd = request
                .getRequestDispatcher("/WEB-INF/web/login/funciones/confirmacion");
        rd.forward(request, response);

    }

 

    protected void ejecutaTransferencia(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");

        RequestDispatcher rd = request
                .getRequestDispatcher("/WEB-INF/web/login/resumen_cuentas/ejecuta_transferencia.jsp");
        rd.forward(request, response);

    }

    protected void transferencia(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");

        RequestDispatcher rd = request
                .getRequestDispatcher("/WEB-INF/web/login/resumen_cuentas/transferencias.jsp");
        rd.forward(request, response);

    }
    protected void ingresar_monto_cuenta_destino(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");

        RequestDispatcher rd = request
                .getRequestDispatcher("/WEB-INF/web/login/resumen_cuentas/ingresar_monto_cuenta_destino.jsp");
        rd.forward(request, response);

    }
    protected void detalle_ahorro(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");

        RequestDispatcher rd = request
                .getRequestDispatcher("/WEB-INF/web/login/resumen_cuentas/detalle_ahorro.jsp");
        rd.forward(request, response);

    }
    protected void detalle_credito(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");

        RequestDispatcher rd = request
                .getRequestDispatcher("/WEB-INF/web/login/resumen_cuentas/detalle_credito.jsp");
        rd.forward(request, response);

    }
    protected void datos_giro(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");

        RequestDispatcher rd = request
                .getRequestDispatcher("/WEB-INF/web/login/resumen_cuentas/pagos/datos_giro.jsp");
        rd.forward(request, response);

    }
    protected void perfil(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");

        RequestDispatcher rd = request
                .getRequestDispatcher("/WEB-INF/web/login/resumen_cuentas/otras_opciones/perfil.jsp");
        rd.forward(request, response);

    }
    protected void informacion(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");

        RequestDispatcher rd = request
                .getRequestDispatcher("/WEB-INF/web/login/resumen_cuentas/otras_opciones/informacion.jsp");
        rd.forward(request, response);

    }
    protected void servicioCliente(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");

        RequestDispatcher rd = request
                .getRequestDispatcher("/WEB-INF/web/login/resumen_cuentas/otras_opciones/servicio_cliente.jsp");
        rd.forward(request, response);

    }
    protected void configuraciones(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");

        RequestDispatcher rd = request
                .getRequestDispatcher("/WEB-INF/web/login/resumen_cuentas/otras_opciones/configuraciones.jsp");
        rd.forward(request, response);

    }
    protected void elegirTipopago(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");

        RequestDispatcher rd = request
                .getRequestDispatcher("/WEB-INF/web/login/resumen_cuentas/pagos/elegir_tipopago.jsp");
        rd.forward(request, response);

    }
    protected void elegirCuenta_credito(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");

        RequestDispatcher rd = request
                .getRequestDispatcher("/WEB-INF/web/login/funciones/elegir_cuenta_credito.jsp");
        rd.forward(request, response);

    }
    protected void elegirCuotaMonto(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");

        RequestDispatcher rd = request
                .getRequestDispatcher("/WEB-INF/web/login/funciones/elegir_cuota_monto.jsp");
        rd.forward(request, response);

    }
    protected void pagos(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");

        RequestDispatcher rd = request
                .getRequestDispatcher("/WEB-INF/web/login/resumen_cuentas/pagos.jsp");
        rd.forward(request, response);

    }
    protected void creditoPagar(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");

        RequestDispatcher rd = request
                .getRequestDispatcher("/WEB-INF/web/login/resumen_cuentas/pagos/ingresar_credito_pagar.jsp");
        rd.forward(request, response);

    }
    protected void pagoInstituciones(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");

        RequestDispatcher rd = request
                .getRequestDispatcher("/WEB-INF/web/login/resumen_cuentas/pagos/pago_instituciones.jsp");
        rd.forward(request, response);

    }
    protected void recargaCelular(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");

        RequestDispatcher rd = request
                .getRequestDispatcher("/WEB-INF/web/login/resumen_cuentas/pagos/recarga_celular.jsp");
        rd.forward(request, response);

    }
    protected void datosPago(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");

        RequestDispatcher rd = request
                .getRequestDispatcher("/WEB-INF/web/login/resumen_cuentas/pagos/datos_pago.jsp");
        rd.forward(request, response);

    }
    protected void resumenCuentas(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");

        RequestDispatcher rd = request
                .getRequestDispatcher("/WEB-INF/web/login/resumen_cuentas.jsp");
        rd.forward(request, response);

    }
    protected void operacionesFrecuentes(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");

        RequestDispatcher rd = request
                .getRequestDispatcher("/WEB-INF/web/login/funciones/operaciones_frecuentes.jsp");
        rd.forward(request, response);

    }
    protected void operaciones_recuentes(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");

        RequestDispatcher rd = request
                .getRequestDispatcher("/WEB-INF/web/login/resumen_cuentas/otras_opciones/operaciones_frecuentes.jsp");
        rd.forward(request, response);

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
