package web.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import service.MiPerfilService;
import service.PagoCreditoTerceroService;
import util.MetodosGlobales;
import util.Result;

@WebServlet(name = "MiPerfilServlet", urlPatterns = {"/MiPerfilServlet"})
public class MiPerfilServlet extends HttpServlet {

    MetodosGlobales metodosGlobales = new MetodosGlobales();
    Boolean estado_frecuente = null;

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        response.setContentType("text/html;charset=UTF-8");
        
        metodosGlobales.validar_dispositivo(request, response);

        String accion = request.getParameter("accion");

        String ID = (String) request.getSession().getAttribute("ID");
        if ((ID == null) || (!ID.equals(request.getSession().getId()))) {
            try (PrintWriter out = response.getWriter()) {
                String result = "0" + "%%%" + "index";
                out.print(result);
            }

        } else {

            request.getSession().setAttribute("rutaServlet", "MiPerfilServlet");

            switch (accion) {
                case "obtener_email":
                    obtener_email(request, response);
                    break;
                case "registrar":
                    registrar(request, response);
                    break;
                case "informacion":
                    informacion(request, response);
                    break;
                case "servicioliente":
                    servicioliente(request, response);
                    break;
                case "configuraciones":
                    configuraciones(request, response);
                    break;
                default:
                    break;
            }
        }

    }

    protected void configuraciones(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String result = "";

        String validador = (String) request.getSession().getAttribute("validador");
        request.getSession().setAttribute("validador", validador);

        result = "0" + "%%%" + "configuraciones";
        try (PrintWriter out = response.getWriter()) {
            out.print(result);
        }

    }

    protected void informacion(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String result = "";

        result = "0" + "%%%" + "informacion";

        try (PrintWriter out = response.getWriter()) {
            out.print(result);
        }
    }

    protected void servicioliente(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String result = "";
 // web/login/resumen_cuentas/otras_opciones/servicio_cliente.jsp
        result = "0" + "%%%" + "servicioCliente";

        try (PrintWriter out = response.getWriter()) {
            out.print(result);
        }

    }

    protected void obtener_email(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        
        
        ArrayList<String> afiliacion_validador = new ArrayList<>();
        afiliacion_validador = metodosGlobales.tipo_afiliacion((String) request.getSession().getAttribute("r_tipoAfilicacion"));

        if ((afiliacion_validador.get(0)).equals("false")) {

            String result;
            result = afiliacion_validador.get(2) + "%%%" + afiliacion_validador.get(1);
            try (PrintWriter out = response.getWriter()) {
                out.print(result);
            }

        } else {

            String result = "";

            estado_frecuente = (Boolean) Boolean.parseBoolean(request.getSession().getAttribute("r_estado_frecuente").toString());
            request.getSession().setAttribute("FLAG_OP_SERVLET", false);

            MiPerfilService cps = new MiPerfilService();
            String numero_tarjeta = (String) request.getSession().getAttribute("r_numeroTarjeta");
            String token = (String) request.getSession().getAttribute("r_tokenAcceso");

            Result resultadoWS = cps.get_correo(numero_tarjeta, token);
            PagoCreditoTerceroService egs = new PagoCreditoTerceroService();
            String codigo_sub_menu = "AP13";
            String tipo_afiliacion = (String) request.getSession().getAttribute("r_tipoAfilicacion");
            ArrayList<String> coordenada_clave = metodosGlobales.tipo_afiliacion(tipo_afiliacion);
            boolean flag = true;

            Object obj_Flag = request.getSession().getAttribute("FLAG_OP_SERVLET");

            Boolean flag_servlet = (Boolean) obj_Flag;

            if (flag_servlet == false || estado_frecuente == false) {
                if (tipo_afiliacion.compareTo("D") == 0) {

                    Result r = egs.retornar_codigo_operacion(numero_tarjeta, codigo_sub_menu, "0", token);

                    if (r.getTipoMensaje() == 0) {

                        List codigoSolicitud = (List) metodosGlobales.read2(r.getResultado());
                        request.getSession().setAttribute("coreo_codigoSolicitud", codigoSolicitud.get(0));

                    } else {
                        result = r.getTipoMensaje() + "%%%" + r.getDescripcion();
                        flag = false;
                    }
                } else if (tipo_afiliacion.compareTo("A") == 0) {
                    request.getSession().setAttribute("perfil_columna", coordenada_clave.get(0));
                    request.getSession().setAttribute("perfil_fila", coordenada_clave.get(1));
                }
                request.getSession().setAttribute("flag_clave", tipo_afiliacion);
            }
            if (flag) {
                List correo = (List) metodosGlobales.read2(resultadoWS.getResultado());
                request.getSession().setAttribute("correo_electronico", correo.get(0));
                request.getSession().setAttribute("mensajeclave", coordenada_clave.get(2));
                // web/login/resumen_cuentas/otras_opciones/perfil.jsp
                result = resultadoWS.getTipoMensaje() + "%%%" + "perfil";
            }

            try (PrintWriter out = response.getWriter()) {
                out.print(result);
            }

        }

    }

    protected void registrar(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String result = "";

        estado_frecuente = (Boolean) Boolean.parseBoolean(request.getSession().getAttribute("r_estado_frecuente").toString());

        String correo_cliente = request.getParameter("correo");
        String tipo_afiliacion = (String) request.getSession().getAttribute("r_tipoAfilicacion");
        String numero_tarjeta = (String) request.getSession().getAttribute("r_numeroTarjeta");
        String codigo_cliente = (String) request.getSession().getAttribute("r_codigoCliente");
        String clave_dinamica = (request.getParameter("coordenada") == null) ? "" : request.getParameter("coordenada");
        String clave_cordenada = (request.getParameter("coordenada") == null) ? "" : request.getParameter("coordenada");

        String ip_publica_movil = MetodosGlobales.getClientIpAddr(request);
        String codigo_sub_menu = "AP13";
        String token = (String) request.getSession().getAttribute("r_tokenAcceso");

        String numero_columna = "";
        String numero_fila = "";
        String codigoSolicitud = "";
        Object obj_Flag = request.getSession().getAttribute("FLAG_OP_SERVLET");
        Boolean flag_servlet = (Boolean) obj_Flag;
        if (flag_servlet == false || estado_frecuente == false) {
            if (tipo_afiliacion.compareTo("D") == 0) {
                clave_cordenada = "";
                codigoSolicitud = (String) request.getSession().getAttribute("coreo_codigoSolicitud");

            } else if (tipo_afiliacion.compareTo("A") == 0) {
                clave_dinamica = "";
                numero_columna = (String) request.getSession().getAttribute("perfil_columna");
                numero_fila = (String) request.getSession().getAttribute("perfil_fila");
            }
        }

        MiPerfilService service = new MiPerfilService();

        Result resultadoWS = service.registrar_pago_servicio(
                correo_cliente,
                tipo_afiliacion,
                numero_tarjeta,
                codigo_cliente,
                clave_dinamica,
                codigoSolicitud,
                clave_cordenada,
                numero_columna,
                numero_fila,
                ip_publica_movil,
                codigo_sub_menu,
                token,
                estado_frecuente.toString());

        if (resultadoWS.getTipoMensaje() == 0) {

            List lista = (List) metodosGlobales.read2(resultadoWS.getResultado());
            String fecha = lista.get(0).toString();
            String hora = lista.get(1).toString();
            String usofuerpais = "";
            String mail = lista.get(2).toString();

            Object[] datos = {fecha, hora, numero_tarjeta, usofuerpais, mail};
            request.getSession().setAttribute("constancia_correo", datos);
            
            // web/login/funciones/constancia.jsp
            result = resultadoWS.getTipoMensaje() + "%%%" + "constancia";

        } else {

            result = resultadoWS.getTipoMensaje() + "%%%" + resultadoWS.getDescripcion();

        }

        try (PrintWriter out = response.getWriter()) {
            out.print(result);
        }

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
