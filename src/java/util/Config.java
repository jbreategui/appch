package util;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class Config {

    public String getRuta() {
        String ruta = "";
        Properties props = new Properties();
        InputStream in;

        try {
            in = getClass().getClassLoader().getResourceAsStream("config/ruta.properties");
            props.load(in);

            ruta = props.getProperty("rutaCH").trim();
            in.close();

        } catch (IOException ex) {
        }
        return ruta;
    }
   
    public String getRutaGet() {
        String ruta = "";
        Properties props = new Properties();
        InputStream in;

        try {
            in = getClass().getClassLoader().getResourceAsStream("config/rutaSL.properties");
            props.load(in);

            ruta = props.getProperty("rutaSLGET").trim();
            in.close();

        } catch (IOException ex) {
        }
        return ruta;
    }
    
    public String getRutaPost() {
        String ruta = "";
        Properties props = new Properties();
        InputStream in;

        try {
            in = getClass().getClassLoader().getResourceAsStream("config/rutaSL.properties");
            props.load(in);

            ruta = props.getProperty("rutaSLPOST").trim();
            in.close();

        } catch (IOException ex) {
        }
        return ruta;
    }

    public Boolean getFlag() {
        Boolean ruta = null;
        Properties props = new Properties();
        InputStream in;

        try {
            in = getClass().getClassLoader().getResourceAsStream("config/ruta.properties");
            props.load(in);

            ruta = Boolean.parseBoolean(props.getProperty("flag").trim());
            in.close();

        } catch (IOException ex) {
        }
        return ruta;
    }
}
