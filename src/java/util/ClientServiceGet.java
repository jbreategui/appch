package util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class ClientServiceGet {

    public static String[] callServiceGet(String urlWS, String parametros) {

        StringBuilder result = new StringBuilder();
        String[] out = new String[2];
        String codigoMensaje = "1";
        try {
            URL url = new URL(urlWS);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setDoOutput(true);

            conn.setRequestMethod("GET");
            conn.setRequestProperty("Accept", "application/json");
            String input = parametros;
            if (input.length() > 0) {

                conn.setRequestProperty("Authorization", parametros);

            }
            if (conn.getResponseCode() != 200) {
                out[0] = codigoMensaje;
                out[1] = "Failed : HTTP error code : " + conn.getResponseCode();
                return out;
            }
            BufferedReader br = new BufferedReader(new InputStreamReader(
                    ( conn.getInputStream() ),"UTF-8" ));

            String output;
            while ((output = br.readLine()) != null) {
                result.append(output);
            }

            conn.disconnect();
            codigoMensaje = "0";
            out[0] = codigoMensaje;
            out[1] = result.toString();
        } catch (MalformedURLException e) {

            out[0] = codigoMensaje;
            out[1] = e.getMessage();
        } catch (IOException e) {

            out[0] = codigoMensaje;
            out[1] = e.getMessage();

        }
        return out;
    }

}
