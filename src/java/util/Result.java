package util;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

public class Result {

    JsonElement resultado;
    Integer tipoMensaje;
    String descripcion;

    public Result() {
    }

    public JsonElement getResultado() {
        return resultado;
    }

    public void setResultado(JsonElement resultado) {
        this.resultado = resultado;
    }

    public Result(JsonElement resultado, Integer tipoMensaje, String descripcion) {
        this.resultado = resultado;
        this.tipoMensaje = tipoMensaje;
        this.descripcion = descripcion;
    }

    public void setResultado(JsonObject resultado) {
        this.resultado = resultado;
    }

    public Integer getTipoMensaje() {
        return tipoMensaje;
    }

    public void setTipoMensaje(Integer tipoMensaje) {
        this.tipoMensaje = tipoMensaje;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

}
