package util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class ClientServicePost {

    public static String[] callServicePost(String urlWS, String parametros, String headers) {
        StringBuilder result = new StringBuilder();
        String[] out = new String[2];
        String codigoMensaje = "1";
        try {

            URL url = new URL(urlWS);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setDoOutput(true);
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setRequestProperty("Authorization", headers);
            String input = parametros;

            OutputStream os = conn.getOutputStream();
            os.write(input.getBytes("UTF-8"));
            os.flush();

            BufferedReader br = new BufferedReader(new InputStreamReader(
                    ( conn.getInputStream() ),"UTF-8" ));

            String output;

            while ((output = br.readLine()) != null) {
                result.append(output);
            }

            conn.disconnect();
            codigoMensaje = "0";
            out[0] = codigoMensaje;
            out[1] = result.toString();

        } catch (MalformedURLException e) {
            out[0] = codigoMensaje;
            out[1] = e.getMessage();

        } catch (IOException e) {
            out[0] = codigoMensaje;
            out[1] = e.getMessage();

        }
        return out;
    }

}
