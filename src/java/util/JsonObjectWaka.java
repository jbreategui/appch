package util;

import java.util.HashMap;

import java.util.Map;

public class JsonObjectWaka {

    private Map<String, Object> arrayPrincipal;

    public JsonObjectWaka() {
        arrayPrincipal = new HashMap<String, Object>();
    }

    public JsonObjectWaka addObject(String name, String value) {
        arrayPrincipal.put(name, value);
        return this;
    }

    public JsonObjectWaka addObject(String name, JsonObjectWaka value) {
        arrayPrincipal.put(name, value);
        return this;
    }

    public JsonObjectWaka addObject(String name, String[] value) {
        arrayPrincipal.put(name, value);
        return this;
    }

    public JsonObjectWaka addObject(String name, JsonObjectWaka[] value) {
        arrayPrincipal.put(name, value);
        return this;
    }


    private Map<String, Object> getMapPrincipal() {
        return arrayPrincipal;
    }

    public String getString() {
        return getString(this);

    }

    public static String getString(JsonObjectWaka jobject) {
        StringBuilder result = new StringBuilder();

        result.append("{");

        for (Map.Entry<String, Object> entry : jobject.getMapPrincipal().entrySet()) {
            result.append("\"").append(entry.getKey()).append("\":");
            Object value = entry.getValue();
            if (value instanceof String) {
                result.append("\"").append(value.toString()).append("\"");
            }
            if (value instanceof String[]) {
                result.append("[");
                for (String valor : (String[]) value) {
                    result.append("\"").append(valor).append("\",");
                }
                result.deleteCharAt(result.length() - 1);
                result.append("]");
            }
            if (value instanceof JsonObjectWaka) {
                result.append(getString((JsonObjectWaka) value));
            }
            if (value instanceof JsonObjectWaka[]) {
                result.append("[");
                for (JsonObjectWaka jo : (JsonObjectWaka[]) value) {
                    result.append(getString(jo)).append(",");
                }

                result.deleteCharAt(result.length() - 1);
                result.append("]");
            }

            result.append(",");
        }
        result.deleteCharAt(result.length() - 1);
        result.append("}");

        return result.toString();

    }

}
