package service;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import util.ClientServiceGet;
import util.MetodosGlobales;
import util.Result;


public class UbicacionService {

    public Result listar_todos() {
        String url;

        if (MetodosGlobales.flag_service) {
            
            url = MetodosGlobales.ruta_service_get +  "/Oficina/ListarOficinasMapa/TODOS";
        } else {
            url = MetodosGlobales.ruta_service + "/Oficina/ListarOficinasMapa/TODOS";
        }
        int tipoMensaje = 1;
        String mensaje;

        String[] result = ClientServiceGet.callServiceGet(url, "");
        JsonParser parser = new JsonParser();
        JsonElement contenido = null;
        if (result[0].equals("0")) {
            JsonObject jp = (JsonObject) parser.parse(result[1]);
            String estado = jp.get("Estado").getAsString();
            if (estado.equals("OK")) {
                tipoMensaje = 0;
            }
            mensaje = jp.get("Mensaje").getAsString();
            contenido = jp.get("contenido");
        } else {
            mensaje = result[1];
        }
        Result resultadoFinal = new Result(contenido, tipoMensaje, mensaje);
        return resultadoFinal;
    }

    public Result listar_oficinas() {
        String url;

        if (MetodosGlobales.flag_service) {
           
            url = MetodosGlobales.ruta_service_get + "/Oficina/ListarOficinasMapa/OFICINAS";
        } else {
            url = MetodosGlobales.ruta_service + "/Oficina/ListarOficinasMapa/OFICINAS";
        }

        int tipoMensaje = 1;
        String mensaje;

        String[] result = ClientServiceGet.callServiceGet(url, "");
        JsonParser parser = new JsonParser();
        JsonElement contenido = null;
        if (result[0].equals("0")) {
            JsonObject jp = (JsonObject) parser.parse(result[1]);
            String estado = jp.get("Estado").getAsString();
            if (estado.equals("OK")) {
                tipoMensaje = 0;
            }
            mensaje = jp.get("Mensaje").getAsString();
            contenido = jp.get("contenido");
        } else {
            mensaje = result[1];
        }
        Result resultadoFinal = new Result(contenido, tipoMensaje, mensaje);
        return resultadoFinal;
    }

    public Result listar_agentes() {
        String url;
        if (MetodosGlobales.flag_service) {
            
            url = MetodosGlobales.ruta_service_get + "/Oficina/ListarOficinasMapa/AGENTES";
        } else {
            url = MetodosGlobales.ruta_service + "/Oficina/ListarOficinasMapa/AGENTES";
        }

        int tipoMensaje = 1;
        String mensaje;

        String[] result = ClientServiceGet.callServiceGet(url, "");
        JsonParser parser = new JsonParser();
        JsonElement contenido = null;
        if (result[0].equals("0")) {
            JsonObject jp = (JsonObject) parser.parse(result[1]);
            String estado = jp.get("Estado").getAsString();
            if (estado.equals("OK")) {
                tipoMensaje = 0;
            }
            mensaje = jp.get("Mensaje").getAsString();
            contenido = jp.get("contenido");
        } else {
            mensaje = result[1];
        }
        Result resultadoFinal = new Result(contenido, tipoMensaje, mensaje);
        return resultadoFinal;
    }
    
    public Result listar_cajeros() {
        String url;
        if (MetodosGlobales.flag_service) {
            
            url = MetodosGlobales.ruta_service_get + "/Oficina/ListarOficinasMapa/CAJEROS";
        } else {
            url = MetodosGlobales.ruta_service + "/Oficina/ListarOficinasMapa/CAJEROS";
        }

        int tipoMensaje = 1;
        String mensaje;

        String[] result = ClientServiceGet.callServiceGet(url, "");
        JsonParser parser = new JsonParser();
        JsonElement contenido = null;
        if (result[0].equals("0")) {
            JsonObject jp = (JsonObject) parser.parse(result[1]);
            String estado = jp.get("Estado").getAsString();
            if (estado.equals("OK")) {
                tipoMensaje = 0;
            }
            mensaje = jp.get("Mensaje").getAsString();
            contenido = jp.get("contenido");
        } else {
            mensaje = result[1];
        }
        Result resultadoFinal = new Result(contenido, tipoMensaje, mensaje);
        return resultadoFinal;
    }

}
