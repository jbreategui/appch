
package service;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import util.ClientServiceGet;
import util.ClientServicePost;
import util.JsonObjectWaka;
import util.MetodosGlobales;
import util.Result;

public class CuentaPropiaService {
    
    public Result listar_cuenta_propia_detalle(String codigo_cliente,String tipo_operacion, String header) {
        String url;
        if (MetodosGlobales.flag_service) {
            url = MetodosGlobales.ruta_service_post;

        } else {
            url = MetodosGlobales.ruta_service + "/Cuenta/ListarCuentaOrigen";
        }
        
        
        int tipoMensaje = 1;
        String mensaje ;
       
        JsonObjectWaka jo = new JsonObjectWaka();

        jo.addObject("codigoCliente", codigo_cliente)
                .addObject("tipoOperacion", tipo_operacion);

        String[] result;

        if (MetodosGlobales.flag_service) {
            JsonObjectWaka jogeneral = new JsonObjectWaka();
            jogeneral.addObject("data", jo);
            jogeneral.addObject("url", "/Cuenta/ListarCuentaOrigen");
            result =  ClientServicePost.callServicePost(url, jogeneral.getString(), header);
        } else {
            result = ClientServicePost.callServicePost(url, jo.getString(), header);
        }
        
        
        
        JsonParser parser = new JsonParser();

        JsonElement contenido = null;
        if (result[0].equals("0")) {
            JsonObject jp = (JsonObject) parser.parse(result[1]);
            String estado = jp.get("Estado").getAsString();
            if (estado.equals("OK")) {
                tipoMensaje = 0;
            }
            mensaje = jp.get("Mensaje").getAsString();
            contenido = jp.get("contenido");
        } else {
            mensaje = result[1];
        }
        Result resultadoFinal = new Result(contenido, tipoMensaje, mensaje);
        return resultadoFinal;
    }   
    
     public Result listar_cuenta_propia_destino_detalle(String codigo_cliente,String tipo_operacion, String header) {
        String url;
        if (MetodosGlobales.flag_service) {
            url = MetodosGlobales.ruta_service_post;

        } else {
            url = MetodosGlobales.ruta_service + "/Cuenta/ListarCuentaDestino";
        }
        
        
        int tipoMensaje = 1;
        String mensaje ;
        
        JsonObjectWaka jo = new JsonObjectWaka();

        jo.addObject("codigoCliente", codigo_cliente)
                .addObject("tipoOperacion", tipo_operacion);

        String[] result ;

        if (MetodosGlobales.flag_service) {
            JsonObjectWaka jogeneral = new JsonObjectWaka();
            jogeneral.addObject("data", jo);
            jogeneral.addObject("url", "/Cuenta/ListarCuentaDestino");
            result = ClientServicePost.callServicePost(url, jogeneral.getString(), header);
        } else {
            result = ClientServicePost.callServicePost(url, jo.getString(), header);
        }
        
        
        JsonParser parser = new JsonParser();

        JsonElement contenido = null;
        if (result[0].equals("0")) {
            JsonObject jp = (JsonObject) parser.parse(result[1]);
            String estado = jp.get("Estado").getAsString();
            if (estado.equals("OK")) {
                tipoMensaje = 0;
            }
            mensaje = jp.get("Mensaje").getAsString();
            contenido = jp.get("contenido");
        } else {
            mensaje = result[1];
        }
        Result resultadoFinal = new Result(contenido, tipoMensaje, mensaje);
        return resultadoFinal;
    }
     
     public Result listar_moneda() {
        String url ;
        
        if (MetodosGlobales.flag_service) {
       
            url = MetodosGlobales.ruta_service_get + "/General/ListarMoneda";
        } else {
            url = MetodosGlobales.ruta_service + "/General/ListarMoneda";
        }
        
        int tipoMensaje = 1;

        String mensaje;

        String[] result = ClientServiceGet.callServiceGet(url, "");
        JsonParser parser = new JsonParser();
        JsonElement contenido = null;
        if (result[0].equals("0")) {
            JsonObject jp = (JsonObject) parser.parse(result[1]);
            String estado = jp.get("Estado").getAsString();
            if (estado.equals("OK")) {
                tipoMensaje = 0;
            }
            mensaje = jp.get("Mensaje").getAsString();
            contenido = jp.get("contenido");
        } else {
            mensaje = result[1];
        }
        Result resultadoFinal = new Result(contenido, tipoMensaje, mensaje);
        return resultadoFinal;
    }
     
     
      public Result constancia_cuenta_propia_destino_detalle(
                String codigo_cuenta_origen, 
                String codigo_cuenta_destino,
                String monto_operacion, 
                String estado_operacion_frecuente,
                String alias_operacion, 
                String ip_publica_movil,
                String numero_tarjeta,
                String codigo_cliente,
                String codigo_sub_menu,
                String header
      ) {
        String url;
        
        if (MetodosGlobales.flag_service) {
            url = MetodosGlobales.ruta_service_post;

        } else {
            url = MetodosGlobales.ruta_service + "/transferencia/RegistrarTransferenciasPropias";
        }
       
               
        int tipoMensaje = 1;
        String mensaje ;
        
        JsonObjectWaka jo = new JsonObjectWaka();

        jo.addObject("codigoCuentaOrigen", codigo_cuenta_origen)
                .addObject("codigoCuentaDestino", codigo_cuenta_destino)
                .addObject("montoOperacion", monto_operacion)
                .addObject("estadoOperacionFrecuente", estado_operacion_frecuente)
                .addObject("aliasOperacion", alias_operacion)
                .addObject("ipPublicaMovil", ip_publica_movil)
                .addObject("numeroTarjeta", numero_tarjeta)
                .addObject("codigoCliente", codigo_cliente)
                .addObject("codigoSubMenu", codigo_sub_menu);

        String[] result;

        if (MetodosGlobales.flag_service) {
            JsonObjectWaka jogeneral = new JsonObjectWaka();
            jogeneral.addObject("data", jo);
            jogeneral.addObject("url", "/transferencia/RegistrarTransferenciasPropias");
            result = ClientServicePost.callServicePost(url, jogeneral.getString(), header);
        } else {
            result = ClientServicePost.callServicePost(url, jo.getString(), header);
        }
        
        JsonParser parser = new JsonParser();

        JsonElement contenido = null;
        if (result[0].equals("0")) {
            JsonObject jp = (JsonObject) parser.parse(result[1]);
            String estado = jp.get("Estado").getAsString();
            if (estado.equals("OK")) {
                tipoMensaje = 0;
            }
            mensaje = jp.get("Mensaje").getAsString();
            contenido = jp.get("contenido");
        } else {
            mensaje = result[1];
        }
        Result resultadoFinal = new Result(contenido, tipoMensaje, mensaje);
        return resultadoFinal;
    }
     
     

     
    
}
