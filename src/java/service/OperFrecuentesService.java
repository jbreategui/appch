package service;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import java.util.List;
import util.ClientServiceGet;
import util.ClientServicePost;
import util.JsonObjectWaka;
import util.MetodosGlobales;
import util.Result;

public class OperFrecuentesService {

    public Result listar_oper_frecuentes(String numero_tarjeta, String token) {

        String url;
        if (MetodosGlobales.flag_service) {
          
            url = MetodosGlobales.ruta_service_get + "/Frecuente/ListarOperacionesFrecuentes/" + numero_tarjeta;
        } else {
            url = MetodosGlobales.ruta_service + "/Frecuente/ListarOperacionesFrecuentes/" + numero_tarjeta;
        }

        int tipoMensaje = 1;
        String mensaje;
        String[] result = ClientServiceGet.callServiceGet(url, token);
        JsonParser parser = new JsonParser();
        JsonElement contenido = null;
        if (result[0].equals("0")) {
            JsonObject jp = (JsonObject) parser.parse(result[1]);
            String estado = jp.get("Estado").getAsString();
            if (estado.equals("OK")) {
                tipoMensaje = 0;
            }
            mensaje = jp.get("Mensaje").getAsString();
            contenido = jp.get("contenido");
        } else {
            mensaje = result[1];
        }
        Result resultadoFinal = new Result(contenido, tipoMensaje, mensaje);
        return resultadoFinal;
    }

    public Result eliminar_operacion_frecuente(
            String tarjeta,
            String[] operaciones,
            String token
    ) {

        String url;
        if (MetodosGlobales.flag_service) {
            url = MetodosGlobales.ruta_service_post;

        } else {
            url = MetodosGlobales.ruta_service + "/Frecuente/EliminarOperacionesFrecuentes";
        }

        int tipoMensaje = 1;
        String mensaje;
        JsonObjectWaka detalles;
        JsonObjectWaka array_detalles[] = new JsonObjectWaka[operaciones.length];
        JsonObjectWaka arrayCabecera = new JsonObjectWaka();

        arrayCabecera
                .addObject("tarjeta", tarjeta);

        int c = 0;
        for (String objects : operaciones) {

            detalles = new JsonObjectWaka();
            String codigoOperacion = objects;
            detalles.addObject("codigoOperacion", codigoOperacion);
            array_detalles[c++] = detalles;

        }
        String[] result;
        if (MetodosGlobales.flag_service) {
            JsonObjectWaka jogeneral = new JsonObjectWaka();
            jogeneral.addObject("data", arrayCabecera.addObject("operaciones", array_detalles)).addObject("url", "/Frecuente/EliminarOperacionesFrecuentes");
            result = ClientServicePost.callServicePost(url, jogeneral.getString(), token);
        } else {
            arrayCabecera.addObject("operaciones", array_detalles);
            result = ClientServicePost.callServicePost(url, arrayCabecera.getString(), token);
        }
        JsonParser parser = new JsonParser();

        JsonElement contenido = null;
        if (result[0].equals("0")) {
            JsonObject jp = (JsonObject) parser.parse(result[1]);
            String estado = jp.get("Estado").getAsString();
            if (estado.equals("OK")) {
                tipoMensaje = 0;
            }
            mensaje = jp.get("Mensaje").getAsString();
            contenido = jp.get("contenido");
        } else {
            mensaje = result[1];
        }
        Result resultadoFinal = new Result(contenido, tipoMensaje, mensaje);
        return resultadoFinal;

    }

}
