package service;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import util.ClientServiceGet;
import util.ClientServicePost;
import util.JsonObjectWaka;
import util.MetodosGlobales;
import util.Result;

public class DetalleCuentaAhorroService {

    public Result listar_detalle_cuenta_ahorro(String codigo_cuenta, String token) {
        String url;

        if (MetodosGlobales.flag_service) {
          
            url = MetodosGlobales.ruta_service_get +"/Cuenta/ListarMovimientosAhorro/" + codigo_cuenta;
        } else {
            url = MetodosGlobales.ruta_service + "/Cuenta/ListarMovimientosAhorro/" + codigo_cuenta;
        }

        int tipoMensaje = 1;

        String[] result = ClientServiceGet.callServiceGet(url, token);
        String mensaje = "";
        JsonElement contenido = null;
        if (result[0].equals("0")) {
            JsonParser parser = new JsonParser();
            JsonObject jp = (JsonObject) parser.parse(result[1]);
            String estado = jp.get("Estado").getAsString();
            if (estado.equals("OK")) {
                tipoMensaje = 0;
            }
            mensaje = jp.get("Mensaje").getAsString();
            contenido = jp.get("contenido");
        } else {
            mensaje = result[1];
        }

        Result resultadoFinal = new Result(contenido, tipoMensaje, mensaje);
        return resultadoFinal;
    }

    public Result listar_detalle_cuenta_credito(String numero_tarjeta, String cuenta_credito_destino, String token) {

        
        String url ;

        if (MetodosGlobales.flag_service) {
            url = MetodosGlobales.ruta_service_post;

        } else {
            url = MetodosGlobales.ruta_service + "/Creditos/ListarDetalleCredito";
        }
        
        
        int tipoMensaje = 1;
        String mensaje;
        
        JsonObjectWaka jo = new JsonObjectWaka();

        jo.addObject("numeroTarjeta", numero_tarjeta)
                .addObject("cuentaCreditoDestino", cuenta_credito_destino);

       
        String[] result;

         if (MetodosGlobales.flag_service) {
            JsonObjectWaka jogeneral = new JsonObjectWaka();
            jogeneral.addObject("data", jo);
            jogeneral.addObject("url", "/Creditos/ListarDetalleCredito");
            result = ClientServicePost.callServicePost(url, jogeneral.getString(), token);
        } else {
            result = ClientServicePost.callServicePost(url, jo.getString(), token);
        }
        
        
        JsonParser parser = new JsonParser();

        JsonElement contenido = null;
        if (result[0].equals("0")) {
            JsonObject jp = (JsonObject) parser.parse(result[1]);
            String estado = jp.get("Estado").getAsString();
            if (estado.equals("OK")) {
                tipoMensaje = 0;
            }
            mensaje = jp.get("Mensaje").getAsString();
            contenido = jp.get("contenido");
        } else {
            mensaje = result[1];
        }
        Result resultadoFinal = new Result(contenido, tipoMensaje, mensaje);
        return resultadoFinal;

    }
}
