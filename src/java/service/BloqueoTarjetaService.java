package service;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import util.ClientServiceGet;
import util.ClientServicePost;
import util.JsonObjectWaka;
import util.MetodosGlobales;
import util.Result;


public class BloqueoTarjetaService {

    public Result bloquearTarjeta(String clave_pin,
            String ippublicamovil,
            String documento_cliente,
            String codigo_descripcion_bloqueo,
            String descripcion_bloqueo) {
       
        String url;
        if (MetodosGlobales.flag_service) {
           
            url = MetodosGlobales.ruta_service_post;
        } else {
            url = MetodosGlobales.ruta_service + "/Tarjeta/RegistrarBloqueoTarjeta";
        }
        
        int tipoMensaje = 1;
        String mensaje;
        
        JsonObjectWaka jo = new JsonObjectWaka();
        jo.addObject("clavePin", clave_pin)
                .addObject("ippublicamovil", ippublicamovil)
                .addObject("documentoCliente", documento_cliente)
                .addObject("codigoDescripcionBloqueo", codigo_descripcion_bloqueo)
                .addObject("descripcionBloqueo", descripcion_bloqueo);

       
        String[] result;
        
        if (MetodosGlobales.flag_service) {
            JsonObjectWaka jogeneral = new JsonObjectWaka();
            jogeneral.addObject("data", jo);
            jogeneral.addObject("url", "/Tarjeta/RegistrarBloqueoTarjeta");
            result = ClientServicePost.callServicePost(url, jogeneral.getString(), "");
        } else {
            result = ClientServicePost.callServicePost(url, jo.getString(), "");
        }
        

        JsonParser parser = new JsonParser();

        JsonElement contenido = null;
        if (result[0].equals("0")) {
            JsonObject jp = (JsonObject) parser.parse(result[1]);
            String estado = jp.get("Estado").getAsString();
            if (estado.equals("OK")) {
                tipoMensaje = 0;
            }
            mensaje = jp.get("Mensaje").getAsString();
            contenido = jp.get("contenido");
        } else {
            mensaje = result[1];
        }
        Result resultadoFinal = new Result(contenido, tipoMensaje, mensaje);
        return resultadoFinal;

    }

    public Result listar_tipo_documento() {
        String url ;
        
        if (MetodosGlobales.flag_service) {
           
            url = MetodosGlobales.ruta_service_get + "/Documento/ListarTipoDocumento";
        } else {
            url = MetodosGlobales.ruta_service + "/Documento/ListarTipoDocumento";
        }
        
        
        int tipoMensaje = 1;
        String mensaje;

        String[] result = ClientServiceGet.callServiceGet(url, "");
        JsonParser parser = new JsonParser();
        JsonElement contenido = null;
        if (result[0].equals("0")) {
            JsonObject jp = (JsonObject) parser.parse(result[1]);
            String estado = jp.get("Estado").getAsString();
            if (estado.equals("OK")) {
                tipoMensaje = 0;
            }
            mensaje = jp.get("Mensaje").getAsString();
            contenido = jp.get("contenido");
        } else {
            mensaje = result[1];
        }
        Result resultadoFinal = new Result(contenido, tipoMensaje, mensaje);
        return resultadoFinal;
    }

    public Result listar_descripcion_bloqueo() {
        String url;
       
        if (MetodosGlobales.flag_service) {
            
            url = MetodosGlobales.ruta_service_get + "/Tarjeta/ListarDescripcionBloqueo";
        } else {
            url = MetodosGlobales.ruta_service + "/Tarjeta/ListarDescripcionBloqueo";
        }
        
        int tipoMensaje = 1;
        String mensaje;

        String[] result = ClientServiceGet.callServiceGet(url, "");
        JsonParser parser = new JsonParser();
        JsonElement contenido = null;
        if (result[0].equals("0")) {
            JsonObject jp = (JsonObject) parser.parse(result[1]);
            String estado = jp.get("Estado").getAsString();
            if (estado.equals("OK")) {
                tipoMensaje = 0;
            }
            mensaje = jp.get("Mensaje").getAsString();
            contenido = jp.get("contenido");
        } else {
            mensaje = result[1];
        }
        Result resultadoFinal = new Result(contenido, tipoMensaje, mensaje);
        return resultadoFinal;
    }

}
