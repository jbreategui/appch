package service;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import util.ClientServiceGet;
import util.ClientServicePost;
import util.JsonObjectWaka;
import util.MetodosGlobales;
import util.Result;


public class EnvioGirosService {

    public Result registrar_giro(
            String codigo_cuenta_origen,
            String monto_envio,
            String codigo_oficina_destino,
            String estado_operacion_frecuente,
            String alias_operacion,
            String apellido_paterno,
            String apellido_materno,
            String nombres_beneficiario,
            String dni_beneficiario,
            String celular_beneficiario,
            String tipo_afiliacion,
            String numero_tarjeta,
            String codigo_cliente,
            String clave_dinamica,
            String clave_cordenada,
            String numero_columna,
            String numero_fila,
            String ip_publica_movil,
            String codigo_sub_menu,
            String header,
            String codigo_solicitud,
            String autenticacion_frecuente
            ) {

        String url ;
        
        if (MetodosGlobales.flag_service) {
            url = MetodosGlobales.ruta_service_post;

        } else {
            url = MetodosGlobales.ruta_service + "/Giro/RegistrarGiro";
        }
        
        
        int tipoMensaje = 1;
        String mensaje;
       
        JsonObjectWaka jo = new JsonObjectWaka();
        JsonObjectWaka beneficiario = new JsonObjectWaka();
        JsonObjectWaka datosValidacion = new JsonObjectWaka();

        beneficiario.addObject("apellidoPaterno", apellido_paterno)
                .addObject("apellidoMaterno", apellido_materno)
                .addObject("nombresBeneficiario", nombres_beneficiario)
                .addObject("dniBeneficiario", dni_beneficiario)
                .addObject("celularBeneficiario", celular_beneficiario);

        datosValidacion.addObject("tipoAfiliacion", tipo_afiliacion)
                .addObject("numeroTarjeta", numero_tarjeta)
                .addObject("codigoCliente", codigo_cliente)
                .addObject("claveDinamica", clave_dinamica)
                .addObject("codigoSolicitud", codigo_solicitud)
                .addObject("claveCordenada", clave_cordenada)
                .addObject("numeroColumna", numero_columna)
                .addObject("numeroFila", numero_fila)
                .addObject("autenticacionFrecuente", autenticacion_frecuente);

        JsonObjectWaka datosAuditoria = new JsonObjectWaka();

        datosAuditoria.addObject("ipPublicaMovil", ip_publica_movil)
                .addObject("codigoSubMenu", codigo_sub_menu);

        jo.addObject("codigoCuentaOrigen", codigo_cuenta_origen)
                .addObject("montoEnvio", monto_envio)
                .addObject("codigoOficinaDestino", codigo_oficina_destino)
                .addObject("estadoOperacionFrecuente", estado_operacion_frecuente)
                .addObject("aliasOperacion", alias_operacion);

         String[] result;

        if (MetodosGlobales.flag_service) {
            JsonObjectWaka jogeneral = new JsonObjectWaka();
          
            jogeneral.addObject("data", jo.addObject("beneficiario", beneficiario).addObject("datosValidacion", datosValidacion).addObject("datosAuditoria", datosAuditoria)).addObject("url", "/Giro/RegistrarGiro");
            result = ClientServicePost.callServicePost(url, jogeneral.getString(), header);
        } else {
            jo.addObject("beneficiario", beneficiario).addObject("datosValidacion", datosValidacion).addObject("datosAuditoria", datosAuditoria);
            result = ClientServicePost.callServicePost(url, jo.getString(), header);
        }
        
        JsonParser parser = new JsonParser();

        JsonElement contenido = null;
        if (result[0].equals("0")) {
            JsonObject jp = (JsonObject) parser.parse(result[1]);
            String estado = jp.get("Estado").getAsString();
            if (estado.equals("OK")) {
                tipoMensaje = 0;
            }
            mensaje = jp.get("Mensaje").getAsString();
            contenido = jp.get("contenido");
        } else {
            mensaje = result[1];
        }
        Result resultadoFinal = new Result(contenido, tipoMensaje, mensaje);
        return resultadoFinal;

    }

    public Result listar_agencias() {
        String url ;
       
        if (MetodosGlobales.flag_service) {

            url = MetodosGlobales.ruta_service_get + "/Oficina/ListarAgencias";
        } else {
            url = MetodosGlobales.ruta_service + "/Oficina/ListarAgencias";
        }
        
        int tipoMensaje = 1;
        String mensaje;

        String[] result = ClientServiceGet.callServiceGet(url, "");
        JsonParser parser = new JsonParser();
        JsonElement contenido = null;
        if (result[0].equals("0")) {
            JsonObject jp = (JsonObject) parser.parse(result[1]);
            String estado = jp.get("Estado").getAsString();
            if (estado.equals("OK")) {
                tipoMensaje = 0;
            }
            mensaje = jp.get("Mensaje").getAsString();
            contenido = jp.get("contenido");
        } else {
            mensaje = result[1];
        }
        Result resultadoFinal = new Result(contenido, tipoMensaje, mensaje);
        return resultadoFinal;
    }
}
