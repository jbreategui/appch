package service;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import util.ClientServiceGet;
import util.ClientServicePost;
import util.JsonObjectWaka;
import util.MetodosGlobales;
import util.Result;

public class CuentaOtroBancoService {

    public Result listar_cuenta_propia_otro_banco_detalle(String codigo_cliente, String tipo_operacion, String header) {
        String url;

        if (MetodosGlobales.flag_service) {
            
            url = MetodosGlobales.ruta_service_post;
        } else {
            url = MetodosGlobales.ruta_service + "/Cuenta/ListarCuentaOrigenCCE";
        }

        int tipoMensaje = 1;
        String mensaje;

        JsonObjectWaka jo = new JsonObjectWaka();

        jo.addObject("codigoCliente", codigo_cliente)
                .addObject("tipoOperacion", tipo_operacion);

        String[] result;

        if (MetodosGlobales.flag_service) {
            JsonObjectWaka jogeneral = new JsonObjectWaka();
            jogeneral.addObject("data", jo).addObject("url", "/Cuenta/ListarCuentaOrigenCCE");
            result = ClientServicePost.callServicePost(url, jogeneral.getString(), header);
        } else {
            result = ClientServicePost.callServicePost(url, jo.getString(), header);
        }

        JsonParser parser = new JsonParser();

        JsonElement contenido = null;
        if (result[0].equals("0")) {
            JsonObject jp = (JsonObject) parser.parse(result[1]);
            String estado = jp.get("Estado").getAsString();
            if (estado.equals("OK")) {
                tipoMensaje = 0;
            }
            mensaje = jp.get("Mensaje").getAsString();
            contenido = jp.get("contenido");
        } else {
            mensaje = result[1];
        }
        Result resultadoFinal = new Result(contenido, tipoMensaje, mensaje);
        return resultadoFinal;
    }

    public Result listar_moneda_otro_banco() {
        String url;

        if (MetodosGlobales.flag_service) {
           
            url = MetodosGlobales.ruta_service_get + "/General/ListarMoneda";
        } else {
            url = MetodosGlobales.ruta_service + "/General/ListarMoneda";
        }

        int tipoMensaje = 1;

        String mensaje;

        String[] result = ClientServiceGet.callServiceGet(url, "");
        JsonParser parser = new JsonParser();
        JsonElement contenido = null;
        if (result[0].equals("0")) {
            JsonObject jp = (JsonObject) parser.parse(result[1]);
            String estado = jp.get("Estado").getAsString();
            if (estado.equals("OK")) {
                tipoMensaje = 0;
            }
            mensaje = jp.get("Mensaje").getAsString();
            contenido = jp.get("contenido");
        } else {
            mensaje = result[1];
        }
        Result resultadoFinal = new Result(contenido, tipoMensaje, mensaje);
        return resultadoFinal;
    }

    public Result val_cuenta_destino_interbancaria(String numero_cci, String monto_transferencia, String codigo_moneda, String codigo_cuenta_origen,String estado_titular, String token) {
        String url;

        if (MetodosGlobales.flag_service) {
            url = MetodosGlobales.ruta_service_post;

        } else {
            url = MetodosGlobales.ruta_service + "/CamaraComercio/ValidarNumeroCCI";
        }

        int tipoMensaje = 1;
        String mensaje;

        JsonObjectWaka jo = new JsonObjectWaka();

        jo.addObject("numeroCCI", numero_cci)
                .addObject("montoTransferencia", monto_transferencia)
                .addObject("codigoMoneda", codigo_moneda)
                .addObject("codigoCuentaOrigen", codigo_cuenta_origen)
                .addObject("estadoTitular", estado_titular);

        String[] result;

        if (MetodosGlobales.flag_service) {
            JsonObjectWaka jogeneral = new JsonObjectWaka();
            jogeneral.addObject("data", jo);
            jogeneral.addObject("url", "/CamaraComercio/ValidarNumeroCCI");
            result = ClientServicePost.callServicePost(url, jogeneral.getString(), token);
        } else {
            result = ClientServicePost.callServicePost(url, jo.getString(), token);
        }

        JsonParser parser = new JsonParser();

        JsonElement contenido = null;
        if (result[0].equals("0")) {
            JsonObject jp = (JsonObject) parser.parse(result[1]);
            String estado = jp.get("Estado").getAsString();
            if (estado.equals("OK")) {
                tipoMensaje = 0;
            }
            mensaje = jp.get("Mensaje").getAsString();
            contenido = jp.get("contenido");
        } else {
            mensaje = result[1];
        }
        Result resultadoFinal = new Result(contenido, tipoMensaje, mensaje);
        return resultadoFinal;
    }

    public Result constancia_cuenta_otro_banco_destino_detalle(
            String codigo_cuenta_origen,
            String numero_cci_destino,
            String codigo_moneda_operacion,
            String monto_transferencia,
            String comision_interbancaria,
            String comision_cliente,
            String estado_titular,
            String estado_operacion_frecuente,
            String alias_operacion,
            String tipo_afiliacion,
            String numero_tarjeta,
            String codigo_cliente,
            String clave_dinamica,
            String codigo_solicitud,
            String clave_cordenada,
            String numero_columna,
            String numero_fila,
            String ip_publica_movil,
            String codigo_sub_menu,
            String header,
            String autenticacion_frecuente,
            String afecta_comision,
            String comision_abono,
            String itf,
            String tipo_medio,
            String valor_medio
    ) {
        String url;

        if (MetodosGlobales.flag_service) {
            url = MetodosGlobales.ruta_service_post;

        } else {
            url = MetodosGlobales.ruta_service + "/CamaraComercio/RegistrarTransferenciaInterbancaria";
        }

        int tipoMensaje = 1;
        String mensaje;

        JsonObjectWaka jo = new JsonObjectWaka();
        JsonObjectWaka datosValidacion = new JsonObjectWaka();
        JsonObjectWaka datosAuditoria = new JsonObjectWaka();
        JsonObjectWaka confirmacionAbono = new JsonObjectWaka();

        jo.addObject("codigoCuentaOrigen", codigo_cuenta_origen)
                .addObject("numeroCciDestino", numero_cci_destino)
                .addObject("codigoMonedaOperacion", codigo_moneda_operacion)
                .addObject("montoTransferencia", monto_transferencia)
                .addObject("comisionInterbancaria", comision_interbancaria)
                .addObject("comisionCliente", comision_cliente)
                .addObject("estadoTitular", estado_titular)
                .addObject("estadoOperacionFrecuente", estado_operacion_frecuente)
                .addObject("aliasOperacion", alias_operacion);

        datosValidacion.addObject("tipoAfiliacion", tipo_afiliacion)
                .addObject("numeroTarjeta", numero_tarjeta)
                .addObject("codigoCliente", codigo_cliente)
                .addObject("claveDinamica", clave_dinamica)
                .addObject("codigoSolicitud", codigo_solicitud)
                .addObject("claveCordenada", clave_cordenada)
                .addObject("numeroColumna", numero_columna)
                .addObject("numeroFila", numero_fila)
                .addObject("autenticacionFrecuente", autenticacion_frecuente);

        datosAuditoria.addObject("ipPublicaMovil", ip_publica_movil)
                .addObject("codigoSubMenu", codigo_sub_menu);

        confirmacionAbono.addObject("afectaComision", afecta_comision)
                .addObject("comisionAbono", comision_abono)
                
                .addObject("tipoMedio", tipo_medio)
                .addObject("ValorMedio", valor_medio);
       
        String[] result ;

        if (MetodosGlobales.flag_service) {
            JsonObjectWaka jogeneral = new JsonObjectWaka();
            jogeneral.addObject("data", jo.addObject("datosAuditoria", datosAuditoria).addObject("datosValidacion", datosValidacion).addObject("confirmacionAbono", confirmacionAbono).addObject("itf", itf)).addObject("url", "/CamaraComercio/RegistrarTransferenciaInterbancaria");
            
            result = ClientServicePost.callServicePost(url, jogeneral.getString(), header);
        } else {
            jo.addObject("datosAuditoria", datosAuditoria).addObject("datosValidacion", datosValidacion).addObject("confirmacionAbono", confirmacionAbono).addObject("itf", itf);
            result = ClientServicePost.callServicePost(url, jo.getString(), header);
        }
   
        JsonParser parser = new JsonParser();

        JsonElement contenido = null;
        if (result[0].equals("0")) {
            JsonObject jp = (JsonObject) parser.parse(result[1]);
            String estado = jp.get("Estado").getAsString();
            if (estado.equals("OK")) {
                tipoMensaje = 0;
            }
            mensaje = jp.get("Mensaje").getAsString();
            contenido = jp.get("contenido");
        } else {
            mensaje = result[1];
        }
        Result resultadoFinal = new Result(contenido, tipoMensaje, mensaje);
        return resultadoFinal;
    }

    public Result clave_dinamica(String numerotarjeta, String codigo_sub_menu, String monto_operacion, String header) {
        String url;

        if (MetodosGlobales.flag_service) {
            url = MetodosGlobales.ruta_service_post;

        } else {
            url = MetodosGlobales.ruta_service + "/ClaveDinamica/RetornarCodigoOperacion";
        }
        
        
        int tipoMensaje = 1;
        String mensaje;
       
        JsonObjectWaka jo = new JsonObjectWaka();

        jo.addObject("numerotarjeta", numerotarjeta)
                .addObject("codigoSubMenu", codigo_sub_menu)
                .addObject("montoOperacion", monto_operacion);
  
        String[] result ;

        if (MetodosGlobales.flag_service) {
            JsonObjectWaka jogeneral = new JsonObjectWaka();
            jogeneral.addObject("data", jo);
            jogeneral.addObject("url", "/ClaveDinamica/RetornarCodigoOperacion");
            result = ClientServicePost.callServicePost(url, jogeneral.getString(), header);
        } else {
            result = ClientServicePost.callServicePost(url, jo.getString(), header);
        }
        
        
        JsonParser parser = new JsonParser();

        JsonElement contenido = null;
        if (result[0].equals("0")) {
            JsonObject jp = (JsonObject) parser.parse(result[1]);
            String estado = jp.get("Estado").getAsString();
            if (estado.equals("OK")) {
                tipoMensaje = 0;
            }
            mensaje = jp.get("Mensaje").getAsString();
            contenido = jp.get("contenido");
        } else {
            mensaje = result[1];
        }
        Result resultadoFinal = new Result(contenido, tipoMensaje, mensaje);
        return resultadoFinal;
    }

}
