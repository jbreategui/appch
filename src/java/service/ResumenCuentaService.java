package service;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import util.ClientServiceGet;
import util.MetodosGlobales;
import util.Result;

public class ResumenCuentaService {

    public Result listar_cuenta_ahorro(String numero_tarjeta, String token) {

        String url;

        if (MetodosGlobales.flag_service) {
           
            url = MetodosGlobales.ruta_service_get +  "/Cuenta/ListarCuentasAhorro/" + numero_tarjeta;
        } else {
            url = MetodosGlobales.ruta_service + "/Cuenta/ListarCuentasAhorro/" + numero_tarjeta;
        }

        int tipoMensaje = 1;
        String mensaje;
        System.out.println(token);
        String[] result = ClientServiceGet.callServiceGet(url, token);
        JsonParser parser = new JsonParser();
        JsonElement contenido = null;
        if (result[0].equals("0")) {
            JsonObject jp = (JsonObject) parser.parse(result[1]);
            String estado = jp.get("Estado").getAsString();
            if (estado.equals("OK")) {
                tipoMensaje = 0;
            }
            mensaje = jp.get("Mensaje").getAsString();
            contenido = jp.get("contenido");
        } else {
            mensaje = result[1];
        }
        Result resultadoFinal = new Result(contenido, tipoMensaje, mensaje);
        return resultadoFinal;
    }

    public Result listar_cuenta_credito(String numero_tarjeta, String token) {
        String url;

        if (MetodosGlobales.flag_service) {
           
            url = MetodosGlobales.ruta_service_get +"/Cuenta/ListarCuentasCredito/" + numero_tarjeta;
        } else {
            url = MetodosGlobales.ruta_service + "/Cuenta/ListarCuentasCredito/" + numero_tarjeta;
        }

        int tipoMensaje = 1;
        String mensaje;
        String[] result = ClientServiceGet.callServiceGet(url, token);
        JsonParser parser = new JsonParser();
        JsonElement contenido = null;
        if (result[0].equals("0")) {
            JsonObject jp = (JsonObject) parser.parse(result[1]);
            String estado = jp.get("Estado").getAsString();
            if (estado.equals("OK")) {
                tipoMensaje = 0;
            }
            mensaje = jp.get("Mensaje").getAsString();
            contenido = jp.get("contenido");
        } else {
            mensaje = result[1];
        }
        Result resultadoFinal = new Result(contenido, tipoMensaje, mensaje);
        return resultadoFinal;
    }
}
