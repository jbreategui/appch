/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import java.util.List;
import util.ClientServicePost;
import util.JsonObjectWaka;
import util.MetodosGlobales;
import util.Result;

/**
 *
 * @author USER
 */
public class PagoInstitucionService {

    public Result registrar_pago_servicio(
            String codigo_cuenta_origen,
            String codigo_empresa,
            String razon_social_empresa,
            String monto_total,
            String cuenta_servicio,
            String codigo_cliente_servicio,
            String nombre_cliente_servicio,
            String estado_operacion_frecuente,
            String alias_operacion,
            String tipo_afiliacion,
            String numero_tarjeta,
            String codigo_cliente,
            String clave_dinamica,
            String codigo_solicitud,
            String clave_cordenada,
            String numero_columna,
            String numero_fila,
            String ip_publica_movil,
            String codigo_sub_menu,
            List<Object[]> array,
            String token,
            String autenticacion_frecuente
    ) {

        String url;
        if (MetodosGlobales.flag_service) {
            url = MetodosGlobales.ruta_service_post;

        } else {
            url = MetodosGlobales.ruta_service + "/Institucion/RegistrarPagoInstitucional";
        }

        int tipoMensaje = 1;
        String mensaje;
        JsonObjectWaka jogeneral = new JsonObjectWaka();
        JsonObjectWaka datosValidacion = new JsonObjectWaka();
        JsonObjectWaka detalles = null;

        JsonObjectWaka array_detalles[] = new JsonObjectWaka[array.size()];
        JsonObjectWaka arrayCabecera = new JsonObjectWaka();
        JsonObjectWaka datosAuditoria = new JsonObjectWaka();

        arrayCabecera
                .addObject("codigoCuentaOrigen", codigo_cuenta_origen)
                .addObject("codigoEmpresa", codigo_empresa)
                .addObject("razonSocialEmpresa", razon_social_empresa)
                .addObject("montoTotal", monto_total)
                .addObject("cuentaServicio", cuenta_servicio)
                .addObject("codigoClienteServicio", codigo_cliente_servicio)
                .addObject("nombreClienteServicio", nombre_cliente_servicio)
                .addObject("estadoOperacionFrecuente", estado_operacion_frecuente)
                .addObject("aliasOperacion", alias_operacion);

        datosValidacion
                .addObject("tipoAfiliacion", tipo_afiliacion)
                .addObject("numeroTarjeta", numero_tarjeta)
                .addObject("codigoCliente", codigo_cliente)
                .addObject("claveDinamica", clave_dinamica)
                .addObject("codigoSolicitud", codigo_solicitud)
                .addObject("claveCordenada", clave_cordenada)
                .addObject("numeroColumna", numero_columna)
                .addObject("numeroFila", numero_fila)
                .addObject("autenticacionFrecuente", autenticacion_frecuente);

        datosAuditoria
                .addObject("ipPublicaMovil", ip_publica_movil)
                .addObject("codigoSubMenu", codigo_sub_menu);
        int c = 0;
        for (Object[] objects : array) {

            detalles = new JsonObjectWaka();
            String sucursal_codigo = objects[0].toString();
            String cliente_empresa = objects[1].toString();
            String concepto_codigo = objects[2].toString();
            String documento_servicio = objects[3].toString();
            String nombre_cliente = objects[4].toString();
            String periodo_pago = objects[5].toString();
            String dni_cliente = objects[6].toString();
            String moneda_cuota = objects[7].toString();
            String monto_cuota = objects[8].toString();
            String prioridad_cuota = objects[9].toString();
            String tipo_pago = objects[10].toString();
            String monto_pagado = objects[11].toString();
            String monto_mora = objects[12].toString();
            String mora_pagado = objects[13].toString();
            String estado_pago_cuota = objects[14].toString();
            String afectacion_comision = objects[15].toString();
            String monto_comision = objects[16].toString();
            String afectacion_cuota = objects[17].toString();
            String _cuenta_servicio = objects[18].toString();
            String estado_registro = objects[19].toString();
            String fecha_vencimiento = objects[21].toString();
            String sucursal_nombre = objects[22].toString();
            String concepto_nombre = objects[23].toString();
            String monto_pronto_pago = objects[24].toString();

            detalles.addObject("sucursalCodigo", sucursal_codigo)
                    .addObject("clienteEmpresa", cliente_empresa)
                    .addObject("conceptoCodigo", concepto_codigo)
                    .addObject("documentoServicio", documento_servicio)
                    .addObject("nombreCliente", nombre_cliente)
                    .addObject("periodoPago", periodo_pago)
                    .addObject("dniCliente", dni_cliente)
                    .addObject("monedaCuota", moneda_cuota)
                    .addObject("montoCuota", monto_cuota)
                    .addObject("prioridadCuota", prioridad_cuota)
                    .addObject("tipoPago", tipo_pago)
                    .addObject("montoPagado", monto_pagado)
                    .addObject("montoMora", monto_mora)
                    .addObject("moraPagado", mora_pagado)
                    .addObject("estadoPagoCuota", estado_pago_cuota)
                    .addObject("afectacionComision", afectacion_comision)
                    .addObject("montoComision", monto_comision)
                    .addObject("afectacionCuota", afectacion_cuota)
                    .addObject("cuentaServicio", _cuenta_servicio)
                    .addObject("estadoRegistro", estado_registro)
                    .addObject("fechaVencimiento", fecha_vencimiento)
                    .addObject("sucursalNombre", sucursal_nombre)
                    .addObject("conceptoNombre", concepto_nombre)
                    .addObject("montoProntoPago", monto_pronto_pago)
                    ;

            array_detalles[c++] = detalles;

        }

        String[] result;
        if (MetodosGlobales.flag_service) {
            jogeneral.addObject("data", arrayCabecera.addObject("listaPagosCuota", array_detalles).addObject("datosValidacion", datosValidacion).addObject("datosAuditoria", datosAuditoria)).addObject("url", "/Institucion/RegistrarPagoInstitucional");
            result = ClientServicePost.callServicePost(url, jogeneral.getString(), token);
        } else {
            arrayCabecera.addObject("listaPagosCuota", array_detalles).addObject("datosValidacion", datosValidacion).addObject("datosAuditoria", datosAuditoria);
            result = ClientServicePost.callServicePost(url, arrayCabecera.getString(), token);
        }
        JsonParser parser = new JsonParser();

        JsonElement contenido = null;
        if (result[0].equals("0")) {
            JsonObject jp = (JsonObject) parser.parse(result[1]);
            String estado = jp.get("Estado").getAsString();
            if (estado.equals("OK")) {
                tipoMensaje = 0;
            }
            mensaje = jp.get("Mensaje").getAsString();
            contenido = jp.get("contenido");
        } else {
            mensaje = result[1];
        }
        Result resultadoFinal = new Result(contenido, tipoMensaje, mensaje);
        return resultadoFinal;

    }

    public Result validar_cliente_Inst(String codigo_empresa, String dni_cliente_servicio, String header) {

        String url;
        if (MetodosGlobales.flag_service) {
            url = MetodosGlobales.ruta_service_post;

        } else {
            url = MetodosGlobales.ruta_service + "/Institucion/ValidarClienteInstitucional";
        }

        int tipoMensaje = 1;
        String mensaje;
        JsonObjectWaka jogeneral = new JsonObjectWaka();
        JsonObjectWaka jo = new JsonObjectWaka();

        jo.addObject("codigoEmpresa", codigo_empresa)
                .addObject("dniClienteServicio", dni_cliente_servicio);

        String[] result;
        if (MetodosGlobales.flag_service) {
            jogeneral.addObject("data", jo).addObject("url", "/Institucion/ValidarClienteInstitucional");
            result = ClientServicePost.callServicePost(url, jogeneral.getString(), header);
        } else {
            result = ClientServicePost.callServicePost(url, jo.getString(), header);
        }
        JsonParser parser = new JsonParser();

        JsonElement contenido = null;
        if (result[0].equals("0")) {
            JsonObject jp = (JsonObject) parser.parse(result[1]);
            String estado = jp.get("Estado").getAsString();
            if (estado.equals("OK")) {
                tipoMensaje = 0;
            }
            mensaje = jp.get("Mensaje").getAsString();
            contenido = jp.get("contenido");
        } else {
            mensaje = result[1];
        }
        Result resultadoFinal = new Result(contenido, tipoMensaje, mensaje);
        return resultadoFinal;

    }

    public Result buscarEmpresa(String razon_social_empresa, String codigo_sub_menu) {

        String url;
        if (MetodosGlobales.flag_service) {
           url = MetodosGlobales.ruta_service_post;

        } else {
            url = MetodosGlobales.ruta_service + "/Servicio/BuscarEmpresa";
        }

        int tipoMensaje = 1;
        String mensaje;
        JsonObjectWaka jo = new JsonObjectWaka();

        jo.addObject("razonSocialEmpresa", razon_social_empresa)
                .addObject("codigoSubMenu", codigo_sub_menu);
        String[] result;
        if (MetodosGlobales.flag_service) {
            JsonObjectWaka jogeneral = new JsonObjectWaka();
            jogeneral.addObject("data", jo).addObject("url", "/Servicio/BuscarEmpresa");
            result = ClientServicePost.callServicePost(url, jogeneral.getString(), "");
        } else {
            result = ClientServicePost.callServicePost(url, jo.getString(), "");
        }
        JsonParser parser = new JsonParser();

        JsonElement contenido = null;
        if (result[0].equals("0")) {
            JsonObject jp = (JsonObject) parser.parse(result[1]);
            String estado = jp.get("Estado").getAsString();
            if (estado.equals("OK")) {
                tipoMensaje = 0;
            }
            mensaje = jp.get("Mensaje").getAsString();
            contenido = jp.get("contenido");
        } else {
            mensaje = result[1];
        }
        Result resultadoFinal = new Result(contenido, tipoMensaje, mensaje);
        return resultadoFinal;

    }
}
