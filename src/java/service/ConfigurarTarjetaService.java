package service;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import util.ClientServicePost;
import util.JsonObjectWaka;
import util.MetodosGlobales;
import util.Result;

public class ConfigurarTarjetaService {

    public Result retornar_estado_fuera_pais(String numero_tarjeta, String codigo_cliente, String header) {
        String url;
       
        if (MetodosGlobales.flag_service) {
            
            url = MetodosGlobales.ruta_service_post;
        } else {
            url = MetodosGlobales.ruta_service + "/Tarjeta/RetornarEstadoFueraPais";
        }
        
        
        int tipoMensaje = 1;
        String mensaje;
        
        JsonObjectWaka jo = new JsonObjectWaka();

        jo.addObject("numeroTarjeta", numero_tarjeta)
                .addObject("codigoCliente", codigo_cliente);

        String[] result ;

        if (MetodosGlobales.flag_service) {
            JsonObjectWaka jogeneral = new JsonObjectWaka();
            jogeneral.addObject("data", jo);
            jogeneral.addObject("url", "/Tarjeta/RetornarEstadoFueraPais");
            result =  ClientServicePost.callServicePost(url, jogeneral.getString(), header);
        } else {
            result = ClientServicePost.callServicePost(url, jo.getString(), header);
        }
        
        JsonParser parser = new JsonParser();

        JsonElement contenido = null;
        if (result[0].equals("0")) {
            JsonObject jp = (JsonObject) parser.parse(result[1]);
            String estado = jp.get("Estado").getAsString();
            if (estado.equals("OK")) {
                tipoMensaje = 0;
            }
            mensaje = jp.get("Mensaje").getAsString();
            contenido = jp.get("contenido");
        } else {
            mensaje = result[1];
        }
        Result resultadoFinal = new Result(contenido, tipoMensaje, mensaje);
        return resultadoFinal;

    }

    public Result registrar_estado(
            String numero_tarjeta,
            String codigo_cliente,
            String estado_activo_fuera,
            String ip_publica_movil,
            String codigo_sub_menu,
            String header) {

        String url;
      
         if (MetodosGlobales.flag_service) {
            
            url = MetodosGlobales.ruta_service_post;

        } else {
            url = MetodosGlobales.ruta_service + "/Tarjeta/RegistarUsoFueraPais";
        }
        
        
        int tipoMensaje = 1;
        String mensaje;
        
       
        JsonObjectWaka jo = new JsonObjectWaka();
 
        jo.addObject("numeroTarjeta", numero_tarjeta)
                .addObject("codigoCliente", codigo_cliente)
                .addObject("estadoActivoFuera", estado_activo_fuera);

        JsonObjectWaka datosAuditoria = new JsonObjectWaka();

        datosAuditoria.addObject("ipPublicaMovil", ip_publica_movil)
                .addObject("codigoSubMenu", codigo_sub_menu);
             
        String[] result;

        if (MetodosGlobales.flag_service) {
            JsonObjectWaka jogeneral = new JsonObjectWaka();
            jogeneral.addObject("data", jo.addObject("datosAuditoria", datosAuditoria));          
            jogeneral.addObject("url", "/Tarjeta/RegistarUsoFueraPais");
            result = ClientServicePost.callServicePost(url, jogeneral.getString(), header);
        } else {
            jo.addObject("datosAuditoria", datosAuditoria);  
            result = ClientServicePost.callServicePost(url, jo.getString(), header);
        }
        
        
        
        JsonParser parser = new JsonParser();

        JsonElement contenido = null;
        if (result[0].equals("0")) {
            JsonObject jp = (JsonObject) parser.parse(result[1]);
            String estado = jp.get("Estado").getAsString();
            if (estado.equals("OK")) {
                tipoMensaje = 0;
            }
            mensaje = jp.get("Mensaje").getAsString();
            contenido = jp.get("contenido");
        } else {
            mensaje = result[1];
        }
        Result resultadoFinal = new Result(contenido, tipoMensaje, mensaje);
        return resultadoFinal;

    }

}
