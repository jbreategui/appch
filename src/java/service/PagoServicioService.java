package service;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import util.ClientServicePost;
import util.JsonObjectWaka;
import util.MetodosGlobales;
import util.Result;


public class PagoServicioService {

    public Result registrar_pago_servicio(
            String codigo_cuenta_origen,
            String codigo_empresa,
            String razon_social_empresa,
            String codigo_sucursal,
            String codigo_concepto,
            String monto_cuota,
            String monto_mora,
            String monto_comision,
            String periodo_pago,
            String cuenta_servicio,
            String codigo_cliente_servicio,
            String nombre_cliente_servicio,
            String estado_operacion_frecuente,
            String alias_operacion,
            String tipo_afiliacion,
            String numero_tarjeta,
            String codigo_cliente,
            String clave_dinamica,
            String codigo_solicitud,
            String clave_cordenada,
            String numero_columna,
            String numero_fila,
            String ip_publica_movil,
            String codigo_sub_menu,
            String token,
            String autenticacion_frecuente
    ) {

        int tipoMensaje = 1;

        String url;
        if (MetodosGlobales.flag_service) {
            url = MetodosGlobales.ruta_service_post;

        } else {
            url = MetodosGlobales.ruta_service + "/Servicio/RegistrarPagoServicio";
        }

        String mensaje;

        JsonObjectWaka jo = new JsonObjectWaka();
        JsonObjectWaka datosValidacion = new JsonObjectWaka();

        datosValidacion.addObject("tipoAfiliacion", tipo_afiliacion)
                .addObject("numeroTarjeta", numero_tarjeta)
                .addObject("codigoCliente", codigo_cliente)
                .addObject("claveDinamica", clave_dinamica)
                .addObject("codigoSolicitud", codigo_solicitud)
                .addObject("claveCordenada", clave_cordenada)
                .addObject("numeroColumna", numero_columna)
                .addObject("numeroFila", numero_fila)
                .addObject("autenticacionFrecuente", autenticacion_frecuente);

        JsonObjectWaka datosAuditoria = new JsonObjectWaka();

        datosAuditoria.addObject("ipPublicaMovil", ip_publica_movil)
                .addObject("codigoSubMenu", codigo_sub_menu);

        jo.addObject("codigoCuentaOrigen", codigo_cuenta_origen)
                .addObject("codigoEmpresa", codigo_empresa)
                .addObject("razonSocialEmpresa", razon_social_empresa)
                .addObject("codigoSucursal", codigo_sucursal)
                .addObject("codigoConcepto", codigo_concepto)
                .addObject("montoCuota", monto_cuota)
                .addObject("montoMora", monto_mora)
                .addObject("montoComision", monto_comision)
                .addObject("periodoPago", periodo_pago)
                .addObject("cuentaServicio", cuenta_servicio)
                .addObject("codigoClienteServicio", codigo_cliente_servicio)
                .addObject("nombreClienteServicio", nombre_cliente_servicio)
                .addObject("estadoOperacionFrecuente", estado_operacion_frecuente)
                .addObject("aliasOperacion", alias_operacion);

        String[] result;
        if (MetodosGlobales.flag_service) {
            JsonObjectWaka jogeneral = new JsonObjectWaka();
            jogeneral.addObject("data", jo.addObject("datosValidacion", datosValidacion).addObject("datosAuditoria", datosAuditoria)).addObject("url", "/Servicio/RegistrarPagoServicio");
            result = ClientServicePost.callServicePost(url, jogeneral.getString(), token);
        } else {
            jo.addObject("datosValidacion", datosValidacion).addObject("datosAuditoria", datosAuditoria);
            result = ClientServicePost.callServicePost(url, jo.getString(), token);
        }
        JsonParser parser = new JsonParser();

        JsonElement contenido = null;
        if (result[0].equals("0")) {
            JsonObject jp = (JsonObject) parser.parse(result[1]);
            String estado = jp.get("Estado").getAsString();
            if (estado.equals("OK")) {
                tipoMensaje = 0;
            }
            mensaje = jp.get("Mensaje").getAsString();
            contenido = jp.get("contenido");
        } else {
            mensaje = result[1];
        }
        Result resultadoFinal = new Result(contenido, tipoMensaje, mensaje);
        return resultadoFinal;

    }

    public Result validar_cliente_servicio(String codigo_empresa, String dni_cliente_servicio, String header) {

        String url;
        if (MetodosGlobales.flag_service) {
            url = MetodosGlobales.ruta_service_post;

        } else {
            url = MetodosGlobales.ruta_service + "/Servicio/ValidarClienteServicio";
        }

        int tipoMensaje = 1;
        String mensaje;

        JsonObjectWaka jo = new JsonObjectWaka();

        jo.addObject("codigoEmpresa", codigo_empresa)
                .addObject("dniClienteServicio", dni_cliente_servicio);
        String[] result;
        if (MetodosGlobales.flag_service) {

            JsonObjectWaka jogeneral = new JsonObjectWaka();
            jogeneral.addObject("data", jo).addObject("url", "/Servicio/ValidarClienteServicio");
            result = ClientServicePost.callServicePost(url, jogeneral.getString(), header);
        } else {
            result = ClientServicePost.callServicePost(url, jo.getString(), header);
        }
        JsonParser parser = new JsonParser();

        JsonElement contenido = null;
        if (result[0].equals("0")) {
            JsonObject jp = (JsonObject) parser.parse(result[1]);
            String estado = jp.get("Estado").getAsString();
            if (estado.equals("OK")) {
                tipoMensaje = 0;
            }
            mensaje = jp.get("Mensaje").getAsString();
            contenido = jp.get("contenido");
        } else {
            mensaje = result[1];
        }
        Result resultadoFinal = new Result(contenido, tipoMensaje, mensaje);
        return resultadoFinal;

    }

    public Result buscarEmpresa(String razon_social_empresa, String codigo_sub_menu) {

        String url;
        if (MetodosGlobales.flag_service) {
            url = MetodosGlobales.ruta_service_post;

        } else {
            url = MetodosGlobales.ruta_service + "/Servicio/BuscarEmpresa";
        }

        int tipoMensaje = 1;
        String mensaje;

        JsonObjectWaka jo = new JsonObjectWaka();
        String[] result;

        jo.addObject("razonSocialEmpresa", razon_social_empresa)
                .addObject("codigoSubMenu", codigo_sub_menu);
        if (MetodosGlobales.flag_service) {
            JsonObjectWaka jogeneral = new JsonObjectWaka();
            jogeneral.addObject("data", jo).addObject("url", "/Servicio/BuscarEmpresa");
            result = ClientServicePost.callServicePost(url, jogeneral.getString(), "");
        } else {
            result = ClientServicePost.callServicePost(url, jo.getString(), "");
        }
        JsonParser parser = new JsonParser();

        JsonElement contenido = null;
        if (result[0].equals("0")) {
            JsonObject jp = (JsonObject) parser.parse(result[1]);
            String estado = jp.get("Estado").getAsString();
            if (estado.equals("OK")) {
                tipoMensaje = 0;
            }
            mensaje = jp.get("Mensaje").getAsString();
            contenido = jp.get("contenido");
        } else {
            mensaje = result[1];
        }
        Result resultadoFinal = new Result(contenido, tipoMensaje, mensaje);
        return resultadoFinal;

    }
}
