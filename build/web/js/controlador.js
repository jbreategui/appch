var IP_DISPOSITIVO_iOS = "";
function set_ip() {
    try {
        webkit.messageHandlers.ip.postMessage("");
    } catch (err) {
        console.log('The native context does not exist yet');
    }
}

function get_ip(ip) {
    IP_DISPOSITIVO_iOS = ip;

}

function set_flag_huella() {
    try {
        webkit.messageHandlers.flag.postMessage("hide");
    } catch (err) {
        console.log('The native context does not exist yet');
    }
}

function get_flag_huella(id) {
    FLAG_HUELLA_IOS = id;
}
function call_phone(numero) {

    try {
        webkit.messageHandlers.call.postMessage(
                numero.trim()
                );
    } catch (err) {
        console.log('The native context does not exist yet');
    }
}

function set_shared(array) {

    try {
        webkit.messageHandlers.shared.postMessage(
                array
                );
    } catch (err) {
        console.log('The native context does not exist yet');
    }
}

function get_UIDD() {
    try {
        webkit.messageHandlers.UIDD.postMessage("");
    } catch (err) {
        console.log('The native context does not exist yet');
    }
}

function set_UIDD(id) {

    document.querySelector('h1').innerHTML = id;

}

function set_correo(correo) {

    try {
        webkit.messageHandlers.correo.postMessage(
                correo.trim()
                );
    } catch (err) {
        console.log('The native context does not exist yet');
    }
}

function get_huella() {
    try {
        webkit.messageHandlers.huella.postMessage("");
    } catch (err) {
        console.log('The native context does not exist yet');
    }
}



function check_so() {
    var os = "d";
    if ((navigator.userAgent.match(/iPhone/i)) || (navigator.userAgent.match(/iPod/i)) || (navigator.userAgent.match(/iPad/i))) {
        os = 'iOS';
    } else if (navigator.userAgent.match(/Android/i)) {
        os = 'Android';
    }

    return os;
}


//  1  obtiene ip
//  2  llama phone
//  3  comparte 
//  4  obtiene  id instancia
//  5   envia correo
// obitne huella digital
function controlador_so(id, parametros) {


    if (check_so === "iOS") {

        var resulatdo;
        switch (id) {

            case 1 :
                set_ip();
            case 2 :
                call_phone(parametros);
            case 3  :
                set_shared(parametros);
            case 4 :
                get_UIDD();

            case 5 :
                set_correo(parametros);
            case 6 :

                get_huella();

        }

    } else if (check_so === "Android") {

        //metodos android


    }

    return resulatdo;
}
function obtenerGPS() {

    try {
        webkit.messageHandlers.gp.postMessage(
                "_"
                );
    } catch (err) {
        console.log('The native context does not exist yet');
    }


    //nota  :  para obtener las corrdenadas copiar y pegar este metodo donde lo necesiten , ya que el ios llamara a esta funcion 

//    function get_GPS(gps) {
//
//        message_req("gps", gps);
//    }
}



function set_LINK(link) {

    try {
        webkit.messageHandlers.celda.postMessage(
                link
                );
    } catch (err) {
        console.log('The native context does not exist yet');
    }
}


function validar_conexion() {

    try {
        webkit.messageHandlers.red.postMessage(
                "conxion"
                );
    } catch (err) {
        console.log('The native context does not exist yet');
    }
}

function in_cerrar_session(ruta) {

    try {
        webkit.messageHandlers.session.postMessage(
                ruta
                );
    } catch (err) {
        console.log('The native context does not exist yet');
    }
}

function out_cerrar_session_ios(servlet) {

    window.location.href = servlet;

}


function cerrar_app() {

    try {
        webkit.messageHandlers.exit.postMessage(
                "exit"
                );
    } catch (err) {
        console.log('The native context does not exist yet');
    }
}