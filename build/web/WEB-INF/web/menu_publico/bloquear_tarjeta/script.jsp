<jsp:include page="/web/glb/script_general.jsp" />

<script>


    $(function () {

        $("#img-bloquear-tarjeta").attr("src", "${pageContext.request.contextPath}/img/i-tarjeta-bloquear.png");


        var mensaje = $("#hdd_mensaje_login").val();
        if (mensaje.length > 0) {
            message_req("Mensaje", mensaje);
        }

        $(".keyboard").numKey({
            limit: 4,
            disorder: true
        });

        $(".keyboard").click(function () {

            $(".keyboard").numKey({
                limit: 4,
                disorder: true
            });

        });

        $("#bloqueo_tarjeta_tipo_documento").val('1');

    });





    function validar_campos() {
        var array = [];
        var result = "<ul>";
        var clavePin = $("#bloqueo_tarjeta_clave").val();
        var documentoCliente = $("#bloqueo_tarjeta_numero_documento").val();
        var codigoDesBloqueo = $("#bloqueo_tarjeta_motivo_bloqueo").val();
        var descripcionBloqueo = $("#bloqueo_tarjeta_detalle").val();


        var flag = true;
        if (documentoCliente.length === 0) {
            result += "<li>Ingrese n�mero de documento</li>";
            flag = false;
        }
        if (descripcionBloqueo.length === 0) {
            result += "<li>Ingrese detalle </li>";
            flag = false;
        }
        if (clavePin.length === 0) {
            result += "<li>Ingrese clave Pin</li>";
            flag = false;
        }

        array = [flag, result];

        return array;
    }

    function js_bloquear_tarjeta() {
        var array = validar_campos();
        if (array[0] === true) {
            js_bloqueo();
        } else {

            message_req("Mensaje", array[1]);

        }
    }

    function js_bloqueo() {


        $.ajax({
            url: '${pageContext.request.contextPath}/BloqueoTarjetaServlet',
            type: 'post',
            dataType: "text",
            beforeSend: function () {
                $("#btn-confirmar").attr("disabled", true).addClass("btn-caja-activo").removeClass("btn-caja");
            },
            data: {
                'clavePin': $("#bloqueo_tarjeta_clave").val(),
                'ippublicamovil': ip_movil(),
                'documentoCliente': $("#bloqueo_tarjeta_numero_documento").val(),
                'codigoDescripcionBloqueo': $("#bloqueo_tarjeta_motivo_bloqueo").val(),
                'accion': 'bloquear_tarjeta',
                'descripcionBloqueo': $("#bloqueo_tarjeta_detalle").val()
            },
            success: function (salida) {

                $("#btn-confirmar").attr("disabled", false).addClass("btn-caja").removeClass("btn-caja-activo");

                if (salida.length !== 0) {

                    var array = salida.split("%%%");
                    if (array[0] === "0") {
                        crearForm(array[1]);
                    } else {
                        message_req("Mensaje", array[1]);
                    }

                } else {
                    message_req("Mensaje", "Error Inesperado");
                }
            },
            error: function () {
                window.location.href = "${pageContext.request.contextPath}/";
            }
        });

    }

    function ir_login() {

        $.ajax({
            url: '${pageContext.request.contextPath}/Index',
            type: 'post',
            dataType: "text",
            beforeSend: function () {

            },
            data: {
                'accion': 'iniciar_variables_globales'
            },
            success: function (salida) {

                if (salida.length !== 0) {

                    var array = salida.split("%%%");
                    if (array[0] === "0") {
                        crearForm(array[1]);
                    } else {
                        message_req("Mensaje", array[1]);
                    }

                } else {
                    message_req("Mensaje", "Error Inesperado");
                }
            },
            error: function () {
                window.location.href = "${pageContext.request.contextPath}/";
            }
        });

    }

    function get_call_tel() {

        if (check_so() === "iOS") {
            call_phone('${numero_fijo}');
        } else if (check_so() === "Android") {
            Android.get_call_cell_phone('${numero_fijo}');
        }

    }



</script>

