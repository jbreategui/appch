<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>


    
    <%
            String id = (String) request.getSession().getAttribute("r_codigoCliente");
            String content = "";
            if (id != null) {
                content = "1";
            } else {
                content = "0";
            }
        %>
    
 <% if ( !content.equals("1") ) { %>
          <div class="row cuerpo" style="margin-top:20px">
 <% }%>
 
 <% if ( !content.equals("0") ) { %>
          <div class="row cuerpo">
 <% }%>
 
    <div class="col-xs-12 col-md-12 text-center">

        <img src="${pageContext.request.contextPath}/img/i-tarjeta-bloquear.png" alt="" style="max-width: 50px;width: 100%">

    </div>

    <div class="col-xs-12 col-md-12" >
        <p class="text-center" style="font-weight: 900;font-size: 20px">
            Bloqueo de tarjetas
        </p>
    </div>

    <div class="col-xs-12 col-md-12 nopad"  >

        <div class="col-xs-12 box box-caja" style="padding: 5px">

            <div class="form-group has-feedback">
                <label>Tipo de Documento</label>
                <select id="bloqueo_tarjeta_tipo_documento" style="display: inline-block;width: auto" type="text" class="form-control" >
                    <c:forEach var="lista" items="${lista_tipo_documento}">
                        <option value="${lista[0]}"> ${lista[1]}</option>
                    </c:forEach>
                </select>
            </div>

            <div class="form-group ">
                <label>N�mero de Documento</label>
                <input onkeypress="return funcion_solo_numeros(event);"  id="bloqueo_tarjeta_numero_documento"  style="display: inline-block;width: auto" type="tel" class="form-control" >
            </div>

            <div class="form-group ">
                <label>Motivo de Bloqueo</label>
                <select id="bloqueo_tarjeta_motivo_bloqueo" style="display: inline-block;width: auto" type="text" class="form-control" >

                    <c:forEach var="lista" items="${lista_descripcion_bloqueo}">
                        <option value="${lista[0]}"> ${lista[1]}</option>
                    </c:forEach>
                </select>
            </div>

            <div class="form-group ">
                <label>Detalle</label>
                <textarea id="bloqueo_tarjeta_detalle" type="text" class="form-control"></textarea>
            </div>

            <div class="form-group ">
                <label>Clave de 4 d�gitos</label>
                <input readonly="readonly" maxlength="4" id="bloqueo_tarjeta_clave" style="display: inline-block;width: auto" type="password" class="form-control keyboard" />
            </div>

            <div class="form-group">
                <div class="col-xs-6">
                    <button id="btn-confirmar"  onclick="js_bloquear_tarjeta();" class="btn btn-caja tam1 pull-right">Bloquear</button>
                </div>
                <div class="col-xs-6">
                    <button class="btn btn-caja tam1 pull-left" onclick="ir_login();">Cerrar</button>
                </div>
            </div>

        </div>

    </div>

    <div class="col-xs-12 col-md-12  text-center" style="padding-bottom:20px">

        <button onclick="get_call_tel();"  class="btn btn-block btn-caja tam1" style="border-radius: 20px">Call Center</button>

    </div>

    <div class="clearfix"></div>

</div>

