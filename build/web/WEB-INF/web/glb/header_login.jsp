<style>
    .progress {
        height: 30px;
        background-color: white;
    }
    .progress > svg {
        height: 100%;
        display: block;
    }
</style>

</head>


<div class="row back-color-1 header" >

    <p>

        <span style="padding: 10px;color:white;font-size: 12px"><i class="far fa-calendar-alt"></i> ${fecha_sistema}</span>


        <%
            String id = (String) request.getSession().getAttribute("r_codigoCliente");       
            if (id != null) {
        %>        
        <a onclick="ir_oper_frec();"><span id="op_frec" class="pull-right">Op. Frecuentes</span></a> 
        <%    }
        %>

    </p>

    <div class="col-xs-12 text-center">

        <span style="color:white">BIENVENIDO (A)</span><br>
        <span style="color:white">${r_nombreCliente}</span>

    </div>



</div>