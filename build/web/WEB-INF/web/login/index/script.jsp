<script>



    $(function () {

        var mensaje = $("#hdd_mensaje_login").val();
        if (mensaje.length > 0) {
            message_req("Mensaje", mensaje);
        }


        var numero_tarjeta = "${datos_tarjeta_recordar[0]}";
        var estado_reacordar = "${datos_tarjeta_recordar[1]}";





        if (numero_tarjeta.length !== 0 && estado_reacordar.length !== 0) {


            if (estado_reacordar.trim() === "true") {
                $("#numero_tarjeta").val(numero_tarjeta.trim());
                $("#recordar").prop("checked", true);
                $("#return-numero-tarjeta").val(numero_tarjeta.trim());

                tarjeta_oculta();

            } else {
                $("#recordar").prop("checked", false);

            }

        }



        function tarjeta_oculta() {

            var valor_tarjeta_actual = $("#return-numero-tarjeta").val();
            var tarjeta_oculta = "";

            var array_tarjeta = valor_tarjeta_actual.split("");

            $.each(array_tarjeta, function (key, valor) {

                if (key < 6 || key > 12) {

                    tarjeta_oculta += valor;

                } else {

                    tarjeta_oculta += "*";

                }

            });

            $("#numero_tarjeta").val(tarjeta_oculta.trim());

        }


        if (estado_reacordar.trim() === "false") {
            var prefijo = $("#codigo_tarjeta").val().split("%%%")[1];
            $("#numero_tarjeta").val(prefijo.trim());
        }

        $("#codigo_tarjeta").change(function () {

            var prefijo = $(this).val().split("%%%")[1];

            if (prefijo.length > 6) {
                tarjeta_oculta();
            }

        });


        if (check_so() === "iOS") {

            obtenerGPS();

        } else if (check_so() === "Android") {

            Android.posicion();

        }




    });



    $(".keyboard").numKey({
        limit: 6,
        disorder: true
    });

    $(".keyboard").click(function () {

        $(this).val('');

        $(".keyboard").numKey({
            limit: 6,
            disorder: true
        });

    });



    function js_listado() {


        $.ajax({
            url: '${pageContext.request.contextPath}/Index',
            type: 'post',
            dataType: "text",
            data: {
                'accion': "lista_dinamica",
                'codigoTarjeta': $("#codigo_tarjeta").val().split("%%%")[0],
                'codigoInstalacion': "${codigoInstalacion_iOS}"
            },
            error: function () {
                window.location.href = "${pageContext.request.contextPath}/";
            },
            success: function (salida) {

                var array = salida.split("%%%");



                var condicion = $("#condicion_tarjeta_default").val(array[0]);
                var codigo_default = $("#codigo_tarjeta_default").val(array[1]);



            }
        });
    }

    function validarCampos() {

        var array = [];
        var result = "<ul>";
        var codigo_tarjeta = $("#codigo_tarjeta").val();
        var numero_tarjeta = $("#numero_tarjeta").val();
        var numero_password = $("#numero_password").val();


        var flag = true;
        if (codigo_tarjeta.length === undefined) {
            result += "<li>Ingrese tipo de tarjeta</li>";
            flag = false;
        }
        if (numero_tarjeta.length === 0) {
            result += "<li>Ingrese n�mero de tarjeta </li>";
            flag = false;
        } else {

            if (numero_tarjeta.length !== 16) {
                result += "<li>N�mero de tarjeta debe ser de 16 d�gitos  </li>";
                flag = false;
            }
        }
        if (numero_password.length === 0) {
            result += "<li>Ingrese clave web</li>";
            flag = false;
        } else {

            if (numero_password.length !== 6) {
                result += "<li>clave web debe ser de 6 d�gitos  </li>";
                flag = false;
            }
        }

        result += "</ul>";
        array = [flag, result];

        return array;
    }




    function ingresar() {
        var array = validarCampos();
        if (array[0] === true) {
            js_login();
        } else {

            message_req("Mensaje", array[1]);

        }
    }


    var huella = "";


    $(document).ready(function () {

        if ('${validador}' === 'false') {

            $("#complemento_huella").attr("class", "col-xs-12 col-md-12");
            $("#huella").attr("class", "hidden");

        }

        $("#btn1").click(function () {//boton que llama al modal

            var array = [];
            var result = "<ul>";
            var flag = true;
            var numero_tarjeta = $("#numero_tarjeta").val();

            if (numero_tarjeta.length === 0) {
                result += "<li>Ingrese n�mero de tarjeta </li>";
                flag = false;
            } else {

                if (numero_tarjeta.length !== 16) {
                    result += "<li>N�mero de tarjeta debe ser de 16 d�gitos  </li>";
                    flag = false;
                }
            }

            result += "</ul>";
            array = [flag, result];

            if (array[0] === false) {
                message_req("Mensaje", array[1]);
                return;
            }



            $("#myModal").modal();
            $("#finger").attr("src", "${pageContext.request.contextPath}/img/finger_load.png");


            aprobar();

        });

    });

    $("#myModal").on('hidden.bs.modal', function () {

        $("#finger").attr("src", "${pageContext.request.contextPath}/img/finger_login.png");

    });





    function aprobar() {

        if (check_so() === "iOS") {
            get_huella();

        } else if (check_so() === "Android") {

            huella = Android.demo();

        }
    }

    function check_tarjeta(tarjeta_actual) {

        if (tarjeta_actual.indexOf('*') === -1) {

            tarjeta_actual = $("#numero_tarjeta").val();

        } else {

            tarjeta_actual = $("#return-numero-tarjeta").val();

        }

        return tarjeta_actual;


    }


    function js_login(resultado, stringkey) {

        if (resultado === undefined) {
            var instance = "";
            var ippublicamovil = "";

            console.log(true);

            if (check_so() === "iOS") {
                instance = "${codigoInstalacion_iOS}";
                ippublicamovil = '${IP_iOS}';
            } else if (check_so() === "Android") {
                instance = Android.getInstanceID();
                ippublicamovil = Android.get_ip_cell_phone();
            } else {
                instance = "100"
                ippublicamovil = "192.168.1.1";
            }


            var tarjeta_actual = $("#numero_tarjeta").val();

            tarjeta_actual = check_tarjeta(tarjeta_actual);

            $.ajax({
                url: '${pageContext.request.contextPath}/LoginServlet',
                type: 'post',
                dataType: "text",
                beforeSend: function () {
                    $("#ingresar_ch").attr("disabled", true).addClass("btn-caja-activo").removeClass("btn-caja");
                },
                data: {
                    'accion': "autenticar",
                    'codigoTarjeta': $("#codigo_tarjeta").val().split("%%%")[0],
                    'numeroTarjeta': tarjeta_actual,
                    'claveWeb': $("#numero_password").val(),
                    'ippublicamovil': ippublicamovil,
                    "estadoHuellaDigital": "false",
                    'codigoInstalacion': instance,
                    'estadoRecordar': $("#recordar").is(":checked") ? 'true' : 'false',
                    "huellaCifrada": ""
                },
                error: function () {
                    window.location.href = "${pageContext.request.contextPath}/";
                },
                success: function (salida) {

                    $("#ingresar_ch").attr("disabled", false).addClass("btn-caja").removeClass("btn-caja-activo");

                    $("#myModal").modal('hide');
                    if (salida.length !== 0) {

                        var array = salida.split("%%%");
                        if (array[0] === "0") {
                            ingresar_cuentas();
                        } else {
                            message_req("Mensaje", array[1]);
                        }

                    } else {
                        message_req("Mensaje", "Error Inesperado");
                    }
                }
            });

        } else {
            if (check_so() === "iOS") {
                ingresa_con_ios(resultado, stringkey);
            } else if (check_so() === "Android") {
                var estado_huella_digital = resultado;

                var instance = Android.getInstanceID();
                var ippublicamovil = Android.get_ip_cell_phone();

                var tarjeta_actual = $("#numero_tarjeta").val();

                tarjeta_actual = check_tarjeta(tarjeta_actual);

                if (estado_huella_digital === "true") {
                    $.ajax({
                        url: '${pageContext.request.contextPath}/LoginServlet',
                        type: 'post',
                        dataType: "text",
                        beforeSend: function () {

                        },
                        data: {
                            'accion': "autenticar",
                            'codigoTarjeta': $("#codigo_tarjeta").val().split("%%%")[0],
                            'numeroTarjeta': tarjeta_actual,
                            'claveWeb': "",
                            'ippublicamovil': ippublicamovil,
                            "estadoHuellaDigital": "true",
                            'codigoInstalacion': instance,
                            'estadoRecordar': $("#recordar").is(":checked") ? 'true' : 'false',
                            "huellaCifrada": stringkey
                        },
                        error: function () {
                            window.location.href = "${pageContext.request.contextPath}/";
                        },
                        success: function (salida) {
                            $("#myModal").modal('hide');
                            if (salida.length !== 0) {

                                var array = salida.split("%%%");
                                if (array[0] === "0") {
                                    ingresar_cuentas();
                                } else {
                                    message_req("Mensaje", array[1]);
                                }

                            } else {
                                message_req("Mensaje", "Error Inesperado");
                            }
                        }
                    });
                }
            }

        }
    }
    function ingresa_con_ios(flag, idhuella) {
        var instance = "${codigoInstalacion_iOS}";
        var ippublicamovil = '${IP_iOS}';

        var tarjeta_actual = $("#numero_tarjeta").val();

        tarjeta_actual = check_tarjeta(tarjeta_actual);

        if (flag === "true") {
            $.ajax({
                url: '${pageContext.request.contextPath}/LoginServlet',
                type: 'post',
                dataType: "text",
                beforeSend: function () {

                },
                data: {
                    'accion': "autenticar",
                    'codigoTarjeta': $("#codigo_tarjeta").val().split("%%%")[0],
                    'numeroTarjeta': tarjeta_actual,
                    'claveWeb': $("#numero_password").val(),
                    'ippublicamovil': ippublicamovil,
                    'codigoInstalacion': instance,
                    'estadoRecordar': $("#recordar").is(":checked") ? 'true' : 'false',
                    "estadoHuellaDigital": flag,
                    "huellaCifrada": idhuella
                },
                error: function () {
                    window.location.href = "${pageContext.request.contextPath}/";
                },
                success: function (salida) {

                    if (salida.length !== 0) {

                        var array = salida.split("%%%");
                        if (array[0] === "0") {
                            ingresar_cuentas();
                        } else {
                            message_req("Mensaje", array[1]);
                        }

                    } else {
                        message_req("Mensaje", "Error Inesperado");
                    }
                }
            });
        }
    }
    function ingresar_cuentas() {

        $.ajax({
            url: '${pageContext.request.contextPath}/ResumenCuentaServlet',
            type: 'post',
            dataType: "text",
            beforeSend: function () {
                $("#ingresar_ch").attr("disabled", true).addClass("btn-caja-activo").removeClass("btn-caja");
            },
            data: {
                'accion': "listar_cuentas"

            },
            error: function () {
                window.location.href = "${pageContext.request.contextPath}/";
            },
            success: function (salida) {

                $("#ingresar_ch").attr("disabled", false).addClass("btn-caja").removeClass("btn-caja-activo");

                if (salida.length !== 0) {

                    var array = salida.split("%%%");
                    if (array[0] === "0") {
                        crearForm(array[1]);
                     } else {
                        message_req("Mensaje", array[1]);
                    }

                } else {
                    message_req("Mensaje", "Error Inesperado");
                }
            }
        });

    }

</script>