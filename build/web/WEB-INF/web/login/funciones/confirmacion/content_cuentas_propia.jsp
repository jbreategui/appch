<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<div class="row cuerpo" >


    <div class="col-xs-12 col-md-12 nopad box box-caja"  style="margin-top: 10px;margin-bottom:0px">


        <div class="col-xs-12 text-center" style="padding: 8px; margin-bottom: 0px;border-bottom:1px solid #808080">

            <p class="text-center negrita titulo" >Confirmación Transferencia a Cuentas Propias</p>

        </div>
    </div>


    <div class="col-xs-12 col-md-12 nopad box box-caja"  style="margin-top: 10px">

        <div class="col-xs-12 item-list-caja">

            <span class="negrita" style="font-size: 15px ">Cuenta origen: <span>${confirmacion_cuentas_propias[0]}</span></span>  


        </div>

        <div class="col-xs-12 item-list-caja">

            <span class="negrita" style="font-size: 15px">Cuenta destino: <span>${confirmacion_cuentas_propias[1]} </span></span>  


        </div>

        <div class="col-xs-12 item-list-caja">

            <span class="negrita" style="font-size: 15px">Moneda y monto: <span>${confirmacion_cuentas_propias[2]}</span><span  id="monto_propia_trans"> ${confirmacion_cuentas_propias[3]}</span></span>  


        </div>

    </div>


    <div class="col-xs-12 text-center">




        <div style="margin-bottom:15px">
            <span>Guardar como operación frecuente  </span> 
            <span style="display:inline-block;height: 16px;padding-left:5px">
                <div class="onoffswitch" style="top:8px">
                    <input type="checkbox" name="onoffswitch" class="onoffswitch-checkbox" id="myonoffswitch" >
                    <label class="onoffswitch-label" for="myonoffswitch">
                        <span class="onoffswitch-inner" ></span>
                        <span class="onoffswitch-switch"></span>
                    </label>
                    <input type="hidden" id="estado_ope" value="false">
                </div>
            </span>

        </div>




        <div class="col-xs-9 col-xs-offset-3" style="margin-top: 10px">   

            <input readonly="false"  style="text-align: right" type="text"  class="form-control " placeholder="Ejemplos: luz casa, Cuenta papá" id="ope_frecuente">


        </div>



    </div>

    <div class="col-xs-12" style="margin-top: 10px">

        <div class="col-xs-6 text-center">
            <a onclick="ingresar_transferencias();" class="btn btn-caja btn-block">Cancelar</a>
        </div>



        <div class="col-xs-6 text-center">
            <button id="btn-confirmar" onclick="js_constancia();" class="btn btn-caja btn-block cambiar_pagina">Confirmar</button>
        </div><br><br>



    </div>

</div>

<jsp:include page="/web/glb/footer_login.jsp" />

<jsp:include page="/web/glb/script_general.jsp" />

<script>
    $(function () {




        $("img#footer_transferencias").attr('src', '${pageContext.request.contextPath}/img/i-celular.png');
        $("#monto_propia_trans").number(true, 2);

        var mensaje = $("#hdd_mensaje_login").val();

        if (mensaje.length > 0) {
            message_req("Mensaje", mensaje);
        }


        $('.onoffswitch-checkbox').on('change', function () {
            var is_checked = $(this).is(':checked');


            if (is_checked === true) {
                document.getElementById("estado_ope").value = "true";
                $('#ope_frecuente').removeAttr("readonly");
                //   $('#ope_frecuente').removeAttr("type"); 

            } else {
                document.getElementById("estado_ope").value = "false";

                $('#ope_frecuente').attr("readonly", "true");
                //  $('#ope_frecuente').attr("type", "hidden");
            }

        });

        // Params ($selector, boolean)
        function setSwitchState(el, flag) {
            el.attr('checked', flag);
        }

        // Usage
        setSwitchState($('.myonoffswitch'), true);

    });





    function js_constancia() {


        ope_frecuente = document.getElementById("ope_frecuente").value;
        estado_ope = document.getElementById("estado_ope").value;


        $.ajax({
            url: '${pageContext.request.contextPath}/CuentaPropiaServlet',
            type: 'post',
            dataType: "text",
            beforeSend: function () {
                $("#btn-confirmar").attr("disabled",true).addClass("btn-caja-activo").removeClass("btn-caja");
            },
            data: {
                accion: 'constancia_transferencia_cuenta_propia',
                ope_frecuente: ope_frecuente,
                estado_ope: estado_ope,
                ippublicamovil: ip_movil()

            },
            success: function (salida) {
                $("#btn-confirmar").attr("disabled",false).addClass("btn-caja").removeClass("btn-caja-activo");

                if (salida.length !== 0) {

                    var array = salida.split("%%%");
                    if (array[0] === "0") {
                        crearForm(array[1]);
                     } else {
                        message_req("Mensaje", array[1]);
                    }

                } else {
                    message_req("Mensaje", "Error Inesperado");
                }
            },
            error: function () {
                window.location.href = "${pageContext.request.contextPath}/";
                
            }
        });

    }


    function ingresar_transferencias() {

        $.ajax({
            url: '${pageContext.request.contextPath}/CuentaPropiaServlet',
            type: 'post',
            dataType: "text",
            beforeSend: function () {

            },
            data: {
                'accion': "transferencia"

            },
            success: function (salida) {

                if (salida.length !== 0) {

                    var array = salida.split("%%%");
                    if (array[0] === "0") {
                        crearForm(array[1]);
                     } else {
                        message_req("Mensaje", array[1]);
                    }

                } else {
                    message_req("Mensaje", "Error Inesperado");
                }
            }
            ,
            error: function () {
                window.location.href = "${pageContext.request.contextPath}/";
            }
        });

    }


</script>