<script>

    $(function () {

        var origen = "<%= session.getAttribute("origen")%>";

        $("#footer_mas").removeClass('footer-gris-img');

        $("#op_frec").addClass('op_frec_elegida');


        $("body").on("click", "#btn-editar", function (event) {

            event.preventDefault();
            $(this).toggleClass('btn-elegido-caja');

            if ($("#btn-editar").hasClass('btn-elegido-caja')) {

                $(".frecuentes_borrar").show();
                $(".frecuentes_edicion").hide();

                $(".operaciones").show();
                $(".imagen_show").hide();
                $(".pregunta_frecuente").removeClass('redirigir');
                $(".borrar").removeClass('oculto');
            } else {

                $(".frecuentes_borrar").hide();
                $(".frecuentes_edicion").show();

                $(".operaciones").hide();
                $(".imagen_show").show();
                $(".pregunta_frecuente").addClass('redirigir');

                $(".borrar").addClass('oculto');

            }


        });


    });
    function borrar_oper_frec() {
        var codigo = [];
        $.each($("input:checkbox[name=radioButton]:checked"), function (key, value) {
            var arr = value.id;
            codigo.push(arr.split("__")[1]);

        });

        if (codigo.length > 0) {

            $.ajax({
                url: '${pageContext.request.contextPath}/OperFrecuentesServlet',
                type: 'post',
                dataType: "text",
                beforeSend: function () {

                },
                data: {
                    accion: 'eliminar_oper_frec',
                    array: codigo
                },
                success: function (salida) {

                    if (salida.length !== 0) {

                        var array = salida.split("%%%");
                        if (array[0] === "0") {
                             crearForm(array[1]);
                        } else {
                            message_req("Mensaje", array[1]);
                        }

                    } else {
                        message_req("Mensaje", "Error Inesperado");
                    }
                },
                error: function () {
                    window.location.href = "${pageContext.request.contextPath}/";
                }
            });
        } else {
            message_req("Mensaje", "Seleccione operación frecuente");
        }
    }


    function ir_operacion_frecuente(codigo_operacion) {



        $.ajax({
            url: '${pageContext.request.contextPath}/OperFrecuentesServlet',
            type: 'post',
            dataType: "text",
            beforeSend: function () {

            },
            data: {
                accion: 'ultra_instinto',
                codigo_operacion: codigo_operacion
            },
            success: function (salida) {

                if (salida.length !== 0) {

                    var array = salida.split("%%%");
                    if (array[0] === "0") {
                        ajax_blue(array[1], array[2]);
                    } else {
                        message_req("Mensaje", array[1]);
                    }

                } else {
                    message_req("Mensaje", "Error Inesperado");
                }
            },
            error: function () {
                window.location.href = "${pageContext.request.contextPath}/";
            }
        });
    }



    function ajax_blue(acc, cotrol) {

        $.ajax({
            url: '${pageContext.request.contextPath}/' + cotrol,
            type: 'post',
            dataType: "text",
            data: {
                accion: acc
            },
            success: function (salida) {

                if (salida.length !== 0) {

                    var array = salida.split("%%%");
                    if (array[0] === "0") {
                        crearForm(array[1]);
                    } else {
                        message_req("Mensaje", array[1]);
                    }

                } else {
                    message_req("Mensaje", "Error Inesperado");
                }
            },
            error: function () {
                window.location.href = "${pageContext.request.contextPath}/";
            }
        });

    }






</script>