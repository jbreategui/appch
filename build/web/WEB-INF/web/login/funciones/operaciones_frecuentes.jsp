<!DOCTYPE html>
<html>
    <head>

        <jsp:include page="/web/glb/head_general.jsp" />

        <jsp:include page="operaciones_frecuentes/head.jsp" />

    </head>
    <body>


        <jsp:include page="/web/glb/header_login.jsp" />  
        <%
            String referrer = (String) request.getSession().getAttribute("rutaServlet");
            String content = "";
            if (referrer.compareTo("OperFrecuentesServlet") == 0) {// poner de donde viene
                content = "operaciones_frecuentes/content.jsp";
            }
        %>
        <jsp:include page="<%=content%>" />

        <jsp:include page="/web/glb/footer_login.jsp" />

        <jsp:include page="/web/glb/script_general.jsp" />

        <jsp:include page="operaciones_frecuentes/script.jsp" />


    </body
</html>