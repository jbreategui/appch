<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<div class="row cuerpo">

    <div class="col-xs-12 col-md-12 nopad box box-caja"  style="margin-top: 10px;margin-bottom:0px">


        <div class="col-xs-12 text-center" style="padding: 8px; margin-bottom: 0px;border-bottom:1px solid #808080">
            <span class="text-center negrita titulo"> Recarga de celular:</span><br/>
            <span  id="titulo" class="negrita">Selecciona la cuenta a cargo</span>

        </div>

    </div>



    <div class="col-xs-12 col-md-12 nopad box box-caja"  style="margin-top: 10px">

        <c:forEach var="lista" items="${elegir_cuenta_recarga_celular}">


            <a class="cambiar_pagina" onclick="elegir_cuenta_cargo('${lista[4]}', '${lista[3]}');" style="color:black">

                <div class="col-xs-12 cuenta-elegida" style="padding: 8px; margin-bottom: 0px;border-bottom:1px solid #808080">

                    <span class="negrita">${lista[1]} </span>
                    <span class=" pull-right glyphicon glyphicon-play text-color-1"></span>
                </div>
            </a>

        </c:forEach>



    </div>




</div>

<jsp:include page="/web/glb/footer_login.jsp" />

<jsp:include page="/web/glb/script_general.jsp" />

<script>

    $(function () {

        $("img#footer_pagos").attr('src', '${pageContext.request.contextPath}/img/i-billetera.png');

    });


    function elegir_cuenta_cargo(indicador_cuenta, mondeda) {

        $.ajax({
            url: '${pageContext.request.contextPath}/PagosRecargasServlet',
            type: 'post',
            dataType: "text",
            beforeSend: function () {

            },
            data: {
                accion: 'listar_operadores',
                indicador_cuenta: indicador_cuenta,
                moneda: mondeda


            },
            success: function (salida) {

                if (salida.length !== 0) {

                    var array = salida.split("%%%");
                    if (array[0] === "0") {
                        crearForm(array[1]);
                    } else {
                        message_req("Mensaje", array[1]);
                    }

                } else {
                    message_req("Mensaje", "Error Inesperado");
                }
            },
            error: function () {
                window.location.href = "${pageContext.request.contextPath}/";
            }
        });


    }


</script>