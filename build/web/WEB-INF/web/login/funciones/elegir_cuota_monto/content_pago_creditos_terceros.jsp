
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>


<div class="row cuerpo">


    <div class="col-xs-12 text-center">

        <span class="negrita" style="font-size:18px">Seleccione cuota</span>

    </div>

    <div class="col-xs-12 col-md-12 nopad box box-caja"  style="margin-top: 10px">

        <c:forEach var="lista" items="${elegir_cuota_monto_creditos_terceros}">

            <div class="col-xs-12" style="padding: 0px 8px; margin-bottom: 0px;border-bottom:1px solid #808080">
                <div class="text-center">Cuota ${lista[1]}</div>
                <div class="text-center"> 
                    <label class="container negrita" style="margin-bottom:0px;padding:0px;font-size:14px">${lista[2]}  <span class="formato_monto"> ${lista[4]}</span>
                        <input onclick="calcularTotal();"  name="radioButton" id="id_array_cuota__${lista[1]}" type="checkbox">
                        <input type="hidden" id="cuota__${lista[1]}"value="${lista[4]}" >
                        <span class="checkmark"></span>
                    </label>
                </div>
                <div class="text-center">Vence: ${lista[0]}</div>

            </div>

        </c:forEach>
        <div class="col-xs-12" style="padding: 0px 8px 8px 8px ; margin-bottom: 0px;border-bottom:1px solid #808080">

            <div class="text-center" style="padding-top: 10px"></div>
            <div class="text-center"> 
                <label class="container" style="margin-bottom:0px;padding:0px;font-size:14px">

                    <span class="pull-left">
                        Digite monto: <input id="listar_cuotas_monto" type="${type}" placeholder="${lista_primera_cuota[2]}" style="position: relative;opacity:1;border-radius:10px;padding-left:5px">
                    </span>

                </label>
            </div>

        </div>

    </div>

    <div class="col-xs-6 text-center">
        <a onclick="ingresar_pagos();" class="btn btn-caja btn-block">Cancelar</a>
    </div>
    <div class="col-xs-6 text-center">
        <a onclick="listar_cuotas_selec();" class="btn btn-caja btn-block">Siguiente</a>
    </div>
    <br><br>

</div>
<script>

    var montoTotal = 0.0;

    function calcularTotal() {
        var montoTotal = 0.0;
        $.each($("input:checkbox[name=radioButton]:checked"), function (key, value) {
            var arr = value.id;
            var cuota = arr.split("__")[1];

            var mon = parseFloat($("#cuota__" + cuota).val());
            montoTotal += mon;

        });
        $("#listar_cuotas_monto").val(montoTotal.toFixed(2));

    }

    function validar_campos() {
        var array = [];
        var result = "<ul>";
        var data = $("input:checkbox[name=radioButton]:checked");
        var monto = $("#listar_cuotas_monto").val();


        var flag = true;

        if (data.length === 0) {
            result += "<li>Seleccione cuota a pagar</li>";
            flag = false;
        } else {
            var bol = false;

            $.each($("input:checkbox[name=radioButton]:checked"), function (key, value) {
                var arr = value.id;
                var id = arr.split("__")[1];

                if ('${lista_primera_cuota[1]}' === id) {
                    bol = true;
                    return bol;
                }
            });

            if (!bol) {
                result += "<li>Ingrese la cuota m�s vencida</li>";
                flag = false;
            }
        }
        if (monto.length === 0) {
            result += "<li>Ingrese monto</li>";
            flag = false;
        } else {
            var reg = /^\d+(?:\.\d{1,2})?$/;
            if (!reg.test(monto)) {
                result += "<li>Ingrese monto correcto</li>";
                flag = false;
            }
            if (monto <= 0) {
                result += "<li>Monto debe ser mayor a cero</li>";
                flag = false;
            }
        }

        result += "</ul>";
        array = [flag, result];

        return array;
    }
    function listar_cuotas_selec() {
        var array = validar_campos();
        if (array[0] === true) {
            funcion_validar_seleccionar();
        } else {

            message_req("Mensaje", array[1]);
        }
    }




    function funcion_validar_seleccionar() {

        var monto = $("#listar_cuotas_monto").val();

        $.ajax({
            url: '${pageContext.request.contextPath}/PagoCreditoTerceroServlet',
            type: 'post',
            dataType: "text",
            beforeSend: function () {

            },
            data: {
                accion: 'elegir_cuenta_cargo',
                listar_cuotas_monto: monto
            },
            success: function (salida) {

                if (salida.length !== 0) {

                    var array = salida.split("%%%");
                    if (array[0] === "0") {
                        crearForm(array[1]);
                    } else {
                        message_req("Mensaje", array[1]);
                    }

                } else {
                    message_req("Mensaje", "Error Inesperado");
                }
            },
            error: function () {
                window.location.href = "${pageContext.request.contextPath}/";
            }
        });


    }

    function ingresar_pagos() {

        $.ajax({
            url: '${pageContext.request.contextPath}/PagoCreditoPropioServlet',
            type: 'post',
            dataType: "text",
            beforeSend: function () {

            },
            data: {
                'accion': "pagos"

            },
            success: function (salida) {

                if (salida.length !== 0) {

                    var array = salida.split("%%%");
                    if (array[0] === "0") {
                        crearForm(array[1]);
                    } else {
                        message_req("Mensaje", array[1]);
                    }

                } else {
                    message_req("Mensaje", "Error Inesperado");
                }
            },
            error: function () {
                window.location.href = "${pageContext.request.contextPath}/";
            }
        });

    }

</script>
