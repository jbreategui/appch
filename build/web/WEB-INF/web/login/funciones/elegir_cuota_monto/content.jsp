
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>


<div class="row cuerpo">


    <div class="col-xs-12 text-center">

        <span class="negrita" style="font-size:18px">Selecciona las cuotas</span>

    </div>

    <div class="col-xs-12 col-md-12 nopad box box-caja"  style="margin-top: 10px">

        <c:forEach var="lista" items="${list_elegir_cuota_monto}">

            <div class="col-xs-12" style="padding: 0px 8px; margin-bottom: 0px;border-bottom:1px solid #808080">
                <div class="text-center">Cuota ${lista[1]}</div>
                <div class="text-center"> 
                    <label class="container negrita" style="margin-bottom:0px;padding:0px;font-size:14px">${lista[2]} ${lista[3]}
                        <input name="radioButton" id="id_array_cuota__${lista[0]}" type="radio">
                        <span class="checkmark"></span>
                    </label>
                </div>
                <div class="text-center">Vence ${lista[4]}</div>

            </div>

        </c:forEach>
        <div class="col-xs-12" style="padding: 0px 8px 8px 8px ; margin-bottom: 0px;border-bottom:1px solid #808080">

            <div class="text-center" style="padding-top: 10px"></div>
            <div class="text-center"> 
                <label class="container" style="margin-bottom:0px;padding:0px;font-size:14px">

                    <span class="pull-left">
                        Digite monto: <input id="list_elegir_cuota_monto_monto" type="text" placeholder="S/" style="position: relative;opacity:1;border-radius:10px;padding-left:5px">
                    </span>

                </label>
            </div>

        </div>

    </div>

    <div class="col-xs-6 text-center">
        <a onclick="ingresar_pagos();" class="btn btn-caja btn-block">Cancelar</a>
    </div>
    <div class="col-xs-6 text-center">
        <a href="#" class="btn btn-caja btn-block">Confirmar</a>
    </div>
    <br><br>

</div>


<script>
    function ingresar_pagos() {

        $.ajax({
            url: '${pageContext.request.contextPath}/PagoCreditoPropioServlet',
            type: 'post',
            dataType: "text",
            beforeSend: function () {

            },
            data: {
                'accion': "pagos"

            },
            success: function (salida) {

                if (salida.length !== 0) {

                    var array = salida.split("%%%");
                    if (array[0] === "0") {
                        crearForm(array[1]);
                     } else {
                        message_req("Mensaje", array[1]);
                    }

                } else {
                    message_req("Mensaje", "Error Inesperado");
                }
            },
            error: function () {
                window.location.href = "${pageContext.request.contextPath}/";
            }
        });

    }


</script>