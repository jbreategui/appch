<script>

    $(function () {

        $("#footer_transferencias").removeClass('footer-gris-img');

        $("body").on("click", ".elegir-moneda", function () {

            switch (this.id) {

                case "soles":
                    $("#dolares").removeClass("btn-elegido");
                    $(this).addClass("btn-elegido");
                    break;

                case "dolares":
                    $("#soles").removeClass("btn-elegido");
                    $(this).addClass("btn-elegido");
                    break;

            }

        });

    });
    var _flag = 0;
    function noback() {

        window.location.hash = "no-back-button";
        window.location.hash = "Again-No-back-button";

        window.onhashchange = function () {
            window.location.hash = "no-back-button";

            if (_flag === 3) {
                volver_home();
            }
            _flag++;
            console.log(_flag);
        };

    }


    function volver_home() {
        $.ajax({
            url: '${pageContext.request.contextPath}/ResumenCuentaServlet',
            type: 'post',
            dataType: "text",
            beforeSend: function () {

            },
            data: {
                'accion': "listar_cuentas"

            },
            success: function (salida) {

                if (salida.length !== 0) {

                    var array = salida.split("%%%");
                    if (array[0] === "0") {
                        crearForm(array[1]);
                     } else {
                        message_req("Mensaje", array[1]);
                    }

                } else {
                    message_req("Mensaje", "Error Inesperado");
                }
            },
            error: function () {
                window.location.href = "${pageContext.request.contextPath}/";
            }
        });
    }
</script>