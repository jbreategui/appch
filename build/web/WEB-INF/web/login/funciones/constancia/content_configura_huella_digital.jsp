<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<div class="row cuerpo" >


    <div class="col-xs-12 col-md-12" style="margin-top: 0px">


        <p class="text-center" style="font-family: futura;font-size:30px;font-weight: 700">
            Constancia

        </p>
    </div>

    <div class="col-xs-12 col-md-12 nopad"  style="margin-top: 0px">

        <div class="col-xs-12 box box-caja" style="padding: 20px">

            <div class="form-group">

                <p><span style="font-weight: 900">Estado de operación: </span>Exitosa</p>

            </div>

            <div class="form-group">

                <p><span style="font-weight: 900"> Fecha:  </span> ${componentes_registro_huella[0]} &nbsp;<span style="font-weight: 900"> Hora:  </span> ${componentes_registro_huella[1]} </p>

            </div>
            <div class="form-group">

                <p><span style="font-weight: 900"> Mensaje:  </span> ${componentes_registro_huella[2]}

            </div>

        </div>

    </div>

<!--    <div class="col-xs-12 nopad">

        <div class="col-xs-offset-3 col-xs-6 text-center">
            <button onclick="compartir();" class="btn btn-caja btn-block" style="padding:5px">Enviar Constancia</button>
        </div>

    </div>-->

</div>
            
<jsp:include page="/web/glb/footer_login.jsp" />

<jsp:include page="/web/glb/script_general.jsp" />
        
<script> 

$(function(){
    
    $("img#footer_mas").attr('src', '${pageContext.request.contextPath}/img/i-puntos.png');
       
});

//function compartir() {
//
//        var ary = ${compartir_registro_huella};
//
//        var texto = "Constancia de Huella Digital" + "\n";
//        texto += "-------------------------------------" + "\n";
//        texto += "Fecha :" + " " + ary.fecha + "\n";
//        texto += "Hora :" + " " + ary.hora + "\n";
//        texto += "Mensaje :" + " " + ary.mensaje + "\n";
//
//        if (check_so() === "iOS") {
//            set_shared(texto);
//        } else if (check_so() === "Android") {
//            Android.shared_with(texto);
//        }
//
//    }

</script>