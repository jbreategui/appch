<div class="row cuerpo" >


    <div class="col-xs-12 col-md-12" style="margin-top: 0px">


        <p class="text-center" style="font-family: futura;font-size:20px;font-weight: 700">
            Constancia de Env�o de giros

        </p>
    </div>

    <div class="col-xs-12 col-md-12 nopad"  style="margin-top: 0px">
        <div class="form-group" style="text-align: center">

            <span style="font-weight: 900">C�digo de operaci�n: </span>${constancia[0]}   <span style="font-weight: 900">Fecha: </span>${constancia[1]}   <span style="font-weight: 900">Hora: </span>${constancia[2]}
        </div>

        <div class="col-xs-12 box box-caja" style="padding: 20px">



            <div class="form-group">

                <p><span style="font-weight: 900">Cuenta cargo: </span>${constancia[3]}</p>

            </div>


            <div class="form-group">

                <p><span style="font-weight: 900">Beneficiario: </span>${constancia[5]}</p>
                <p><span style="font-weight: 900">DNI: </span>${constancia[4]}</p>
                <p><span style="font-weight: 900">Destino: </span>${constancia[8]}</p>

            </div>

            <div class="form-group">

                <p><span style="font-weight: 900"> Comisi�n: </span>${constancia[6]} ${constancia[9]} </p>

            </div>


            <div class="form-group">

                <p><span style="font-weight: 900"> Moneda y Monto: </span> <span id="monedamonto"> ${constancia[6]} ${constancia[7]} </span> </p>

            </div>

            <div class="col-xs-12 nopad">

                <div class="col-xs-offset-3 col-xs-6 text-center">
                    <button onclick="compartir();" class="btn btn-caja btn-block" style="padding:5px">Enviar Constancia</button>
                </div>

            </div>    




        </div>

    </div>



</div>

<jsp:include page="/web/glb/footer_login.jsp" />

<jsp:include page="/web/glb/script_general.jsp" />

<script>

    $(function () {

        $("img#footer_pagos").attr('src', '${pageContext.request.contextPath}/img/i-billetera.png');

        $("#monedamonto").text(${compartir_giro}.monedamonto);

    });


    function compartir() {


        var ary = ${compartir_giro};

        var texto = "Constancia Env�o de Giros" + "\n";
        texto += "------------------------------------" + "\n";
        texto += "C�digo Operaci�n :" + " " + ary.cod + "\n";
        texto += "Fecha :" + " " + ary.fecha + "\n";
        texto += "Hora :" + " " + ary.hora + "\n";
        texto += "Cuenta origen :" + " " + ary.cuenta + "\n";
        texto += "N�mero de DNI :" + " " + ary.dni + "\n";
        texto += "Datos :" + " " + ary.datos + "\n";
        texto += "Monto:" + " " + ary.monedamonto + "\n";
        texto += "Destino:" + " " + ary.destino + "\n";
        texto += "Comisi�n :" + " " + ary.comision + "\n";


        if (check_so() === "iOS") {
            set_shared(texto);
        } else if (check_so() === "Android") {
            Android.shared_with(texto);
        }

    }

</script>