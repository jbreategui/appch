<!DOCTYPE html>
<html>
    <head>
        
        <jsp:include  page="../../web/glb/head_general.jsp" />

        <jsp:include page="./index/head.jsp" />

    </head>
    <body>
        
        <jsp:include page="./index/content.jsp" />
        
        <jsp:include page="../../web/glb/footer_guest.jsp" />
        
        <jsp:include page="../../web/glb/script_general.jsp" />

        <jsp:include page="./index/script.jsp" />
        
        
    </body
</html>