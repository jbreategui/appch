<!DOCTYPE html>
<html>
    <head>

        <jsp:include page="/web/glb/head_general.jsp" />

        <jsp:include page="ingresar_monto_cuenta_interbancaria/head.jsp" />

    </head>
    <body>

        <jsp:include page="/web/glb/header_login.jsp" />

        <jsp:include page="ingresar_monto_cuenta_interbancaria/content.jsp" />

        <jsp:include page="/web/glb/footer_login.jsp" />
        <jsp:include page="/web/glb/script_general.jsp" />

        <jsp:include page="ingresar_monto_cuenta_interbancaria/script.jsp" />


    </body
</html>