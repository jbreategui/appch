<!-- jQuery -->
<script src="${pageContext.request.contextPath}/jq/jquery-3.2.1.min.js" type="text/javascript"></script>
<!-- Bootstrap -->
<script src="${pageContext.request.contextPath}/jq/bootstrap/bootstrap.min.js" type="text/javascript"></script>

<script src="${pageContext.request.contextPath}/js/number.js" type="text/javascript"></script>
<script>

    $(function () {

        $("img#footer_transferencias").attr('src', '${pageContext.request.contextPath}/img/i-celular.png');

        function mon_tip() {

            var tipo_mon = document.getElementById("tipo_mon_ori").value;

            if (tipo_mon === "1")
            {
                $("#dolares").removeClass("btn-elegido");
                $("#soles").addClass("btn-elegido");
                document.getElementById("monto_propia_trans").placeholder = "S/        ";
                $("#codigo_moneda").val("S/");

            } else if (tipo_mon === "2") {
                $("#dolares").addClass("btn-elegido");
                $("#soles").removeClass("btn-elegido");
                document.getElementById("monto_propia_trans").placeholder = "USD       ";
                $("#codigo_moneda").val("USD");

            }

        }
        ;
        mon_tip();


        var cuentadestino = "${inputs_AP02[6]}";
        if (cuentadestino.length !== 0) {
            $("#cuenta_destino").val(cuentadestino.trim());
        }
    });

    function validarCampos() {

        var result = [];
        var mensaje = "<ul>";
        var flag = true;
        monto = document.getElementById("monto_propia_trans").value;

        if (monto.length === 0) {
            mensaje += "<li>Ingrese un monto</li>";
            flag = false;
        }

        mensaje += "</ul>";
        result = [flag, mensaje];
        return result;
    }

    function js_confir_tran_tercero() {

        codigo_moneda = document.getElementById("codigo_moneda").value;
        monto = document.getElementById("monto_propia_trans").value;
        cuenta_destino = document.getElementById("cuenta_destino").value;

        var validar = validarCampos();

        if (validar[0] === false) {

            message_req("Mensaje", validar[1]);
            return;
        }

        $.ajax({
            url: '${pageContext.request.contextPath}/CuentaTerceroServlet',
            type: 'post',
            dataType: "text",
            beforeSend: function () {

            },
            data: {
                accion: 'confirmar_transferencia_cuenta_tercero',
                codigoMoneda: codigo_moneda,
                monto: monto,
                cuenta_destino: cuenta_destino

            },
            success: function (salida) {


                if (salida.length !== 0) {

                    var array = salida.split("%%%");
                    if (array[0] === "0") {
                        crearForm(array[1]);
                    } else {
                        message_req("Mensaje", array[1]);
                    }

                } else {
                    message_req("Mensaje", "Error Inesperado");
                }
            },
            error: function () {
                window.location.href = "${pageContext.request.contextPath}/";
            }
        });

    }

    function cancelar_transferencia() {

        $.ajax({
            url: '${pageContext.request.contextPath}/CuentaPropiaServlet',
            type: 'post',
            dataType: "text",
            beforeSend: function () {

            },
            data: {
                'accion': "transferencia"

            },
            success: function (salida) {

                if (salida.length !== 0) {

                    var array = salida.split("%%%");
                    if (array[0] === "0") {
                        crearForm(array[1]);
                    } else {
                        message_req("Mensaje", array[1]);
                    }

                } else {
                    message_req("Mensaje", "Error Inesperado");
                }
            },
            error: function () {
                window.location.href = "${pageContext.request.contextPath}/";
            }
        });

    }
</script>