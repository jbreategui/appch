<!DOCTYPE html>
<html>
    <head>

        <jsp:include page="/web/glb/head_general.jsp" />

        <jsp:include page="ejecuta_transferencia/head.jsp" />

    </head>
    <body>

        <jsp:include page="/web/glb/header_login.jsp" />

        <%
            String referrer = (String) request.getSession().getAttribute("rutaServlet");
            String content = "";
            if (referrer.compareTo("CuentaPropiaServlet-ingresa_monto_propia") == 0) {// poner de donde viene
                content = "ejecuta_transferencia/content.jsp";
            }
        %>
        <jsp:include page="<%=content%>" />
        <jsp:include page="/web/glb/script_general.jsp" />

        <jsp:include page="ejecuta_transferencia/script.jsp" />



    </body
</html>