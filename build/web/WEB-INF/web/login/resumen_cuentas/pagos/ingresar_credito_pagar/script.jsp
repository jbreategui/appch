<script>

    $("img#footer_pagos").attr('src','${pageContext.request.contextPath}/img/i-billetera.png');

    function validar_campos() {
        var array = [];
        var result = "<ul>";
        var numero_cuenta = $("#numero_cuenta").val();

        var flag = true;
        if (numero_cuenta.length === 0) {
            result += "<li>Ingrese n�mero de cuenta </li>";
            flag = false;
        }

        result += "</ul>";
        array = [flag, result];
        return array;
    }


    function js_validar_credito_tercero() {

        var array = validar_campos();
        if (array[0] === true) {
            funcion_validar_credito_tercero();
        } else {

            message_req("Mensaje", array[1]);
        }
    }

    function funcion_validar_credito_tercero() {

        var numero_cuenta = $("#numero_cuenta").val();

        $.ajax({
            url: '${pageContext.request.contextPath}/PagoCreditoTerceroServlet',
            type: 'post',
            dataType: "text",
            beforeSend: function () {

            },
            data: {
                accion: 'validar_cuenta_credito',
                cuenta_credito_destino: numero_cuenta

            },
            success: function (salida) {


                if (salida.length !== 0) {

                    var array = salida.split("%%%");
                    if (array[0] === "0") {
                        crearForm(array[1]);
                     } else {
                        message_req("Mensaje", array[1]);
                    }

                } else {
                    message_req("Mensaje", "Error Inesperado");
                }
            },
            error: function () {
                window.location.href = "${pageContext.request.contextPath}/";
            }
        });


    }



</script>