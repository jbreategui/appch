<!-- jQuery -->
<script src="${pageContext.request.contextPath}/jq/jquery-3.2.1.min.js" type="text/javascript"></script>
<!-- Bootstrap -->
<script src="${pageContext.request.contextPath}/jq/bootstrap/bootstrap.min.js" type="text/javascript"></script>
<script src="${pageContext.request.contextPath}/jq/jquery.redirect.js" type="text/javascript"></script>
<script>

    $(function () {

        $("img#footer_pagos").attr('src', '${pageContext.request.contextPath}/img/i-billetera.png');

        $("#buscar_empresa").val('');
        //Configuracion de parametros
        var detener; //variable de seguimiento para la llamada del conteo
        var intervalo_espera = 2000; //Asignamos 3 segundos de espera
        var $input = $('#buscar_empresa'); //identificador
        
        var typingTimer;

//Al deja de escrbir, Empezamos el conteo para correr la funcion
        $input.on('keyup', function () {
            clearTimeout(typingTimer);
            typingTimer = setTimeout(funcion_buscar, intervalo_espera);
        });
//Al escribir nuevamente, Reiniciamos el conteo
        $input.on('keydown', function () {
            clearTimeout(typingTimer);
        });

        $("body").on("change", "#codigo_empresa", function () {

            var codigo_general = $("#codigo_empresa").val().split('%%%');

            $("#codigo_nombre").text(codigo_general[1]);

        });

    });
    //Al terminar de escribir y esperar "intervalo_espera" segundos llamamos a la funcion ajax
    function funcion_buscar() {
        
        var rz = $("#buscar_empresa").val();
        
        if (rz.length > 2) {
            
            $.ajax({
                url: '${pageContext.request.contextPath}/PagoServicioServlet?accion=buscarEmpresa',
                type: 'post',
                dataType: "text",
                beforeSend: function () {
                    // setting a timeout
                    $("#loading").show();
                },
                data: {
                    empresa: $("#buscar_empresa").val()
                },
                success: function (salida) {//ejecuta si el ajax fue exitoso
                    $("#codigo_empresa").html(salida);

                    if (salida !== '') {

                        var codigo_general = $("#codigo_empresa").val().split('%%%');

                        $("#codigo_nombre").text(codigo_general[1]);

                    } else {
                        $("#codigo_nombre").text("Ingrese C�digo de Cliente");
                    }


                    $("#loading").hide();
                },
                error: function () {
                    window.location.href = "${pageContext.request.contextPath}/";
                }
            });
        } 
        else {
            message_req("Mensaje", "Se requiere como minimo 3 caracteres");
        }
        
    }

    function validarCampos() {
        var array = [];
        var result = "<ul>";
        var codigo_empresa = $("#codigo_empresa").val();
        var codigo_cliente = $("#codigo_cliente").val();
        var flag = true;
        if (codigo_empresa === null || codigo_empresa === '') {
            result += "<li>Seleccione empresa</li>";
            flag = false;
        }
        if (codigo_cliente.length === 0) {
            result += "<li>Ingrese codigo cliente</li>";
            flag = false;
        }

        array = [flag, result];
        return array;
    }


    function js_validar_cliente_servicio() {

        var array = validarCampos();
        if (array[0] === true) {
            funcion_validar_cliente_servicio();
        } else {

            message_req("Mensaje", array[1]);
        }
    }

    function funcion_validar_cliente_servicio() {

        if ($("#codigo_empresa").val() !== '' && $("#codigo_empresa").val() !== null) {

            var codigo_general = $("#codigo_empresa").val().split('%%%');
            var codigo_empresa = codigo_general[0];

        } else {

            var codigo_empresa = $("#codigo_empresa").val();
        }

        var codigo_cliente = $("#codigo_cliente").val();

        $.ajax({
            url: '${pageContext.request.contextPath}/PagoServicioServlet',
            type: 'post',
            dataType: "text",
            beforeSend: function () {

            },
            data: {
                accion: 'validarClienteServicio',
                codigo_empresa: codigo_empresa,
                codigo_cliente: codigo_cliente
            },
            success: function (salida) {


                if (salida.length !== 0) {

                    var array = salida.split("%%%");
                    if (array[0] === "0") {
                        crearForm(array[1]);
                    } else {
                        message_req("Mensaje", array[1]);
                    }

                } else {
                    message_req("Mensaje", "Error Inesperado");
                }
            },
            error: function () {
                window.location.href = "${pageContext.request.contextPath}/";
            }
        });


    }


</script>