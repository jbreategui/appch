<script>

    $(function () {

        var mensaje = $("#hdd_mensaje_login").val();
        if (mensaje.length > 0) {
            message_req("Mensaje", mensaje);
        }


        $("#footer_miscuentas").attr('src', '${pageContext.request.contextPath}/img/i-caja-fuerte.png');

        $(".monto_formato").number(true, 2);
    });



    function  js_detalle_cuenta(indicador, descripcion) {

        $.ajax({
            url: '${pageContext.request.contextPath}/DetalleCuentaServlet',
            type: 'post',
            dataType: "text",
            beforeSend: function () {

            },
            data: {

                'accion': "listar_detalle_cuenta",
                'indicador': indicador,
                'descripcion': descripcion
            },
            success: function (salida) {

                if (salida.length !== 0) {

                    var array = salida.split("%%%");
                    if (array[0] === "0") {
                        crearForm(array[1]);
                    } else {
                        message_req("Mensaje", array[1]);
                    }

                } else {
                    message_req("Mensaje", "Error Inesperado");
                }
            },
            error: function () {
                window.location.href = "${pageContext.request.contextPath}/";
            }
        });

    }




    function js_detalle_cuenta_credito(indicador_credito, descripcion_credito) {


        $.ajax({
            url: '${pageContext.request.contextPath}/DetalleCuentaServlet',
            type: 'post',
            dataType: "text",
            beforeSend: function () {

            },
            data: {

                'accion': "listar_detalle_cuenta_credito",
                'indicador_credito': indicador_credito,
                'descripcion_credito': descripcion_credito
            },
            success: function (salida) {

                if (salida.length !== 0) {

                    var array = salida.split("%%%");
                    if (array[0] === "0") {
                        crearForm(array[1]);
                     } else {
                        message_req("Mensaje", array[1]);
                    }

                } else {
                    message_req("Mensaje", "Error Inesperado");
                }
            },
            error: function () {
                window.location.href = "${pageContext.request.contextPath}/";
            }
        });

    }




</script>