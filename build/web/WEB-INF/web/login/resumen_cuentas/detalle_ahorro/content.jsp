<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<div class="row" style="margin: 0px;padding: 10px;margin-top: 100px">

    <div class="col-xs-12 col-md-12 nopad"  style="margin-top: 0px">

      
            <div class="col-xs-12 box box-caja text-center" style="padding: 8px; margin-bottom: 0px">

            <p style="font-size: 18px;text-align: center;margin-bottom: 0px;font-weight: 600"> 
                
                <img class="pull-left" src="../../../img/i-alcancia.png" style="width: 30px" alt=""> 
        
                CUENTA DE ${descripcion}
                
                <img id="compartir_cuenta_ahorros" class="pull-right" src="../../../img/i-compartir.png" style="width: 20px;vertical-align: middle" alt="">
            
            </p>

            <p>

                <span class="text-center" style="font-size: 12px">Cuenta: <span>${array_lista_detalle_ahorro_cta_session[0]}</span></span> 
                <br>
                <span class="text-center" style="font-size: 12px">CCI: <span>${array_lista_detalle_ahorro_cta_session[1]}</span></span>

            </p>

            <p style="font-size: 18px;font-weight: 600">Saldo disponible: <span>${array_lista_detalle_ahorro_cta_session[2]}: <span class="formato_monto">${array_lista_detalle_ahorro_cta_session[3]}</span></span></p>
            
            <c:if test="${descripcion == 'CTS'}">
            
            <p style="font-size: 18px;font-weight: 600">Saldo Total: <span> ${array_lista_detalle_ahorro_cta_session[2]}: <span class="formato_monto"> ${array_lista_detalle_ahorro_cta_session[5   ]} </span></span></p>

            </c:if>
            
        </div>
      

    </div>


    <div class="col-xs-12 col-md-12 nopad box box-caja"  style="margin-top: 10px">
        <input type="hidden" id="dimension" value=${dimension}>

        
         <c:forEach var="listarec" items="${array_lista_detalle_ahorro_cta_session[4]}">
        <div class="col-xs-12 item-list-caja" name="lista_color">   


            <div class="col-xs-4 " style="text-align: center">
                <span>${listarec[0]}</span>
            </div>
            <div class="col-xs-5 " style="text-align: center">
                <span>${listarec[1]}</span>
            </div>
            <div class="col-xs-3 " style="text-align: center">
                <input type="hidden" name="num_color" value=${listarec[2]}>
                <span>${listarec[2]}</span>
            </div>

        </div>
         </c:forEach>
    </div>
    <div class="clearfix"></div>


</div>