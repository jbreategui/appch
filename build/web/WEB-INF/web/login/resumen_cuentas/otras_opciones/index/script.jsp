<!-- jQuery -->
<script src="${pageContext.request.contextPath}/jq/jquery-3.2.1.min.js" type="text/javascript"></script>
<!-- Bootstrap -->
<script src="${pageContext.request.contextPath}/jq/bootstrap/bootstrap.min.js" type="text/javascript"></script>

<script>

    $(function () {

        $("#footer_mas").attr('src', '${pageContext.request.contextPath}/img/i-puntos.png');


    });

    function ir_configuraciones() {

        $.ajax({
            url: '${pageContext.request.contextPath}/MiPerfilServlet',
            type: 'post',
            dataType: "text",
            beforeSend: function () {

            },
            data: {
                'accion': "configuraciones"

            },
            success: function (salida) {

                if (salida.length !== 0) {

                    var array = salida.split("%%%");
                    if (array[0] === "0") {
                        crearForm(array[1]);
                    } else {
                        message_req("Mensaje", array[1]);
                    }

                } else {
                    message_req("Mensaje", "Error Inesperado");
                }
            }
            ,
            error: function () {
                window.location.href = "${pageContext.request.contextPath}/";
            }
        });

    }

    function ir_perfil() {



        $.ajax({
            url: '${pageContext.request.contextPath}/MiPerfilServlet',
            type: 'post',
            dataType: "text",
            beforeSend: function () {

            },
            data: {
                accion: 'obtener_email'
            },
            success: function (salida) {


                if (salida.length !== 0) {

                    var array = salida.split("%%%");
                    if (array[0] === "0") {
                        crearForm(array[1]);
                    } else {
                        message_req("Mensaje", array[1]);
                    }

                } else {
                    message_req("Mensaje", "Error Inesperado");
                }
            },
            error: function () {
                window.location.href = "${pageContext.request.contextPath}/";
            }
        });

    }

    function ir_servicio_al_cliente() {

        $.ajax({
            url: '${pageContext.request.contextPath}/MiPerfilServlet',
            type: 'post',
            dataType: "text",
            beforeSend: function () {

            },
            data: {
                'accion': "servicioliente"

            },
            success: function (salida) {

                if (salida.length !== 0) {

                    var array = salida.split("%%%");
                    if (array[0] === "0") {
                        crearForm(array[1]);
                    } else {
                        message_req("Mensaje", array[1]);
                    }

                } else {
                    message_req("Mensaje", "Error Inesperado");
                }
            }
            ,
            error: function () {
                window.location.href = "${pageContext.request.contextPath}/";
            }
        });

    }

    function ir_informacion() {

        $.ajax({
            url: '${pageContext.request.contextPath}/MiPerfilServlet',
            type: 'post',
            dataType: "text",
            beforeSend: function () {

            },
            data: {
                'accion': "informacion"

            },
            success: function (salida) {

                if (salida.length !== 0) {

                    var array = salida.split("%%%");
                    if (array[0] === "0") {
                        crearForm(array[1]);
                    } else {
                        message_req("Mensaje", array[1]);
                    }

                } else {
                    message_req("Mensaje", "Error Inesperado");
                }
            }
            ,
            error: function () {
                window.location.href = "${pageContext.request.contextPath}/";
            }
        });

    }


</script>