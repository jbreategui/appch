<div class="row cuerpo">

    <%session.setAttribute("origen", "mas");%>

    <div class="col-xs-12 col-md-12">

        <div class="form-group text-center">

            Registrar Huella Digital

        </div>

    </div>



    <div class="col-xs-offset-1 col-xs-10 col-md-offset-1 col-md-10 text-center" style="border-radius:10px;border:solid 1px #C0C7C8;padding:5px;margin-bottom:15px">

        <span>Seleccione en iniciar y coloque su huella digital en el lector de huella.</span>
        <p></p>
        <p><img class="img-responsive" src="${pageContext.request.contextPath}/img/fingerprint.png"></p>




    </div>


    <div class="clearfix"></div>

    <div class="col-xs-6 col-md-6 ">

        <a onclick="ir_configuraciones();" class="btn btn-caja btn-block">Cancelar</a>

    </div>
    <div class="col-xs-6 col-md-6">


        <button id="empezar_huella" class="btn btn-caja btn-block cambiar_pagina" >Iniciar</button>

        <!-- Modal -->
        <div class="modal fade" id="myModal" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content modal-dialog-center">

                    <div class="modal-body">

                        <p  align="center"><img class="img-responsive" src="${pageContext.request.contextPath}/img/fingerprint-timer.png"></p>
                        <p align="center">Touch ID for "Caja Huancayo".</p>
                        <p align="center">Confirma tu Huella Digital</p>


                    </div>

                    <div class="modal-footer">
                        
                        <button type="button" class="btn btn-caja btn-block" data-dismiss="modal" >Cancelar</button>
                    </div>
                </div>

            </div>
        </div>

    </div>





</div>


<div class="modal fade" id="terminosycondiciones" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content modal-dialog-center">

            <div class="modal-body">

                <h3 align="center">T�rminos y Condiciones</h3>
                <p align="center">
                    ${terminos[0]}
                </p>


            </div>

            <div class="modal-footer">
                
                
                <div class="col-md-12 text-center">
                    <button class="btn btn-caja" id="llamar_huella" >Acepto</button>
                    <button class="btn btn-caja" data-dismiss="modal">Cancelar</button>
                </div>
                
                <div class="clearfix"></div>
                
                
            </div>
        </div>

    </div>
</div>