<!DOCTYPE html>
<html>
    <head>

        <jsp:include page="/web/glb/head_general.jsp" />

        <jsp:include page="perfil/head.jsp" />

    </head>
    <body>

        <jsp:include page="/web/glb/header_login.jsp" />

        <jsp:include page="perfil/content.jsp" />

        <jsp:include page="/web/glb/footer_login.jsp" />
        <jsp:include page="/web/glb/script_general.jsp" />

        <jsp:include page="perfil/script.jsp" />


    </body
</html>