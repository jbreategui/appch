<!-- jQuery -->
<script src="${pageContext.request.contextPath}/jq/jquery-3.2.1.min.js" type="text/javascript"></script>
<!-- Bootstrap -->
<script src="${pageContext.request.contextPath}/jq/bootstrap/bootstrap.min.js" type="text/javascript"></script>

<script>

    $(function () {

        //pintamos footer
        $("img#footer_mas").attr('src', '${pageContext.request.contextPath}/img/i-puntos.png');

        var chek = "${configura_oper[0]}";

        if (chek === "true") {

            $("#myonoffswitch").prop("checked", true);
        } else {
            $("#myonoffswitch").prop("checked", false);
        }

    });
    function validar_campos() {
        var array = [];
        var result = "<ul>";

        var coordenada = $("#coordenada_clave_dinamica").val();
        var flag = true;
        var flag_frecuente = "${r_estado_frecuente}";
        var flag_servlet = "${FLAG_OP_SERVLET}";
        if (flag_servlet === "false" || flag_frecuente === "false") {
            if (coordenada.length === 0) {

                result += "<li>${configura_oper_men}</li>";
                flag = false;
            } else {
                if ("${flag_clave}" === "A") {
                    if (coordenada.length !== 3) {
                        flag = false;
                        result += "<li>Clave de coordenada debe ser de tres d�gitos</li>";
                    }
                }

            }
        }

        result += "</ul>";
        array = [flag, result];

        return array;
    }
    function registrar_frec() {

        var array = validar_campos();
        if (array[0] === true) {

            funcion_ajax_registrar();

        } else {

            message_req("Mensaje", array[1]);
        }
    }


    function funcion_ajax_registrar() {

        var coordenada = $("#coordenada_clave_dinamica").val();
        var check = $("#myonoffswitch").is(":checked") ? 'true' : 'false';
        $.ajax({
            url: '${pageContext.request.contextPath}/ConfiguraOperFrecuentesServlet',
            type: 'post',
            dataType: "text",
            beforeSend: function () {
                $("#btn-confirmar").attr("disabled", true).addClass("btn-caja-activo").removeClass("btn-caja");
            },
            data: {
                accion: 'registrar',
                coordenada: coordenada,
                check: check,
                ip: ip_movil()

            },
            success: function (salida) {

                $("#btn-confirmar").attr("disabled", false).addClass("btn-caja").removeClass("btn-caja-activo");

                if (salida.length !== 0) {

                    var array = salida.split("%%%");
                    if (array[0] === "0") {
                        crearForm(array[1]);
                    } else {
                        message_req("Mensaje", array[1]);
                    }

                } else {
                    message_req("Mensaje", "Error Inesperado");
                }
            },
            error: function () {
                window.location.href = "${pageContext.request.contextPath}/";
            }
        });


    }
</script>