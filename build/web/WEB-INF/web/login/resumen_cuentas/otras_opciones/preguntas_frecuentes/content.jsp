<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<div class="row cuerpo">

    <div class="col-xs-12 col-md-12 text-center">
        <div class="form-group">
            <span class="titulo-negrita">Preguntas frecuentes</span>
        </div>

    </div>

    <div class="col-xs-12 col-md-12 nopad box box-caja"  style="margin-top: 10px">

        <c:forEach var="lista" items="${lista_prguntas}" >
            <a href="${pageContext.request.contextPath}/PreguntasFrecuentesServlet?accion=obtener_respuesta&codigoPregunta=${lista[0]}">
                <div class="col-xs-12 pregunta_frecuente" style="padding: 8px; margin-bottom: 0px;border-bottom:1px solid #808080">

                    <span class="negrita">${lista[1]} </span><span class="pull-right"><img style="max-height:15px" src="${pageContext.request.contextPath}/img/i-right.png" alt=""></span>

                </div>
            </a>    
        </c:forEach>

    </div>




</div>