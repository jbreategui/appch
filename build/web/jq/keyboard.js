$("body").click(function (e) {

    if ( $(e.target).hasClass("keyboard") || $(e.target).hasClass("keyboard2") || $(e.target).hasClass("keyboard3") || $(e.target).hasClass("keyboard4") || $(e.target).hasClass("fuzy-numKey-active") || $(e.target).parents(".fuzy-numKey-active").length || $(e.target).parent("#fuzzy-table-custom").length) {
        
    }
    
    else{

        remove();
        
    }

});

function remove() {
    $(".fuzy-numKey").hide(100, function () {
        $(".fuzy-numKey").remove();
    });
    //$("#keyboard").removeAttr('readonly');
}

(function (event) {

    $.fn.numKey = function (options) {

        function teclado() {

            var arr = [];

            while (arr.length < 10) {

                var randomnumber = Math.floor(Math.random() * 10);
                if (arr.indexOf(randomnumber) > -1)
                    continue;
                arr[arr.length] = randomnumber;

            }

            return arr;

        }

        var defaults = {
            limit: 100,
            disorder: false
        };

        var options = event.extend({}, defaults, options);
        var input = $(this);
        var nums = teclado();

        if (options.disorder) {

            nums.sort(function () {
                return 0.5 - Math.random();
            });

        }

        var html = '\
		<div class="fuzy-numKey">\
		<!--<div class="fuzy-numKey-active"> ﹀ </div>-->\
		<table id="fuzzy-table-custom">\
		<tr>\
		<td class="fuzy-numKey-lightgray fuzy-numKey-active text-center">' + nums[7] + '</td>\
		<td class="fuzy-numKey-lightgray fuzy-numKey-active text-center">' + nums[8] + '</td>\
		<td class="fuzy-numKey-lightgray fuzy-numKey-active text-center">' + nums[9] + '</td>\
		</tr>\
		<tr>\
		<td class="fuzy-numKey-lightgray fuzy-numKey-active text-center">' + nums[4] + '</td>\
		<td class="fuzy-numKey-lightgray fuzy-numKey-active text-center">' + nums[5] + '</td>\
		<td class="fuzy-numKey-lightgray fuzy-numKey-active text-center">' + nums[6] + '</td>\
		</tr>\
		<tr>\
		<td class="fuzy-numKey-lightgray fuzy-numKey-active text-center">' + nums[1] + '</td>\
		<td class="fuzy-numKey-lightgray fuzy-numKey-active text-center">' + nums[2] + '</td>\
		<td class="fuzy-numKey-lightgray fuzy-numKey-active text-center">' + nums[3] + '</td>\
		</tr>\
		<tr>\
		<td class="fuzy-numKey-darkgray fuzy-numKey-active text-center" id="backspace" style="background:#CC3333"><i style="color:white" class="fas fa-caret-square-down"><span hidden>backspace</span></i></td>\
		<td class="fuzy-numKey-lightgray fuzy-numKey-active text-center">' + nums[0] + '</td>\
                <td class="fuzy-numKey-darkgray fuzy-numKey-active text-center" id="limpiar" style="background:#CC3333"><i style="color:white" class="fa fa-trash-alt"><span hidden>limpiar</span></i></td>\
		</tr>\
		</table>\
		</div>';

        input.on("click", function () {

            $(this).attr('readonly', 'readonly');
            $(".fuzy-numKey").remove();
            $('body').append(html);
            $(".fuzy-numKey").show(100);

            $(".fuzy-numKey table tr td").on("click", function () {

                if (isNaN($(this).text())) {

                    if ($(this).text() === 'limpiar') {

                        input.val('');

                    } else {

                        //input.val(input.val().substring(0, input.val().length - 1)).trigger("change");
                        document.activeElement.blur();
                        remove();

                    }

                } else {

                    input.val(input.val() + $(this).text());

                    if (input.val().length >= options.limit) {

                        input.val(input.val().substring(0, options.limit)).trigger("change");
                        
                        
                        remove();

                    }

                }


            });

            $(".fuzy-numKey div").on("click", function () {

                remove();

            });

        });

        function remove() {
            $(".fuzy-numKey").hide(100, function () {
                $(".fuzy-numKey").remove();
            });
            //input.removeAttr('readonly');
        }
    };
})(jQuery);