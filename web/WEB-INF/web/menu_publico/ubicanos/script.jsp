<script 
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCMRAaJnSr_2uV37IGo-A_kMuZF4wTLNng">
</script>

<script>
    
    alto_pantalla();

    function alto_pantalla(){


        var alto = $(window).height() * 0.65;

        $("#map").height(alto);

    }

    var arrayTodos =${array_oficina_mapa_todos}; 
    var arrayOficinas =${array_oficina_mapa_oficinas};
    var arrayAgentes =${array_oficina_mapa_agentes};
    

    var map;
    var markers = [];

    var currentInfoWindow;
    
    //Para Debug
    var posicion_actual = "-12.067497,-75.211184";
    var centro = "";
    

    if (check_so() === "iOS") {
        
        var gps = obtenerGPS();
        
        function get_GPS(gps) {

            posicion_actual = "-12.067497,-75.211184";
        
        pos = posicion_actual.split(",");

        if(pos.length > 1){
            
            la = pos[0];
            lo = pos[1];
            
        }
        
        else{
            
            la = "-12.067497";
            lo = "-75.211184";
            
        }
        
        //Posicion Actual del dispositivo
         centro = {lat: parseFloat(la), lng: parseFloat(lo)};
        
        initMap();
        }
        
        
        
    } else if (check_so() === "Android") {
        
        var gps = Android.check_permiso_posicion();
                
        if(gps === "no_gps"){
            
            Android.showToast("Active su GPS, porfavor");
            
        }
        
        else if(gps !== "no_posicion"){
                
                posicion_actual = gps;
                
                
                if( posicion_actual === null || posicion_actual === '' ){
                    Android.showToast("No se pudo obtener su ubicación");
                    posicion_actual = "-12.067497,-75.211184";
                }
                
        }
        else{
            
            Android.posicion();
        
            //Android.showToast("Active su GPS, porfavor");
    
        }
        
        pos = posicion_actual.split(",");
        
        if(pos.length > 1){
            
            la = pos[0];
            lo = pos[1];
            
        }
        
        else{
            
            la = "-12.067497";
            lo = "-75.211184";
            
        }
        
        //Posicion Actual del dispositivo
        var centro = {lat: parseFloat(la), lng: parseFloat(lo)};
        
        initMap();
        
    } 
    
    else{
        
        pos = posicion_actual.split(",");
        
        if(pos.length > 1){
            
            la = pos[0];
            lo = pos[1];
            
        }
        
        else{
            
            la = "-12.067497";
            lo = "-75.211184";
            
        }
       
        var centro = {lat: parseFloat(la), lng: parseFloat(lo)};
        
        initMap();
        
        
    }
    
    function initMap() {

        var myStyles = [
            {
                featureType: "poi",
                elementType: "labels",
                stylers: [
                    {visibility: "off"}
                ]
            }
        ];

        map = new google.maps.Map(document.getElementById('map'), {
            zoom: 17,
            center: centro,
            disableDefaultUI: true,
            styles: myStyles
        });
    }

    // Sets the map on all markers in the array.
    function set_map_on_all(map) {
        for (var i = 0; i < markers.length; i++) {
            markers[i].setMap(map);
        }
    }

    // Removes the markers from the map, but keeps them in the array.
    function clear_markers() {
        set_map_on_all(null);
    }

    // Shows any markers currently in the array.
    function show_markers() {
        set_map_on_all(map);
    }

    // Deletes all markers in the array by removing references to them.
    function delete_markers() {
        clear_markers();
        markers = [];
    }

    function mostrar_marcadores($tipo) {

        switch ($tipo) {

            case 'TODOS':
                $marcadores = arrayTodos;
                break;

            case 'AGENTES':
                $marcadores = arrayAgentes;
                break;

            case 'OFICINAS':
                $marcadores = arrayOficinas;
                break;

        }
        
        var icon = {
            url: "${pageContext.request.contextPath}/img/ubicacion.png", // url
            scaledSize: new google.maps.Size(30, 50)
        };

        for (var i = 0; i < $marcadores.length; i++) {

            if ($marcadores[i].latitudOficina !== '' && $marcadores[i].longitudOficina !== '') {

                var contentString = '<div id="content">' +
                        '<div id="siteNotice">' +
                        '</div>' +
                        '<h4 id="firstHeading" class="firstHeading">' + $marcadores[i].nombreOficina + '</h4>' +
                        '<div id="bodyContent">' + $marcadores[i].referenciaOficina +
                        '</div>' +
                        '</div>';

                var marker = new google.maps.Marker({
                    position: new google.maps.LatLng(parseFloat($marcadores[i].longitudOficina), parseFloat($marcadores[i].latitudOficina)),
                    map: map,
                    icon: icon,
                    title: $marcadores[i].tipoOficina
                });

                google.maps.event.addListener(marker, 'click', (function (marker, i) {

                    return function () {
                        infowindow.setContent(contentString);
                        infowindow.open(map, marker);
                    };

                })(marker, i));

                markers.push(marker);

            }


        }

        $wewe = [];
        
        

        $.each($wewe, function (a, b) {

            if (b.latitudOficina !== '' && b.longitudOficina !== '') {

                var posicion = new google.maps.LatLng(parseFloat(b.longitudOficina), parseFloat(b.latitudOficina));

                var contentString = '<div id="content">' +
                        '<div id="siteNotice">' +
                        '</div>' +
                        '<h4 id="firstHeading" class="firstHeading">' + b.nombreOficina + '</h4>' +
                        '<div id="bodyContent">' + b.referenciaOficina +
                        '</div>' +
                        '</div>';

                var infowindow = new google.maps.InfoWindow({
                    content: contentString
                });

                var marker = new google.maps.Marker({
                    position: posicion,
                    map: map,
                    title: b.tipoOficina
                });

                google.maps.event.addListener(marker, 'click', function () {

                    map.setZoom(17);
                    map.setCenter(marker.getPosition());

                    if (currentInfoWindow !== null) {
                        currentInfoWindow.close();
                    }
                    infowindow.open(map, marker);
                    
                    currentInfoWindow = infowindow;

                });

                markers.push(marker);

            }

        });

    }

    $(function () {

        $("body").on("click", ".elegir-opcion", function () {

            switch (this.id) {

                case "oficinas":

                    $("#oficinas").attr("src", "${pageContext.request.contextPath}/img/i-justicia.png");
                    $("#agentes").attr("src", "${pageContext.request.contextPath}/img/i-tienda-gray.png");
                    $("#todos").attr("src", "${pageContext.request.contextPath}/img/i-cajero-gray.png");

                    deleteMarkers();
                    mostrar_marcadores("OFICINAS");
                    break;

                case "agentes":

                    $("#oficinas").attr("src", "${pageContext.request.contextPath}/img/i-justicia-gray.png");
                    $("#agentes").attr("src", "${pageContext.request.contextPath}/img/i-tienda.png");
                    $("#todos").attr("src", "${pageContext.request.contextPath}/img/i-cajero-gray.png");

                    $(this).addClass("tienda-elegida");
                    deleteMarkers();
                    mostrar_marcadores("AGENTES");
                    break;

                case "todos":

                    $("#oficinas").attr("src", "${pageContext.request.contextPath}/img/i-justicia-gray.png");
                    $("#agentes").attr("src", "${pageContext.request.contextPath}/img/i-tienda-gray.png");
                    $("#todos").attr("src", "${pageContext.request.contextPath}/img/i-cajero.png");

                    $(this).addClass("tienda-elegida");
                    deleteMarkers();
                    mostrar_marcadores("TODOS");
                    break;

            }

        });

    });

</script>