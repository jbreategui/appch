 <html style="height:100%">
    <head>

        <jsp:include page="/web/glb/head_general.jsp" />

        <jsp:include page="ubicanos/head.jsp" />

    </head>
    <body  style="height:100%">

        <jsp:include page="ubicanos/content.jsp" />

        <jsp:include page="/web/glb/script_general.jsp" />

        <jsp:include page="ubicanos/script.jsp" />

    </body>
</html>
