<div class="row row-footer">

    <div class="footer">

        <div class="col-xs-12 col-md-12 nopad  text-center footer_login">


            <a class="cambiar_pagina col-xs-3" onclick="ingresar_cuentas();">

                <div>

                    <img id="footer_miscuentas" class=" iconos-footer" src="${pageContext.request.contextPath}/img/i-caja-fuerte-gray.png" alt="">

                </div>


            </a>


            <a class="cambiar_pagina col-xs-3" onclick="ingresar_transferencias();">

                <div style="word-break: break-all">



                    <img id="footer_transferencias" class=" iconos-footer" src="${pageContext.request.contextPath}/img/i-celular-gray.png" alt="">



                </div>

            </a>

            <a class="cambiar_pagina col-xs-3" onclick="ingresar_pagos();">

                <div>

                    <img id="footer_pagos" class=" iconos-footer" src="${pageContext.request.contextPath}/img/i-billetera-gray.png" alt="">

                </div>

            </a>

            <a class="cambiar_pagina col-xs-3" onclick="ingresar_otras_opciones();">

                <div>
                    <img id="footer_mas" class=" iconos-footer" src="${pageContext.request.contextPath}/img/i-puntos-gray.png" alt="" style="padding-top: 10px;">

                </div>

            </a>




            <div class="clearfix"></div>
            <div class="col-xs-3">
                Mis Cuentas
            </div>
            <div class="col-xs-3" style="word-break: break-all;padding:0px">
                Transferencias
            </div>
            <div class="col-xs-3">
                Pagos
            </div>
            <div class="col-xs-3">
                Mas
            </div>
        </div>

    </div>

</div>


<script type="text/javascript">
    function ingresar_cuentas() {

        $.ajax({
            url: '${pageContext.request.contextPath}/ResumenCuentaServlet',
            type: 'post',
            dataType: "text",
            beforeSend: function () {

            },
            data: {
                'accion': "listar_cuentas"

            },
            success: function (salida) {

                if (salida.length !== 0) {

                    var array = salida.split("%%%");
                    if (array[0] === "0") {
                        crearForm(array[1]);
                    } else {
                        message_req("Mensaje", array[1]);
                    }

                } else {
                    message_req("Mensaje", "Error Inesperado");
                }
            },
            error: function () {
                window.location.href = "${pageContext.request.contextPath}/";
            }
        });

    }

    function ingresar_transferencias() {

        $.ajax({
            url: '${pageContext.request.contextPath}/CuentaPropiaServlet',
            type: 'post',
            dataType: "text",
            beforeSend: function () {

            },
            data: {
                'accion': "transferencia"

            },
            success: function (salida) {

                if (salida.length !== 0) {

                    var array = salida.split("%%%");
                    if (array[0] === "0") {

                        crearForm(array[1]);
                    } else {
                        message_req("Mensaje", array[1]);
                    }

                } else {
                    message_req("Mensaje", "Error Inesperado");
                }
            },
            error: function () {
                window.location.href = "${pageContext.request.contextPath}/";
            }
        });

    }


    function ingresar_pagos() {

        $.ajax({
            url: '${pageContext.request.contextPath}/PagoCreditoPropioServlet',
            type: 'post',
            dataType: "text",
            beforeSend: function () {

            },
            data: {
                'accion': "pagos"

            },
            success: function (salida) {

                if (salida.length !== 0) {

                    var array = salida.split("%%%");
                    if (array[0] === "0") {
                        crearForm(array[1]);
                    } else {
                        message_req("Mensaje", array[1]);
                    }

                } else {
                    message_req("Mensaje", "Error Inesperado");
                }
            },
            error: function () {
                window.location.href = "${pageContext.request.contextPath}/";
            }
        });

    }
    function ingresar_otras_opciones() {

        $.ajax({
            url: '${pageContext.request.contextPath}/ConfiguraTarjetaServlet',
            type: 'post',
            dataType: "text",
            beforeSend: function () {

            },
            data: {
                'accion': "otras_opciones"

            },
            success: function (salida) {

                if (salida.length !== 0) {

                    var array = salida.split("%%%");
                    if (array[0] === "0") {
                        crearForm(array[1]);
                    } else {
                        message_req("Mensaje", array[1]);
                    }

                } else {
                    message_req("Mensaje", "Error Inesperado");
                }
            },
            error: function () {
                window.location.href = "${pageContext.request.contextPath}/";
            }
        });

    }

    function ir_oper_frec() {


        $.ajax({
            url: '${pageContext.request.contextPath}/OperFrecuentesServlet',
            type: 'post',
            dataType: "text",
            beforeSend: function () {

            },
            data: {
                accion: "listar_oper_frec"
            },
            success: function (salida) {

                if (salida.length !== 0) {

                    var array = salida.split("%%%");
                    if (array[0] === "0") {
                        crearForm(array[1]);
                    } else {
                        message_req("Mensaje", array[1]);
                    }

                } else {
                    message_req("Mensaje", "Error Inesperado");
                }
            },
            error: function () {
                window.location.href = "${pageContext.request.contextPath}/";
            }
        });


    }
</script>