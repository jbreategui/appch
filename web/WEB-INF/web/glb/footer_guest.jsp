
<div class="row row-footer">

    <div class="footer">
        <div class="row" style="position: absolute;bottom: 0px;width: 100%;margin:0px;">

            <div class="col-xs-12 col-md-12  text-center back-color-2" style="padding: 10px 0px;
                 font-size: 12px;
                 background-color: #F2F2F2;">

                <div class="col-xs-4">
                    <a class="cambiar_pagina" onclick="bloquear_tarjeta()"/>
                    <img id="img-bloquear-tarjeta" class="footer-gris-img" src="${pageContext.request.contextPath}/img/i-tarjeta-bloquear-gray.png" style="width: 30px;line-height: 52px" alt=""><br>
                    <span class="text-color-3">Bloqueo de tarjeta</span>
                    </a>

                </div>
                <div class="col-xs-4">
                    <a class="cambiar_pagina" href="${pageContext.request.contextPath}/web/menu_publico/contactanos.jsp">
                        <img id="img-contactanos" class="footer-gris-img" src="${pageContext.request.contextPath}/img/i-auricular-gray.png" style="width: 30px;line-height: 52px" alt=""><br>
                        <span class="text-color-3">Contáctanos</span>
                    </a>

                </div>
                <div class="col-xs-4">
                    <a class="cambiar_pagina" href="${pageContext.request.contextPath}/UbicanosServlet?accion=listar_datos_mapa">
                        <img id="img-ubicanos" class="footer-gris-img" src="${pageContext.request.contextPath}/img/i-mapa-gray.png" style="width: 25px;line-height: 52px" alt=""><br>
                        <span class="text-color-3">Ubícanos</span>
                    </a>

                </div>

            </div>

        </div>
    </div>
</div>
<script type="text/javascript">


    function bloquear_tarjeta() {

        $.ajax({
            url: '${pageContext.request.contextPath}/BloqueoTarjetaServlet',
            type: 'post',
            dataType: "text",
            beforeSend: function () {

            },
            data: {
                'accion': "listar_combos"

            },
            success: function (salida) {

                if (salida.length !== 0) {

                    var array = salida.split("%%%");
                    if (array[0] === "0") {
                        crearForm(array[1]);
                    } else {
                        message_req("Mensaje", array[1]);
                    }

                } else {
                    message_req("Mensaje", "Error Inesperado");
                }
            },
            error: function () {
                window.location.href = "${pageContext.request.contextPath}/";
            }
        });

    }


</script>