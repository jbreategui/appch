
<!DOCTYPE html>
<html>
    <head>


        <jsp:include page="/web/glb/head_general.jsp" />

        <jsp:include page="constancia/head.jsp" />

    </head>
    <body onload="noback();">
        
        <%
            String id = (String) request.getSession().getAttribute("r_codigoCliente");
            String cabecera = "";
            if (id != null) {
                cabecera = "/web/glb/header_login.jsp";
            } else {
                cabecera = "";
            }
        %>

        <% if ( !cabecera.equals("") ) { %>
            <jsp:include page="/web/glb/header_login.jsp" />   
        <% }%>
        
        <%
            String referrer = (String) request.getSession().getAttribute("rutaServlet");
            String content = "";
            if (referrer.compareTo("BloqueoTarjetaServlet") == 0) {// poner de donde viene
                content = "constancia/content_bloqueo_tarjeta.jsp";
            } else if (referrer.compareTo("OlvidoClaveServlet") == 0) {// poner de donde vien
                content = "constancia/content_olvido_clave.jsp";
            } else if (referrer.compareTo("PagoServicioServlet") == 0) {// poner de donde vien
                content = "constancia/content_pago_servicios.jsp";
            } else if (referrer.compareTo("EnvioGirosServlet") == 0) {// poner de donde vien
                content = "constancia/content_envio_giros.jsp";
            } else if (referrer.compareTo("RegistroPagoCreditoServlet") == 0) {// poner de donde vien
                content = "constancia/content_pago_tarjeta_credito.jsp";
            } else if (referrer.compareTo("PagoCreditoPropioServlet") == 0) {// poner de donde vien
                content = "constancia/content_pago_creditos_propios.jsp";
            } else if (referrer.compareTo("PagoCreditoTerceroServlet") == 0) {// poner de donde vien
                content = "constancia/content_pago_creditos_terceros.jsp";
            } else if (referrer.compareTo("CuentaPropiaServlet-constancia_transferencia_cuenta_propia") == 0) {// poner de donde vien
                content = "constancia/content_cuentas_propias.jsp";
            } else if (referrer.compareTo("CuentaTerceroServlet-constancia_transferencia_cuenta_tercero") == 0) {// poner de donde vien
                content = "constancia/content_cuentas_terceros.jsp";
            } else if (referrer.compareTo("PagosRecargasServlet") == 0) {// poner de donde vien
                content = "constancia/content_recarga_celular.jsp";
            }
            
            else if (referrer.compareTo("CuentaOtroBancoServlet-constancia_transferencia_cuenta_otro_banco") == 0) {// poner de donde vien
                content = "constancia/content_cuentas_otros_bancos.jsp";
            }
            else if (referrer.compareTo("MiPerfilServlet") == 0) {// poner de donde vien
                content = "constancia/content_datos_personales.jsp";
            }
            else if (referrer.compareTo("PagoInstitucionesServlet") == 0) {// poner de donde vien
                content = "constancia/content_pago_instituciones.jsp";
            }
            else if (referrer.compareTo("ConfiguraTarjetaServlet") == 0) {// poner de donde vien
                content = "constancia/content_configura_tarjeta.jsp";
            }
            else if (referrer.compareTo("ConfiguraOperFrecuentesServlet") == 0) {// poner de donde vien
                content = "constancia/content_configura_oper.jsp";
            }
            else if (referrer.compareTo("HuellaDigital-registrar_huella") == 0) {// poner de donde vien
                content = "constancia/content_configura_huella_digital.jsp";
            }
            else {
                content = "constancia/content.jsp";
            }
        %>
        <jsp:include page="<%=content%>" />
        
        
        

        

        <jsp:include page="constancia/script.jsp" />

     
    </body
</html>