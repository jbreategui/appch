<div class="row cuerpo" >


    <div class="col-xs-12 col-md-12" style="margin-top: 0px">


        <p class="text-center" style="font-family: futura;font-size:30px;font-weight: 700">
            Constancia de pago de cr�dito a terceros

        </p>
    </div>

    <div class="col-xs-12 col-md-12 nopad"  style="margin-top: 0px">
        <div class="form-group" style="text-align: center">

            <span style="font-weight: 900">C�digo de operaci�n: </span>${contancia_terceros[0]}   <span style="font-weight: 900">Fecha: </span>${contancia_terceros[1]}   <span style="font-weight: 900">Hora: </span>${contancia_terceros[2]}
        </div>
        <div class="col-xs-12 box box-caja" style="padding: 20px">



            <div class="form-group">

                <p><span style="font-weight: 900">Cuenta cargo: </span>${contancia_terceros[3]}</p>

            </div>
            <div class="form-group">

                <p><span style="font-weight: 900">Cuenta cr�dito: </span>${contancia_terceros[4]}</p>
                <p><span style="font-weight: 900">Titular: </span>${contancia_terceros[5]}</p>

            </div>

            <div class="form-group">

                <p><span style="font-weight: 900">Cuota</span> ${contancia_terceros[6]}: ${contancia_terceros[7]} ${contancia_terceros[8]}</p>

            </div>

            <div class="form-group">

                <p><span style="font-weight: 900"> Moneda y Monto: </span> <span id="monedamonto"> ${contancia_terceros[7]} ${contancia_terceros[8]} </span> </p>

            </div>




            <% String siguiente_cuota = (String) request.getSession().getAttribute("siguiente_cuota");

                if (siguiente_cuota.equals("1")) { %>

            <div class="form-group">

                <p><span style="font-weight: 900">Pr�xima cuota: </span> ${contancia_terceros[9]} ${contancia_terceros[10]}   <span style="font-weight: 900"> Vence: </span> ${contancia_terceros[11]} </p>

            </div>

            <% }%>

        </div>

    </div>

    <div class="col-xs-12 nopad">

        <div class="col-xs-offset-3 col-xs-6 text-center">
            <button onclick="compartir();" class="btn btn-caja btn-block" style="padding:5px">Enviar Constancia</button>
        </div>

    </div>

</div>

<jsp:include page="/web/glb/footer_login.jsp" />

<jsp:include page="/web/glb/script_general.jsp" />

<script>

    $(function () {

        $("img#footer_pagos").attr('src', '${pageContext.request.contextPath}/img/i-billetera.png');

        $("#monedamonto").text(${coompartir_terceros}.monedamonto);

    });

    function compartir() {

        var ary = ${coompartir_terceros};

        var texto = "Constancia de Pago de Cr�dito Tercero" + "\n";
        texto += "-------------------------------------" + "\n";
        texto += "C�digo Operaci�n :" + " " + ary.cod + "\n";
        texto += "Fecha :" + " " + ary.fecha + "\n";
        texto += "Hora :" + " " + ary.hora + "\n";
        texto += "Cuenta cargo :" + " " + ary.cuenta + "\n";
        texto += "Cuenta cr�dito :" + " " + ary.cuentaCredito + "\n";
        texto += "Titular :" + " " + ary.titular + "\n";
        texto += "Cuota" + " " + ary.cuota + " : " + ary.monedamonto + "\n";
        texto += "Moneda y monto :" + " " + ary.monedamonto + "\n";
        texto += "Pr�xima cuota :" + " " + ary.proxima_cuota_moneda + "\n";
        texto += "Vence :" + " " + ary.vence;

        if (check_so() === "iOS") {
            set_shared(texto);
        } else if (check_so() === "Android") {
            Android.shared_with(texto);
        }

    }

</script>