<div class="row cuerpo" >


    <div class="col-xs-12 col-md-12" style="margin-top: 0px">


        <p class="text-center" style="font-family: futura;font-size:30px;font-weight: 700">
            Constancia 

        </p>
        <p class="text-center" style="font-family: futura;font-size:20px;font-weight: 700">
            Solicitud de Pago de Tarjeta de Cr�dito
        </p>
    </div>

    <div class="col-xs-12 col-md-12 nopad"  style="margin-top: 0px">
        <div class="form-group" style="text-align: center">

            <span style="font-weight: 900">C�digo de operaci�n: </span>${constancia_pago_tarjeta_credito_datos[0]}   <span style="font-weight: 900">Fecha: </span>${constancia_pago_tarjeta_credito_datos[1]}   <span style="font-weight: 900">Hora: </span>${constancia_pago_tarjeta_credito_datos[2]}
        </div>
        <div class="col-xs-12 box box-caja" style="padding: 20px">



            <div class="form-group">

                <p><span style="font-weight: 900">Cuenta Origen: </span>${constancia_pago_tarjeta_credito_datos[3]}</p>

            </div>
            <div class="form-group">

                <p><span style="font-weight: 900">Banco Destino </span>${constancia_pago_tarjeta_credito_datos[4]}</p>


            </div>
            <div class="form-group">

                <p><span style="font-weight: 900">Numero Tarjeta </span>${constancia_pago_tarjeta_credito_datos[5]}</p>


            </div>

            <div class="form-group">

                <p><span style="font-weight: 900"> Moneda y Monto: </span> <span id="monedamonto"> ${constancia_pago_tarjeta_credito_datos[6]} ${constancia_pago_tarjeta_credito_datos[7]} </span> </p>

            </div>
            <div class="form-group">

                <p><span style="font-weight: 900"> Comision Interbancaria: </span> ${constancia_pago_tarjeta_credito_datos[6]}  ${constancia_pago_tarjeta_credito_datos[8]}</p>

            </div>
                
            <div class="form-group">

                <p><span style="font-weight: 900"> Comision Cliente: </span> ${constancia_pago_tarjeta_credito_datos[6]}  ${constancia_pago_tarjeta_credito_datos[9]}</p>

            </div>
                
            <div class="form-group">

                <p><span style="font-weight: 900"> Comision de Confirmaci�n de Abono: </span> ${constancia_pago_tarjeta_credito_datos[6]}  ${constancia_pago_tarjeta_credito_datos[11]}</p>

            </div>
                
            <div class="form-group">

                <p><span style="font-weight: 900"> ITF:  </span> ${constancia_pago_tarjeta_credito_datos[6]}  ${constancia_pago_tarjeta_credito_datos[13]}</p>

            </div>
                
            
                
                
                
            <div class="form-group">

                <p><span style="font-weight: 900"> Total: </span>${constancia_pago_tarjeta_credito_datos[6]} ${constancia_pago_tarjeta_credito_datos[10]} </p>

            </div>
                
            <div class="form-group">

                <p><span style="font-weight: 900">Codigo de Solicitud: </span> ${constancia_pago_tarjeta_credito_datos[12]}</p>

            </div>
                
                
            <p style="font-weight: 900">CAJA HUANCAYO-... �Tu mejor opci�n financiera</p>
            <p style="font-weight: 900"></p>

        </div>

    </div>

    <div class="col-xs-12 nopad">

        <div class="col-xs-offset-3 col-xs-6 text-center">
            <button  onclick="compartir();"class="btn btn-caja btn-block" style="padding:5px">Enviar Constancia</button>
        </div>

    </div>

</div>

<jsp:include page="/web/glb/footer_login.jsp" />

<jsp:include page="/web/glb/script_general.jsp" />

<script>

    $(function () {

        $("img#footer_pagos").attr('src', '${pageContext.request.contextPath}/img/i-billetera.png');
        
        $("#monedamonto").text(${cons_compratir}.monedamonto);


        $("body").on("click", ".elegir-moneda", function () {

            switch (this.id) {

                case "soles":
                    $("#dolares").removeClass("btn-elegido");
                    $(this).addClass("btn-elegido");
                    break;

                case "dolares":
                    $("#soles").removeClass("btn-elegido");
                    $(this).addClass("btn-elegido");
                    break;

            }

        });

    });


    function compartir() {

        var ary = ${cons_compratir};

        var texto = "Constancia de Pago de Tarjeta de cr�dito" + "\n";
        texto += "----------------------------------------------" + "\n";
        texto += "C�digo Operaci�n :" + " " + ary.cod + "\n";
        texto += "Fecha :" + " " + ary.fecha + "\n";
        texto += "Hora :" + " " + ary.hora + "\n";
        texto += "Cuenta Origen :" + " " + ary.cuenta + "\n";
        texto += "Entidad :" + " " + ary.banco + "\n";
        texto += "N�mero Tarjeta :" + " " + ary.numero + "\n";
        texto += "Moneda y Monto :" + " " + ary.moneda + "\n";
        texto += "Comisi�n Interbancaria :" + " " + ary.comisioninterbancaria + "\n";
        texto += "Comisi�n Cliente :" + " " + ary.comisioncliente + "\n";
        texto += "Comisi�n de Confirmaci�n de Abono :" + " " + ary.comisionabono + "\n";
        texto += "ITF :" + " " + ary.itf + "\n";
        texto += "Total :" + " " + ary.total+ "\n";
        texto += "C�digo Solicitud :" + " " + ary.codigosolicitud;

        if (check_so() === "iOS") {
            set_shared(texto);
        } else if (check_so() === "Android") {
            Android.shared_with(texto);
        }

    }




</script>