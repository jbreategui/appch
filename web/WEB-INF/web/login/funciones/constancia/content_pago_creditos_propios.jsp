<div class="row cuerpo" >


    <div class="col-xs-12 col-md-12" style="margin-top: 0px">


        <p class="text-center" style="font-family: futura;font-size:30px;font-weight: 700">
            Constancia de Pago de cr�ditos propios

        </p>
    </div>

    <div class="col-xs-12 col-md-12 nopad"  style="margin-top: 0px">
        <div class="form-group" style="text-align: center">
            <span style="font-weight: 900">C�digo de operaci�n: </span>${constancia_pago_credito[0]}   <span style="font-weight: 900">Fecha: </span>${constancia_pago_credito[1]}   <span style="font-weight: 900">Hora: </span>${constancia_pago_credito[2]}
        </div>
        <div class="col-xs-12 box box-caja" style="padding: 20px">

            <div class="form-group">

                <p><span style="font-weight: 900">Cuenta Cargo: </span>${constancia_pago_credito[3]} </p>

            </div>

            <div class="form-group">

                <p><span style="font-weight: 900">Cuenta Cr�dito: </span> ${constancia_pago_credito[4]}  </p>

            </div>
            <div class="form-group">

                <p><span style="font-weight: 900">Cuota </span>  ${constancia_pago_credito[5]} : ${constancia_pago_credito[6]} ${constancia_pago_credito[7]} </p>

            </div>
            <div class="form-group">

                <p><span style="font-weight: 900">Moneda y Monto: </span> <span id="monedamonto">${constancia_pago_credito[8]} ${constancia_pago_credito[9]} </span> </p>

            </div>


            <% String siguiente_cuota = (String) request.getSession().getAttribute("siguiente_cuota");

                if (siguiente_cuota.equals("1")) { %>
                
            <div class="form-group">

                <p>
                    <span style="font-weight: 900">Pr�xima cuota: </span> ${constancia_pago_credito[10]} ${constancia_pago_credito[11]}   <span style="font-weight: 900">Vence: </span> ${constancia_pago_credito[12]}   
                </p>

            </div>
                
            <% }%>





        </div>

    </div>

    <div class="col-xs-12 nopad">

        <div class="col-xs-offset-3 col-xs-6 text-center">
            <button  onclick="compartir();" class="btn btn-caja btn-block" style="padding:5px">Enviar Constancia</button>
        </div>

    </div>

</div>

<jsp:include page="/web/glb/footer_login.jsp" />

<jsp:include page="/web/glb/script_general.jsp" />

<script>

    $(function () {

        $("img#footer_pagos").attr('src', '${pageContext.request.contextPath}/img/i-billetera.png');

        $("#monedamonto").text(${compartir_credio}.monedamonto);

    });

    function compartir() {

        var ary = ${compartir_credio};

        var texto = "Constancia de Pago de Cr�dito Propio" + "\n";
        texto += "----------------------------------------------" + "\n";
        texto += "C�digo Operaci�n :" + " " + ary.cod + "\n";
        texto += "Fecha :" + " " + ary.fecha + "\n";
        texto += "Hora :" + " " + ary.hora + "\n";
        texto += "Cuenta cargo :" + " " + ary.cuenta + "\n";
        texto += "Cuenta cr�dito :" + " " + ary.cuentaCredito + "\n";
        texto += "Cuota :" + " " + ary.cuota + "\n";
        texto += "Moneda y monto :" + " " + ary.monedamonto + "\n";
        texto += "Pr�xima cuota :" + " " + ary.prxima + "\n";
        texto += "Vence :" + " " + ary.vence;

        if (check_so() === "iOS") {
            set_shared(texto);
        } else if (check_so() === "Android") {
            Android.shared_with(texto);
        }

    }
</script>