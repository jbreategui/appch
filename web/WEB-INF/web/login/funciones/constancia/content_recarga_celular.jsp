<div class="row cuerpo" >


    <div class="col-xs-12 col-md-12" style="margin-top: 0px">


        <p class="text-center" style="font-family: futura;font-size:30px;font-weight: 700">
            Constancia Recarga de celular

        </p>
    </div>

    <div class="col-xs-12 col-md-12 nopad"  style="margin-top: 0px">
        <div class="form-group" style="text-align: center">

            <span style="font-weight: 900">C�digo de operaci�n: </span>${constancia_recarga[0]}   <span style="font-weight: 900">Fecha: </span>${constancia_recarga[1]}   <span style="font-weight: 900">Hora: </span>${constancia_recarga[2]}
        </div>
        <div class="col-xs-12 box box-caja" style="padding: 20px">



            <div class="form-group">

                <p><span style="font-weight: 900">Cuenta cargo: </span>${constancia_recarga[3]}</p>

            </div>
            <div class="form-group">

                <p><span style="font-weight: 900">Operador: </span>${constancia_recarga[4]}</p>


            </div>
            <div class="form-group">

                <p><span style="font-weight: 900">Tel�fono: </span>${constancia_recarga[5]}</p>


            </div>

            <div class="form-group">

                <p><span style="font-weight: 900"> Moneda y Monto: </span> <span id="monedamonto">${constancia_recarga[6]} ${constancia_recarga[7]} </span> </p>

            </div>


        </div>

    </div>

    <div class="col-xs-12 nopad">

        <div class="col-xs-offset-3 col-xs-6 text-center">
            <button onclick="compartir();" class="btn btn-caja btn-block" style="padding:5px">Enviar Constancia</button>
        </div>

    </div>

</div>

<jsp:include page="/web/glb/footer_login.jsp" />

<jsp:include page="/web/glb/script_general.jsp" />

<script>
    
    $(function(){
       
        $("img#footer_pagos").attr('src', '${pageContext.request.contextPath}/img/i-billetera.png');
        
        $("#monedamonto").text(${compartir_recarga}.monedamonto);
        
    });
    
    function compartir() {

        var ary = ${compartir_recarga};

        var texto = "Constancia de Recarga de Celular" + "\n";
        texto += "--------------------------------------" + "\n";
        texto += "C�digo Operaci�n :" + " " + ary.cod + "\n";
        texto += "Fecha :" + " " + ary.fecha + "\n";
        texto += "Hora :" + " " + ary.hora + "\n";
        texto += "Cuenta cargo :" + " " + ary.cuenta + "\n";
        texto += "Operador :" + " " + ary.operador + "\n";
        texto += "Tel�fono :" + " " + ary.telefono + "\n";
        texto += "Moneda y monto :" + " " + ary.moneda + " " + ary.monto + "\n";


        if (check_so() === "iOS") {
            set_shared(texto);
        } else if (check_so() === "Android") {
            Android.shared_with(texto);
        }
    }

</script>


