<!DOCTYPE html>
<html>
    <head>
        
        <jsp:include page="/web/glb/head_general.jsp" />

        <jsp:include page="olvido_clave/head.jsp" />

    </head>
    <body>
        
        <jsp:include page="olvido_clave/content.jsp" />
        
        <jsp:include page="/web/glb/script_general.jsp" />

        <jsp:include page="olvido_clave/script.jsp" />
        
        
    </body
</html>