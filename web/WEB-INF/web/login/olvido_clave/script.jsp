<script>

    $(function () {
        var mensaje = $("#hdd_mensaje_login").val();
        if (mensaje.length > 0) {
            message_req("Mensaje", mensaje);
        }

        $(".keyboard").numKey({
            limit: 16,
            disorder: true
        });

        $(".keyboard").click(function () {

            $(".keyboard").numKey({
                limit: 4,
                disorder: true
            });

        });

        $(".keyboard2").numKey({
            limit: 4,
            disorder: true
        });

        $(".keyboard2").click(function () {

            $(".keyboard2").numKey({
                limit: 4,
                disorder: true
            });

        });

        $(".keyboard3").numKey({
            limit: 6,
            disorder: true
        });

        $(".keyboard3").click(function () {

            $(".keyboard3").numKey({
                limit: 6,
                disorder: true
            });

        });

        $(".keyboard4").numKey({
            limit: 6,
            disorder: true
        });

        $(".keyboard4").click(function () {

            $(".keyboard4").numKey({
                limit: 6,
                disorder: true
            });

        });



    });




    function validarCampos() {
        var array = [];
        var result = "<ul>";
        var numero_tarjeta = $("#numero_tarjeta").val();
        var clave = $("#clave").val();
        var nueva_clave = $("#nueva_clave").val();
        var confirmar_clave = $("#confirmar_clave").val();


        var flag = true;
        if (numero_tarjeta.length === 0) {
            result += "<li>Ingrese n�mero de tarjeta</li>";
            flag = false;
        } else {
            if (numero_tarjeta.length < 16) {
                result += "<li>N�mero de tarjeta debe tener 16 d�gitos</li>";
                flag = false;
            }
        }
        if (clave.length === 0) {
            result += "<li>Ingrese clave Pin</li>";
            flag = false;
        }
        if (nueva_clave.length === 0) {
            result += "<li>Ingrese nueva  clave web</li>";
            flag = false;
        } else {
            if (nueva_clave !== confirmar_clave) {
                result += "<li>Las claves no coinciden</li>";
                flag = false;
            }
        }

        array = [flag, result];

        return array;
    }


    function js_validar_clave() {

        var array = validarCampos();

        if (array[0] === true) {
            js_validar_clave_reg();
        } else {

            message_req("Mensaje", array[1]);
        }
    }

    function js_validar_clave_reg() {

        var ippublicamovil = "";
        if (check_so() === "iOS") {
            ippublicamovil = '${IP_iOS}';
        } else if (check_so() === "Android") {

            ippublicamovil = Android.get_ip_cell_phone();

        }
        $.ajax({
            url: '${pageContext.request.contextPath}/OlvidoClaveServlet',
            type: 'post',
            dataType: "text",
            beforeSend: function () {
                $("#btn-confirmar").attr("disabled", true).addClass("btn-caja-activo").removeClass("btn-caja");
            },
            data: {
                'accion': "validarClavePin",
                'numerotarjeta': $("#numero_tarjeta").val(),
                'clave': $("#clave").val(),
                'ippublicamovil': ippublicamovil,
                'nuevaClave': $("#nueva_clave").val()
            },
            success: function (salida) {

                $("#btn-confirmar").attr("disabled", false).addClass("btn-caja").removeClass("btn-caja-activo");

                if (salida.length !== 0) {

                    var array = salida.split("%%%");
                    if (array[0] === "0") {
                        crearForm(array[1]);
                    } else {
                        message_req("Mensaje", array[1]);
                    }

                } else {
                    message_req("Mensaje", "Error Inesperado");
                }
            },
            error: function () {
                window.location.href = "${pageContext.request.contextPath}/";
            }
        });

    }
</script>