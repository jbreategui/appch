<!-- jQuery -->
<script src="${pageContext.request.contextPath}/jq/jquery-3.2.1.min.js" type="text/javascript"></script>
<!-- Bootstrap -->
<script src="${pageContext.request.contextPath}/jq/bootstrap/bootstrap.min.js" type="text/javascript"></script>

<script>

    $(function () {

        $("#footer_pagos").attr('src','${pageContext.request.contextPath}/img/i-billetera.png');
        

    });


    function list_pago_propio() {
        $.ajax({
            url: '${pageContext.request.contextPath}/PagoCreditoPropioServlet',
            type: 'post',
            dataType: "text",
            beforeSend: function () {

            },
            data: {
                accion: 'listar_cuenta_credito'
              
            },
            success: function (salida) {

                if (salida.length !== 0) {

                    var array = salida.split("%%%");
                    if (array[0] === "0") {
                        crearForm(array[1]);
                     } else {
                        message_req("Mensaje", array[1]);
                    }

                } else {
                    message_req("Mensaje", "Error Inesperado");
                }
            },
            error: function () {
                window.location.href = "${pageContext.request.contextPath}/";
            }
        });



    }
    function list_pago_terc() {
        $.ajax({
            url: '${pageContext.request.contextPath}/PagoCreditoTerceroServlet',
            type: 'post',
            dataType: "text",
            beforeSend: function () {

            },
            data: {
                accion: 'pago_terceros'
              
            },
            success: function (salida) {

                if (salida.length !== 0) {

                    var array = salida.split("%%%");
                    if (array[0] === "0") {
                        crearForm(array[1]);
                     } else {
                        message_req("Mensaje", array[1]);
                    }

                } else {
                    message_req("Mensaje", "Error Inesperado");
                }
            },
            error: function () {
                window.location.href = "${pageContext.request.contextPath}/";
            }
        });



    }


</script>