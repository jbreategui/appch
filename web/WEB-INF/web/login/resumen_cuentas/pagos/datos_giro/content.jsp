<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<div class="row cuerpo">


    <div class="col-xs-12 col-md-12" style="margin-top: 0px">

        <p class="text-center negrita" style="font-size:18px">Datos del giro</p>

    </div>

    <div class="col-xs-12 text-center">

        <div class="form-group">

            <input type="text" id="apellido_paterno" class="form-control" placeholder="Ingresa apellido paterno">

        </div>

    </div>

    <div class="col-xs-12 text-center">


        <div class="form-group">

            <input type="text" id="apellido_materno" class="form-control" placeholder="Ingresa apellido materno">

        </div>

    </div>

    <div class="col-xs-12 text-center">

        <div class="form-group">

            <input type="text" id="nombres" class="form-control" placeholder="Ingresa nombres">

        </div>

    </div>

    <div class="col-xs-6 text-center">

        <div class="form-group">

            <input  type="tel" id="dni" maxlength="8" class="form-control solo-numeros" placeholder="Ingrese DNI">

        </div>

    </div>

    <div class="col-xs-6 text-center">

        <div class="form-group">

            <input type="tel" id="telefono" maxlength="9" class="form-control solo-numeros" placeholder="Ingrese tel�fono">

        </div>

    </div>

    <div class="col-xs-5 text-center" style="padding-right:0px">

        <div class="form-group" style="line-height:35px">

            <span>Destino del Giro</span>

        </div>

    </div>

    <div class="col-xs-7 text-center " style="padding-left:0px">

        <div class="form-group">

            <select class="form-control" id="agencia">
                <c:forEach var="lista" items="${lista_agencia}">
                    <option value="${lista[0]}">${lista[1]}</option>
                </c:forEach>
            </select>

        </div>

    </div>

    <div class="col-xs-12 text-center">

        <span>Monto de env�o</span> <br>

        <div class="input-group">

            <input id="monto" type="${type}" class="form-control solo-numeros" placeholder="${moneda_giro}">
            
            <div class="input-group-btn">
                <button class="btn btn-borrar" type="submit">
                    <i style="color:#CC3333" class="fas fa-times-circle"></i>
                </button>
            </div>

        </div>

    </div>


    <div class="col-xs-12" style="margin-top: 10px">

        <div class="col-xs-6 text-center">
            <a onclick="ingresar_pagos();" class="btn btn-caja btn-block">Cancelar</a>
        </div>
        <div class="col-xs-6 text-center">
            <a  onclick="js_validar_giro();"  class="btn btn-caja btn-block">Siguiente</a>
        </div>

    </div>




</div>

<script>

    function ingresar_pagos() {

        $.ajax({
            url: '${pageContext.request.contextPath}/PagoCreditoPropioServlet',
            type: 'post',
            dataType: "text",
            beforeSend: function () {

            },
            data: {
                'accion': "pagos"

            },
            success: function (salida) {

                if (salida.length !== 0) {

                    var array = salida.split("%%%");
                    if (array[0] === "0") {
                        crearForm(array[1]);
                     } else {
                        message_req("Mensaje", array[1]);
                    }

                } else {
                    message_req("Mensaje", "Error Inesperado");
                }
            },
            error: function () {
                window.location.href = "${pageContext.request.contextPath}/";
            }
        });

    }


</script>