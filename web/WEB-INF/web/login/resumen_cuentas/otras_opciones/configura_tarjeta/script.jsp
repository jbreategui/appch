<!-- jQuery -->
<script src="${pageContext.request.contextPath}/jq/jquery-3.2.1.min.js" type="text/javascript"></script>
<!-- Bootstrap -->
<script src="${pageContext.request.contextPath}/jq/bootstrap/bootstrap.min.js" type="text/javascript"></script>

<script>

    $(function () {

        //pintamos footer
        $("img#footer_mas").attr('src', '${pageContext.request.contextPath}/img/i-puntos.png');

        var chek = "${configuratarjeta[1]}";

        if (chek === "true") {

            $("#myonoffswitch").prop("checked", true);
        } else {
            $("#myonoffswitch").prop("checked", false);
        }

    });





    function configurar_tarjeta() {
        var estado = $("#myonoffswitch").is(":checked") ? 'true' : 'false';
        $.ajax({
            url: '${pageContext.request.contextPath}/ConfiguraTarjetaServlet',
            type: 'post',
            dataType: "text",
            beforeSend: function () {
                $("#btn-confirmar").attr("disabled", true).addClass("btn-caja-activo").removeClass("btn-caja");
            },
            data: {
                accion: 'registrar',
                estado: estado,
                ip: ip_movil()
            },
            success: function (salida) {

                $("#btn-confirmar").attr("disabled", false).addClass("btn-caja").removeClass("btn-caja-activo");

                if (salida.length !== 0) {

                    var array = salida.split("%%%");
                    if (array[0] === "0") {
                        crearForm(array[1]);
                    } else {
                        message_req("Mensaje", array[1]);
                    }

                } else {
                    message_req("Mensaje", "Error Inesperado");
                }
            },
            error: function () {
                window.location.href = "${pageContext.request.contextPath}/";
            }
        });


    }

</script>