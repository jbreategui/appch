<!DOCTYPE html>
<html>
    <head>

        <jsp:include page="/web/glb/head_general.jsp" />

        <jsp:include page="confirma_huella/head.jsp" />

    </head>
    <body>

        <jsp:include page="/web/glb/header_login.jsp" />

        <jsp:include page="confirma_huella/content.jsp" />

        <jsp:include page="/web/glb/footer_login.jsp" />

        <jsp:include page="confirma_huella/script.jsp" />


    </body
</html>