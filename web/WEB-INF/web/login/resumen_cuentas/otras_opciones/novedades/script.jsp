<!-- jQuery -->
<script src="${pageContext.request.contextPath}/jq/jquery-3.2.1.min.js" type="text/javascript"></script>
<!-- Bootstrap -->
<script src="${pageContext.request.contextPath}/jq/bootstrap/bootstrap.min.js" type="text/javascript"></script>

<script>

    $(function () {

        var origen = "<%= session.getAttribute("origen")%>";

        $("img#footer_mas").attr('src','${pageContext.request.contextPath}/img/i-puntos.png');

    });
    
    var tipo_red_social = "";


    function facebook() {
        
        var link = '${redes_facebook[2]}';
        tipo_red_social = "facebook";

        red_social(link);

    }
    function twitter() {
        
        var link = '${redes_twitter[2]}';
        tipo_red_social = "twitter";

        red_social(link);

    }
    function youtube() {
        
        var link = '${redes_youtube[2]}';
        tipo_red_social = "youtube";

        red_social(link);
        
    } 
    function blogger() {
        
        var link = '${redes_blogger[2]}';
        tipo_red_social = "blogger";

        red_social(link);
        
    }
    
    function red_social(link){
        
        if (check_so() === "iOS") {
            
            link +="%%%%"+tipo_red_social;
            
            set_LINK( link );
        } else if (check_so() === "Android") {
           Android.url_externo( link );
        }
        
    }
    

</script>