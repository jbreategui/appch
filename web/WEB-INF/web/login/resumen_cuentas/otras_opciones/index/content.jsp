<div class="row cuerpo">

    <%session.setAttribute("origen", "pagos");%>

    <div class="col-xs-12 col-md-12 nopad box box-caja"  style="margin-top: 10px">

        <div class="col-xs-12 text-center" style="padding: 8px; margin-bottom: 0px;border-bottom:1px solid #808080">
        <div class="text-center text-color-3 "  style="font-family: futura;font-size:20px;font-weight: 700">Otras Opciones</div>
        
        </div>
        
    </div>



    <div class="col-xs-12 col-md-12 nopad box box-caja"  style="margin-top: 10px">

        <a onclick="ir_configuraciones();">   
            <div id="configuraciones" class="form-group text-center eleccion cambiar_pagina lista-caja">
                <span>Configuraciones</span>
                <span class="glyphicon glyphicon-play text-color-1 pull-right"></span>
            </div>
        </a>
        <a onclick="ir_perfil();">   

            <div id="perfil" class="form-group text-center eleccion cambiar_pagina lista-caja">
                <span>Mi Perfil</span>
                <span class="glyphicon glyphicon-play text-color-1 pull-right"></span>
            </div>

        </a>
        <a onclick="ir_servicio_al_cliente();">   

            <div id="servicio_cliente" class="form-group text-center eleccion cambiar_pagina lista-caja">
                <span>Servicio al cliente</span>
                <span class="glyphicon glyphicon-play text-color-1 pull-right"></span>
            </div>

        </a>
        <a onclick="ir_informacion();">   

            <div id="informacion" class="form-group text-center eleccion cambiar_pagina lista-caja">
                <span >Información</span>
                <span class="glyphicon glyphicon-play text-color-1 pull-right"></span>
            </div>

        </a>



    </div>

    <div class="col-xs-12  col-sm-10 col-sm-offset-1  text-center" style="margin-top:20px">
        <a  href="${pageContext.request.contextPath}/LoginServlet?accion=LOGOUT"class="btn btn-block btn-caja tam1 cambiar_pagina" style="border-radius: 20px">Cerrar Sesión</a>
    </div>

</div>