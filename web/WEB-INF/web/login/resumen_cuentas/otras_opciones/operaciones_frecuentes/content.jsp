<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<div class="row cuerpo">

    <%session.setAttribute("origen", "mas");%>

    <div class="col-xs-12 col-md-12">

        <div class="form-group text-center">

            <span class="titulo-negrita">Configura tus operaciones frecuentes</span>

        </div>

    </div>


    <div class="col-xs-12 col-md-12 text-center">

        <div class="form-group">

            <span class="negrita">Sin clave SMS o Coordenada </span> 
            <span style="display:inline-block;height: 16px;">
                <div class="onoffswitch" style="top:8px">
                    <input type="checkbox" name="onoffswitch" class="onoffswitch-checkbox" id="myonoffswitch" checked>
                    <label class="onoffswitch-label" for="myonoffswitch">
                        <span class="onoffswitch-inner"></span>
                        <span class="onoffswitch-switch"></span>
                    </label>
                </div>
            </span>

        </div>

    </div>





    <div class="col-xs-offset-1 col-xs-10 col-md-offset-1 col-md-10 text-center" style="border-radius:10px;border:solid 1px #C0C7C8;padding:5px;margin-bottom:15px">

        <span> Activa y desactiva tu clave SMS o tarjeta coordenada para realizar operaciones frecuentes </span>

    </div>





    <c:if test="${FLAG_OP_SERVLET=='false' || r_estado_frecuente == 'false'}">

        <div class="col-xs-offset-1 col-xs-10 col-md-offset-1 col-md-10 text-center" >

            <span> ${configura_oper_men} </span>

        </div>

        <div class="col-xs-offset-1 col-xs-10 col-md-offset-1 col-md-10 text-center nopad" >

            <input id="coordenada_clave_dinamica" type="text" class="form-control">


        </div>
    </c:if>
    <div class=" col-xs-offset-1 col-xs-10 col-md-offset-1 col-md-10" style="margin-top: 10px">


        <button id="btn-confirmar" onclick="registrar_frec();" class="btn btn-caja btn-block">Guardar</button>


    </div>


</div>