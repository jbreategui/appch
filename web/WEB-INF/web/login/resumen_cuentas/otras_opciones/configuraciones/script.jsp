<!-- jQuery -->
<script src="${pageContext.request.contextPath}/jq/jquery-3.2.1.min.js" type="text/javascript"></script>
<!-- Bootstrap -->
<script src="${pageContext.request.contextPath}/jq/bootstrap/bootstrap.min.js" type="text/javascript"></script>

<script>

    $(function () {

        $("img#footer_mas").attr('src', '${pageContext.request.contextPath}/img/i-puntos.png');


    });

    $(document).ready(function () {

        if (${validador} === true) {

        } else {

            $("#mostrar_huella").attr("class", "hidden");
        }
    }
    );

    function configurar_tarjeta() {


        $.ajax({
            url: '${pageContext.request.contextPath}/ConfiguraTarjetaServlet',
            type: 'post',
            dataType: "text",
            beforeSend: function () {

            },
            data: {
                accion: 'configura_tarjeta'

            },
            success: function (salida) {

                if (salida.length !== 0) {

                    var array = salida.split("%%%");
                    if (array[0] === "0") {
                        crearForm(array[1]);
                    } else {
                        message_req("Mensaje", array[1]);
                    }

                } else {
                    message_req("Mensaje", "Error Inesperado");
                }
            },
            error: function () {
                window.location.href = "${pageContext.request.contextPath}/";
            }
        });


    }
    function configurar_oper_frec() {


        $.ajax({
            url: '${pageContext.request.contextPath}/ConfiguraOperFrecuentesServlet',
            type: 'post',
            dataType: "text",
            beforeSend: function () {

            },
            data: {
                accion: 'operaciones_frecuentes'

            },
            success: function (salida) {

                if (salida.length !== 0) {

                    var array = salida.split("%%%");
                    if (array[0] === "0") {
                        crearForm(array[1]);
                    } else {
                        message_req("Mensaje", array[1]);
                    }

                } else {
                    message_req("Mensaje", "Error Inesperado");
                }
            },
            error: function () {
                window.location.href = "${pageContext.request.contextPath}/";
            }
        });


    }

    function configurar_huella_digital() {




        $.ajax({
            url: '${pageContext.request.contextPath}/ConfiguraHuellaDigitalServlet',
            type: 'post',
            dataType: "text",
            beforeSend: function () {

            },
            data: {
                accion: 'retornar_huella'


            },
            success: function (salida) {

                if (salida.length !== 0) {

                    var array = salida.split("%%%");
                    if (array[0] === "0") {
                        crearForm(array[1]);
                     } else {
                        message_req("Mensaje", array[1]);
                    }

                } else {
                    message_req("Mensaje", "Error Inesperado");
                }
            },
            error: function () {
                window.location.href = "${pageContext.request.contextPath}/";
            }
        });


    }






</script>