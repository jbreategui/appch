<!-- jQuery -->
<script src="${pageContext.request.contextPath}/jq/jquery-3.2.1.min.js" type="text/javascript"></script>
<!-- Bootstrap -->
<script src="${pageContext.request.contextPath}/jq/bootstrap/bootstrap.min.js" type="text/javascript"></script>

<script>



    $(function () {

        var huella = "";

        var estado = "${cambio_estado_huella}";


        $("#footer_mas").removeClass('footer-gris-img');

        $("#btn1").click(function () {//boton que llama al modal

            aprobar();

        });

        $("#empezar_huella").click(function () {

            if (estado === "true") {
                $("#terminosycondiciones").modal();
            } else if (estado === "false") {
                $("#myModal").modal();
                aprobar();
            }

        });

        $("#llamar_huella").click(function () {

            $("#terminosycondiciones").modal('toggle');

            $("#myModal").modal();

            aprobar();

        });




        function aprobar() {

            if (check_so() === "iOS") {

                huella = get_huella();


            } else if (check_so() === "Android") {

                huella = Android.demo();

            }

        }

    });


    function js_login(resultado, cifrado) {

        var estado_huella_digital = resultado;
        var huella_estado = ${cambio_estado_huella};
        var huella_cifrada = cifrado;
        if (estado_huella_digital === "true") {

            $.ajax({
                url: '${pageContext.request.contextPath}/ConfiguraHuellaDigitalServlet',
                type: 'post',
                dataType: "text",
                beforeSend: function () {

                },
                data: {
                    accion: 'registrar_huella',
                    estado_huella_digital: huella_estado,
                    ippublicamovil: ip_movil(),
                    huella_cifrada: huella_cifrada


                },
                success: function (salida) {

                    if (salida.length !== 0) {

                        var array = salida.split("%%%");
                        if (array[0] === "0") {
                            crearForm(array[1]);
                        } else {
                            message_req("Mensaje", array[1]);
                        }

                    } else {

                    }
                },
                error: function () {
                    window.location.href = "${pageContext.request.contextPath}/";
                }
            });

        } else
        {
            alert("clave incorrecta");
        }

    }


    function ir_configuraciones() {

        $.ajax({
            url: '${pageContext.request.contextPath}/MiPerfilServlet',
            type: 'post',
            dataType: "text",
            beforeSend: function () {

            },
            data: {
                'accion': "configuraciones"

            },
            success: function (salida) {

                if (salida.length !== 0) {

                    var array = salida.split("%%%");
                    if (array[0] === "0") {
                        crearForm(array[1]);
                    } else {
                        message_req("Mensaje", array[1]);
                    }

                } else {
                    message_req("Mensaje", "Error Inesperado");
                }
            }
            ,
            error: function () {
                window.location.href = "${pageContext.request.contextPath}/";
            }
        });

    }


</script>