<div class="row cuerpo">

    <div class="col-xs-12 col-md-12 text-center">
        <div class="form-group">
            <span class="titulo-negrita">Preguntas frecuentes</span>
        </div>

    </div>

    <div class="col-xs-12 col-md-12 text-center" style="border:1px solid #C0C7C8;border-radius:10px;margin-bottom:15px">
        <div class="form-group">${respuesta_preguntas_frec}</div>
    </div>

    <div class="col-xs-12 col-md-12 ">
        <div class="form-group">
            <a href="${pageContext.request.contextPath}/PreguntasFrecuentesServlet?accion=listar_preguntas" class="btn btn-caja btn-block">volver</a>
        </div>

    </div>





</div>