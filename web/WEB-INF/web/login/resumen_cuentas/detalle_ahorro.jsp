<!DOCTYPE html>
<html>
    <head>

        <jsp:include page="/web/glb/head_general.jsp" />

        <jsp:include page="detalle_ahorro/head.jsp" />

    </head>
    <body>

        <jsp:include page="/web/glb/header_login.jsp" />

        <jsp:include page="detalle_ahorro/content.jsp" />

        <jsp:include page="/web/glb/footer_login.jsp" />
        
        <jsp:include page="/web/glb/script_general.jsp" />

        <jsp:include page="detalle_ahorro/script.jsp" />


    </body
</html>