<!-- jQuery -->
<script src="${pageContext.request.contextPath}/jq/jquery-3.2.1.min.js" type="text/javascript"></script>
<!-- Bootstrap -->
<script src="${pageContext.request.contextPath}/jq/bootstrap/bootstrap.min.js" type="text/javascript"></script>
<script src="${pageContext.request.contextPath}/js/number.js" type="text/javascript"></script>

<script>

    $(function () {

        $("#footer_miscuentas").attr('src','${pageContext.request.contextPath}/img/i-caja-fuerte.png');


        $(".formato_monto").number(true, 2);


        function color_detalle() {
            var num_color = [];

            var dimension = document.getElementById("dimension").value;

            for (var i = 0; i < dimension; ++i)
            {

                var num_color = document.getElementsByName("num_color")[i].value;
                // console.log(num_color);

                if (num_color.match(/^(-)/)) {


                    document.getElementsByName("lista_color")[i].setAttribute("class", "col-xs-12 item-list-caja text-color-1");

                }

            }


        };
        
        color_detalle();
        
        
        $("#compartir_cuenta_ahorros").click(function(){
            
            var texto = "Mi Cuenta de " + "${descripcion}"+" en Caja Huancayo es " + "${array_lista_detalle_ahorro_cta_session[0]}" + "\n \n";
            texto += "Mi n�mero de cuenta interbancaria es " + "${array_lista_detalle_ahorro_cta_session[1]}" + "\n";
            
            if (check_so() === "iOS") {
                set_shared(texto);
            } else if (check_so() === "Android") {
                Android.shared_with(texto);
            }
            
            
        });
            
            
            
        



    });



</script>