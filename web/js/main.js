$(function () {
    $("button").button();
});

function mostrarEnToast(nombre) {

    Android.showToast(nombre);
}

function recogeValoresDeAndroid() {

    var data = Android.valoresGet();

    $.ajax({
        url: "Demo",
        data: {
            data: data
        },
        success: function (result) {
            $("#result").val(result);
        }
    });
}
